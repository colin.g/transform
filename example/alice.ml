let (value : (int * int) -> (int -> int)) =
 fun (param : int * int) ->
 fun (state: int) ->
  if is_zero (fst param) then 0
  else state + (snd param)
in

let (operations :(int * int) -> (int -> (int * int) list)) =
 fun (param : (int * int)) ->
   fun (state:int) ->
  if is_zero (fst param) then (0, state)::[]
  else []
in

let (run: (int * int) -> (int * int) -> (((int * int) list)  * (int * int) )) =
 fun (param : int * int) -> fun (laststate : int * int) ->
  let (init_state : int) = 0 in
  let (state : int) =
    if is_zero (fst laststate) then init_state else (snd laststate)
  in
  let (time : int) = fst laststate in
  let (vs : int) = value param state in
  let (ops : (int * int) list)  = operations param state in
  let (newstate : int * int) = (time + 1, vs) in
  (ops, newstate)
in run
