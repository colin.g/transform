.PHONY: all clean check top dev-deps

all:
	dune build

clean:
	dune clean

top:
	dune utop

run:
	dune exec bin/main.exe

check:
	dune runtest

watch:
	dune runtest --watch

dev-deps:
	opam exec -- opam install --deps-only --assume-depexts -y ./dev-deps
