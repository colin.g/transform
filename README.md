# Transform

This is an implementation to investigate the generation of
a small-step interpreter for CBV based on the
CPS+defunctionalization program transformation.

# Quick start

- to install it
```sh
$ opam install .
```

- to install locally
```sh
$ ./configure
$ make install
```

- to build it
```sh
$ ./configure
$ make
```

- to install emacs dev tools (merlin, tuareg, ...)
```sh
$ make dev-deps
```

- to test it
```sh
$ make check
```

- to run it
```sh
$ make check
```

- to launch an interactive top-level:
```sh
$ make top
```

# Dependencies:
`dune` and `opam` for setup and build.
