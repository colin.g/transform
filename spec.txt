G ::= bool | int | unit
A ::= U Ḇ | G
Ḇ ::= A -> Ḇ | F A

C ::=
  | λ x:A . C | return V | force V | C V
  | let x:A = V in C | C to x : A in C | μ x : A . C

V ::= x | c | thunk C
A Stream ≃  (Time → F (A Stream))
Time ≃ ℤ

〚 n 〛ᵛ = DUP n
〚 c 〛ᵛ = PUSH c
〚 thunk C 〛ᵛ = LAMBA unit A [〚 C 〛ᶜ]
〚 ⟨V,V'⟩ 〛ᵛ : = Pair [〚V〛ᵛ; 〚 V′〛]

〚λ A . C 〛ᶜ = [LAMBDA A A′ [〚 C 〛ᶜ; DIP [DROP]]]
〚return V 〛ᶜ = [〚V〛ᵛ]
〚force V〛ᶜ = [〚V〛ᵛ ;PUSH unit (); EXEC]]
〚C V〛ᶜ = 〚C〛ᶜ @ [〚V〛ᵛ;EXEC]
〚let A = V in C〛ᶜ = [〚V〛ᵛ] @〚C〛ᶜ @[ DIP [DROP] ]
〚C to A in C′〛ᶜ = 〚C〛ᶜ @〚C′〛ᶜ@ [DIP [DROP]]
〚μ x:A . C〛ᶜ = [LAMBDA_REC unit A [DROP ;〚C〛ᶜ ; DIP [DROP]]]

〚 input x〛ᵛ  = proj 〚x〛ᵛ I

let nth = thunk (
μ (nth). λ x. λ y. λ time.
  if y =? 0 then hd x else
  tl x to mtail in
  (force mtail) time to tail in y−1 to y in time+1 to time in (force nth_t) tail y time )
Origin = 0
Contract :

λ ((HI,T),I).
let F = λ H T . program_translation
let HI' = HI++I in
(HI', ((force nth) (T+1) Origin  (proj_operation (F HI') Origin))


Example : give back:

operations                 | deposit
—————————————————————————————————–
transfer sender ipt0[0]    |

               ↓ to lisa

rectup operations = transfer ipt0@(fun x -> x)::operations
and    ipt0 = input "ipt0"::ipt0

              ↓ to CBPV+Stream+At
now ̣≔ λ x. x

μ.{ operations = return (transfer sender deposit@now::operations);
        depoist    = return (input "deposit"::deposit }

              ↓ to CBPV+Stream

μ.{ operations = fun t ->
       return (transfer sender (now t to t in nth 0 t deposit)::
            thunk (fun t -> ((force operations) t+1));
    deposit    = fun t ->
       return (input "deposit"::
            thunk (fun t -> ((force deposit) t+1)) }

CONTRACT ((deposit,t),d)
  { UNPAIR;
    UNPAIR;
    DUP %deposit;
    DUP %d;
    LIST_APPEND; % make new history
    〚 now 〛ᶜ;
    PUSH 1;
    DUP %t;
    PLUS;
    EXEC;  %compute observation time: now t+1
    〚nth〛ᶜ;
    PUSH 0;
    DUP %t+1;
    PAIR;
    LAMBDA_REC ((h, n),()) {transfer sender (list_nth n h); SWAP;PAIR};
    DUP %new history;
    DUP %t+1;
    PAIR;
    APPLY
    PAIR;
    EXEC;
    PAIR;
    DIP {DROP %remaining data}
}

let rec p =
((fun t -> [0] @ (let rec next t = if t = 10 then [] else (snd p) (t+1) in next (t))),
 (fun t -> [1] @ (let rec next t = if t = 10 then [] else (fst p) (t+1) in next (t))))

let rec n t i = [0] @ (let rec next t = if t = i then [] else [List.nth (n 0 t) (t-1) + 1] @ next (t+1) in next (t+1) );;

rec fibo = 0::thunk (1::thunk (rec next = (force fibo)@pre + (force fibo)@pre2 :: next))

let rec fibo t i = [0;1] @
(let rec next t = if t = i then [] else
[List.nth (fibo 0 t) (t-1) + List.nth (fibo 0 t) (t-2)] @
next (t+1) in if t= i then [] else next (t+2) );;

let rec fibo_cts t i =
  let cache = ref [0;1] in
  let rec next t =
    if
      t = i then ()
    else
      (let cache' = [List.nth !cache (t-1) + List.nth !cache (t-2)] in
      cache := !cache @ cache'; next (t+1))
  in
  if i < List.length !cache then () else next (t+2); !cache
;;

let rec fibo_cts_fast i =
  let cache = ref [1;0] in
  let rec next t =
    if
      t = i then ()
    else
      (
      let cache' = List.nth !cache 0 + List.nth !cache 1
      in cache := cache'::!cache; next (t+1) )
  in
  if i < 2  then () else next 2; !cache;;


let fibo =
  thunk (rec s = fun time_s ->
  return (0::thunk (fun time0 ->
  return (1::thunk (fun time1 ->
    ((rec next  = fun time2 ->
      force s time1 =: n in
      force nth n (time2 - 1) 0 =: v in
      force nth n (time2 - 2) 0 =: w in
      return ((v+w)::(thunk (fun t -> force next (t+1))))) (time1+1)))))))

let fibo = fun () ->
   let rec s_0 () t i =
   [0] @
   (let s_1 () t i = [1] @
      (let rec next () t i =
         if t = i then [] else
         [List.nth (s_0 0 t) (t-1) +
          List.nth (s_0 0 t) (t-2)] @ (next ()) (t+1) i
       in next () (t+1) i)
    in if t=i then [] else s_1 () (t+1) i)
   in s_0 ()

let nat =
  thunk (rec s = fun time_s ->
  return (0::thunk (fun time0 ->
    ((rec next  = fun time1 ->
      force s time1 =: n in
      force nth n (time1 - 1) 0 =: predn in
      predn + 1 =: v in
      return (v::(thunk (fun t -> force next (t+1))))) (time0+1)))))

let nat =
  thunk (rec s = fun time_s i ->
   List.cons 0
   ((force (thunk (fun time0 i ->
   if time0 = i then return [] else
    ((rec next  = fun time1 i ->
      if i = time1 then return [] else
      (force s) 0 t =: n in
      List.nth n (time1 - 1)  =: predn in
      predn+1 =: v in
      List.cons  (force (thunk ( (fun i t -> force next (t+1) i) time1 0))) (time0+1) i))))
   (time_s + 1) i))


let nat =
  thunk (rec s = fun time_s i ->
   List.cons [0]
   (if t = i then return [] else
   ((rec next  = fun time1 i ->
      if i = t then return [] else
      (force s) 0 t =: n in
      List.nth n (time1 - 1) + 1  =: v in
      List.cons (v+1) ((force next) (time1+1) i)) (time_s+1) i))

let nat = fun () ->
   let rec s () t i =
   0 ::
   (let rec next () t i =
     if t = i then [] else
     List.nth (s () 0 t) (t-1) + 1 ::  next () (t+1) i
   in
   if t = i then [] else
   next () (t+1) i)
   in s ()

Monade Observation
A Stream ≃  (Time → U (F (A Stream)))
Time ≃ ℤ

Monade d'Iteration
〚 A Stream 〛 = (Time -> Time -> F (A List))
〚a:: thunk (fun t -> x (t+1))〛 = fun origin horizon ->
 if origin = horizon then [] else (List.cons a ((force (thunk x)) origin horizon)
〚(force s) t〛 = (force s) 0 t
