open Syntax
open Cont
open Terms

let push x = Push (TyInt, Int x)
let pair = Prim Pair

(* let dup = Prim Dup *)
let seq a b = Seq (a, b)
let add = Prim Add

type (_, _) stackty =
  | TyEmpty : (unit, unit) stackty
  | TyItem : 'a ty * ('s, 'rest) stackty -> ('a, 's * 'rest) stackty

type 'before dupn_proof_arg =
  | Dupn_proof_arg :
      ('before, 'a) dup_n_gadt_witness * 'a ty
      -> 'before dupn_proof_arg

type (_, _) dropn_proof_arg =
  | Dropn_proof_arg :
      ('a, 's, 'a, 's, 'fa, 'fs, 'fa, 'fs) proof * ('fa, 'fs) stackty
      -> ('a, 's) dropn_proof_arg

exception TypeError of string Lazy.t

type spec = Spec : node * ('a, 's) stackty * ('b, 'u) stackty -> spec
type (_, _) eq = Eq : ('a, 'a) eq

let string_of_node = Printer.string_of_mimi

let rec string_of_ty : type a. a ty -> string = function
  | TyInt -> "int"
  | TyUnit -> "unit"
  | TyBool -> "bool"
  | TyList t -> Printf.sprintf "(list %s)" (string_of_ty t)
  | TyProd (a, b) ->
      Printf.sprintf "(pair %s %s)" (string_of_ty a) (string_of_ty b)
  | TyLam (a, b) ->
      Printf.sprintf "(lambda %s %s)" (string_of_ty a) (string_of_ty b)
  | TyStream t -> Printf.sprintf "(stream %s)" (string_of_ty t)

let rec string_of_stackty : type a s. (a, s) stackty -> string = function
  | TyEmpty -> "()"
  | TyItem (ty, sty) -> string_of_ty ty ^ "; " ^ string_of_stackty sty

let base = TyEmpty
let ( @+ ) ty s = TyItem (ty, s)
let tyint = TyInt
let tyunit = TyUnit

let rec ty_equal : type a b. a ty -> b ty -> (a, b) eq =
 fun a b ->
  match (a, b) with
  | TyInt, TyInt -> Eq
  | TyUnit, TyUnit -> Eq
  | TyBool, TyBool -> Eq
  | TyProd (a1, b1), TyProd (a2, b2) -> (
      match ty_equal a1 a2 with Eq -> ( match ty_equal b1 b2 with Eq -> Eq))
  | TyList t, TyList u ->
      let Eq = ty_equal t u in
      Eq
  | TyStream t, TyStream u ->
      let Eq = ty_equal t u in
      Eq
  | TyLam (a1, b1), TyLam (a2, b2) -> (
      match ty_equal a1 a2 with Eq -> ( match ty_equal b1 b2 with Eq -> Eq))
  | _, _ ->
      let msg =
        lazy (Printf.sprintf "%s <> %s\n" (string_of_ty a) (string_of_ty b))
      in
      raise (TypeError msg)

let rec stackty_equal :
    type a s b u. (a, s) stackty -> (b, u) stackty -> (a * s, b * u) eq =
 fun s t ->
  match (s, t) with
  | TyEmpty, TyEmpty -> Eq
  | TyItem (a, s), TyItem (b, t) -> (
      match ty_equal a b with Eq -> ( match stackty_equal s t with Eq -> Eq))
  | _, _ ->
      let msg =
        lazy
          (Printf.sprintf "Type clash: %s <> %s\n" (string_of_stackty s)
             (string_of_stackty t))
      in
      raise (TypeError msg)

type (_, _) typing_judgment =
  | J : {
      isty : ('a, 's) stackty;
      osty : ('b, 'u) stackty;
      kinstr : 'r 'f. ('b, 'u, 'r, 'f) cinstr -> ('a, 's, 'r, 'f) cinstr;
    }
      -> ('a, 's) typing_judgment

type exty = Ex : 'a ty -> exty

type literal_typing_judgment =
  | JLit : { x : 'a; xty : 'a ty } -> literal_typing_judgment

let rec gty_of_ty : Syntax.Types.ty -> exty =
 fun t ->
  match t with
  | Syntax.Types.TyInt -> Ex TyInt
  | Syntax.Types.TyBool -> Ex TyBool
  | Syntax.Types.TyUnit -> Ex TyUnit
  | Syntax.Types.TyProd (ty1, ty2) -> (
      match gty_of_ty ty1 with
      | Ex ty1 -> ( match gty_of_ty ty2 with Ex ty2 -> Ex (TyProd (ty1, ty2))))
  | Syntax.Types.TyArrow (ty1, ty2) -> (
      match gty_of_ty ty1 with
      | Ex ty1 -> ( match gty_of_ty ty2 with Ex ty2 -> Ex (TyLam (ty1, ty2))))
  | Syntax.Types.TyList ty -> (
      match gty_of_ty ty with Ex ty -> Ex (TyList ty))
  | Syntax.Types.TyStream ty -> (
      match gty_of_ty ty with Ex ty -> Ex (TyStream ty))

let rec infer_literal : value -> literal_typing_judgment = function
  | Int i -> JLit { x = i; xty = TyInt }
  | Bool b -> JLit { x = b; xty = TyBool }
  | Unit -> JLit { x = (); xty = TyUnit }
  | List xs -> infer_list xs

and infer_list xs =
  let rec aux : value list -> literal_typing_judgment = function
    | [ x ] -> (
        match infer_literal x with
        | JLit { x; xty } -> JLit { x = [ x ]; xty = TyList xty })
    | x :: xs -> (
        match (infer_literal x, aux xs) with
        | JLit { x; xty }, JLit { x = xs; xty = TyList tyelt } -> (
            match ty_equal tyelt xty with
            | Eq -> JLit { x = x :: xs; xty = TyList xty })
        | _ -> raise (TypeError (lazy "Type mismatch in list")))
    | _ -> raise (TypeError (lazy "Empty list literal"))
  in
  aux xs

and elaborate : type a s. (a, s) stackty -> node -> (a, s) typing_judgment =
 fun isty node ->
  match (node, isty) with
  | Prim (Push (ty, x)), _ -> (
      let (JLit { x; xty }) = infer_literal x in
      let (Ex ty) = gty_of_ty ty in
      match ty_equal ty xty with
      | Eq ->
          let osty = TyItem (xty, isty) in
          J { isty; osty; kinstr = (fun k -> IPush (x, k)) })
  | Prim (Dup i), _ ->
      let rec make_proof_argument :
          type a s. int -> (a, s) stackty -> (a * s) dupn_proof_arg =
       fun n (stk : (a, s) stackty) ->
        match (n = 0, stk) with
        | true, TyItem (t, _) -> Dupn_proof_arg (Dup_n_zero, t)
        | false, TyItem (_, tl) ->
            let (Dupn_proof_arg (p, t)) = make_proof_argument (n - 1) tl in
            Dupn_proof_arg (Dup_n_succ p, t)
        | _ -> assert false
      in
      let (Dupn_proof_arg (p, elt)) = make_proof_argument i isty in
      let osty = TyItem (elt, isty) in
      J { isty; osty; kinstr = (fun k -> IDup (p, k)) }
  | Prim (Drop n), _ ->
      let rec make_proof_argument :
          type a s. int -> (a, s) stackty -> (a, s) dropn_proof_arg =
       fun n s ->
        match (n = 0, s) with
        | true, s -> Dropn_proof_arg (KStack, s)
        | false, TyItem (_, s) ->
            let (Dropn_proof_arg (p, s)) = make_proof_argument (n - 1) s in
            Dropn_proof_arg (KPrefix p, s)
        | _ -> assert false
      in
      let (Dropn_proof_arg (p, osty)) = make_proof_argument n isty in
      J { isty; osty; kinstr = (fun k -> IDrop (p, k)) }
  | Prim (Lam (aty, rty, code)), _ -> (
      let (Ex aty') = gty_of_ty aty in
      let (Ex rty') = gty_of_ty rty in
      match elaborate (TyItem (aty', TyEmpty)) code with
      | J jcode -> (
          match stackty_equal jcode.osty (TyItem (rty', TyEmpty)) with
          | Eq ->
              J
                {
                  isty;
                  osty = TyItem (TyLam (aty', rty'), isty);
                  kinstr =
                    (fun k -> ILambda (Lam (aty', rty', jcode.kinstr IHalt), k));
                }))
  | Prim (LamRec (aty, rty, code)), _ -> (
      let (Ex aty') = gty_of_ty aty in
      let (Ex rty') = gty_of_ty rty in
      match
        elaborate (TyItem (aty', TyItem (TyLam (aty', rty'), TyEmpty))) code
      with
      | J jcode -> (
          match stackty_equal jcode.osty (TyItem (rty', TyEmpty)) with
          | Eq ->
              J
                {
                  isty;
                  osty = TyItem (TyLam (aty', rty'), isty);
                  kinstr =
                    (fun k ->
                      ILambda (LamRec (aty', rty', jcode.kinstr IHalt), k));
                }))
  | Prim Exec, TyItem (TyLam (a, r), TyItem (b, isty')) -> (
      match ty_equal a b with
      | Eq ->
          let osty = TyItem (r, isty') in
          J { isty; osty; kinstr = (fun k -> IExec k) })
  | Prim Apply, TyItem (TyLam (TyProd (a, c), r), TyItem (b, isty')) -> (
      match ty_equal a b with
      | Eq ->
          let osty = TyItem (TyLam (c, r), isty') in
          J { isty; osty; kinstr = (fun k -> IApp k) })
  | Prim Pair, TyItem (ty1, TyItem (ty2, isty')) ->
      let osty = TyItem (TyProd (ty1, ty2), isty') in
      J { isty; osty; kinstr = (fun k -> IPair k) }
  | Prim Swap, TyItem (ty1, TyItem (ty2, isty')) ->
      let osty = TyItem (ty2, TyItem (ty1, isty')) in
      J { isty; osty; kinstr = (fun k -> ISwap k) }
  | Prim Unpair, TyItem (TyProd (ty1, ty2), isty') ->
      let osty = TyItem (ty1, TyItem (ty2, isty')) in
      J { isty; osty; kinstr = (fun k -> IUnpair k) }
  | Prim Fst, TyItem (TyProd (ty1, _), isty') ->
      let osty = TyItem (ty1, isty') in
      J { isty; osty; kinstr = (fun k -> IFst k) }
  | Prim Snd, TyItem (TyProd (_, ty2), isty') ->
      let osty = TyItem (ty2, isty') in
      J { isty; osty; kinstr = (fun k -> ISnd k) }
  | Prim (Dip t), TyItem (ty, (TyItem (_, _) as bot)) -> (
      match elaborate bot t with
      | J jt ->
          let osty' = jt.osty in
          let osty = TyItem (ty, osty') in
          J { isty; osty; kinstr = (fun k -> IDip (jt.kinstr IHalt, k)) })
  | Prim CompNZ, TyItem (TyInt, bot) ->
      let osty = TyItem (TyBool, bot) in
      J { isty; osty; kinstr = (fun k -> ICompNZ k) }
  | Prim Not, TyItem (TyBool, isty') ->
      let osty = TyItem (TyBool, isty') in
      J { isty; osty; kinstr = (fun k -> INot k) }
  | Prim Add, TyItem (TyInt, TyItem (TyInt, bot)) ->
      let osty = TyItem (TyInt, bot) in
      J { isty; osty; kinstr = (fun k -> IAdd k) }
  | Prim Minus, TyItem (TyInt, TyItem (TyInt, bot)) ->
      let osty = TyItem (TyInt, bot) in
      J { isty; osty; kinstr = (fun k -> IMinus k) }
  | Prim Mult, TyItem (TyInt, TyItem (TyInt, bot)) ->
      let osty = TyItem (TyInt, bot) in
      J { isty; osty; kinstr = (fun k -> IMult k) }
  | Prim Div, TyItem (TyInt, TyItem (TyInt, bot)) ->
      let osty = TyItem (TyInt, bot) in
      J { isty; osty; kinstr = (fun k -> IDiv k) }
  | Prim And, TyItem (TyBool, TyItem (TyBool, bot)) ->
      let osty = TyItem (TyBool, bot) in
      J { isty; osty; kinstr = (fun k -> IAnd k) }
  | Prim Or, TyItem (TyBool, TyItem (TyBool, bot)) ->
      let osty = TyItem (TyBool, bot) in
      J { isty; osty; kinstr = (fun k -> IOr k) }
  | Prim Compare, TyItem (a, TyItem (b, bot)) -> (
      match ty_equal a b with
      | Eq ->
          let osty = TyItem (TyInt, bot) in
          J { isty; osty; kinstr = (fun k -> ICompare k) })
  | Prim Cons, TyItem (t, TyItem (TyList u, bot)) -> (
      match ty_equal t u with
      | Eq ->
          let osty = TyItem (TyList u, bot) in
          J { isty; osty; kinstr = (fun k -> ICons k) })
  | ( Prim Stream,
      TyItem (t, TyItem (TyLam (TyProd (TyUnit, TyInt), TyStream r), bot)) )
    -> (
      match ty_equal t r with
      | Eq ->
          let osty = TyItem (TyStream t, bot) in
          J { isty; osty; kinstr = (fun k -> IStream k) })
  | Prim Head, TyItem (TyStream a, bot) ->
      let osty = TyItem (a, bot) in
      J { isty; osty; kinstr = (fun k -> IHead k) }
  | Prim Tail, TyItem (TyStream a, bot) ->
      let osty = TyItem (TyLam (TyProd (TyUnit, TyInt), TyStream a), bot) in
      J { isty; osty; kinstr = (fun k -> ITail k) }
  | Prim (Nil t), _ ->
      let (Ex ty) = gty_of_ty t in
      let osty = TyItem (TyList ty, isty) in
      J { isty; osty; kinstr = (fun k -> INil k) }
  | Prim (If (tt, ff)), TyItem (TyBool, bot) -> (
      match (elaborate bot tt, elaborate bot ff) with
      | J jtt, J jff -> (
          match stackty_equal jtt.osty jff.osty with
          | Eq ->
              let osty = jtt.osty in
              J
                {
                  isty;
                  osty;
                  kinstr = (fun k -> IIf (jtt.kinstr k, jff.kinstr k));
                }))
  | Seq (i, is), _ -> (
      match elaborate isty i with
      | J ji -> (
          match elaborate ji.osty is with
          | J jis ->
              let osty = jis.osty in
              let kinstr k = ji.kinstr (jis.kinstr k) in
              J { isty; osty; kinstr }))
  | _, _ ->
      let msg =
        lazy
          (Printf.sprintf "Stack: %s\nis not a valid output stack for: %s\n"
             (string_of_stackty isty) (string_of_node node))
      in
      raise (TypeError msg)

let typechecker verbose (Spec (n, sin, _)) () =
  try
    ignore (elaborate sin n);
    true
  with exn ->
    if verbose then Printf.printf "%s\n" (Printexc.to_string exn);
    false

let fact_rec =
  LamRec
    ( TyInt,
      TyInt,
      Seq
        ( Prim (Dup 0),
          Seq
            ( Prim CompNZ,
              Prim
                (If
                   ( Seq (Prim (Push (TyInt, Int 1)), Prim (Dip (Prim (Drop 2)))),
                     Seq
                       ( Prim (Dup 0),
                         Seq
                           ( Prim (Dup 0),
                             Seq
                               ( Prim (Push (TyInt, Int 1)),
                                 Seq
                                   ( Prim Minus,
                                     Seq
                                       ( Prim (Dup 3),
                                         Seq
                                           ( Prim Exec,
                                             Seq
                                               ( Prim (Dup 1),
                                                 Seq
                                                   ( Prim Mult,
                                                     Prim (Dip (Prim (Drop 3)))
                                                   ) ) ) ) ) ) ) ) )) ) ) )

let fact_rec_n n =
  Seq (Prim (Push (TyInt, Int n)), Seq (Prim fact_rec, Prim Exec))

let one_two_three = Prim (Push (TyList TyInt, List [ Int 1; Int 2; Int 3 ]))

let%test _ =
  try
    ignore (elaborate base one_two_three);
    true
  with TypeError m ->
    print_endline (Lazy.force m);
    false

let%test _ = typechecker true (Spec (fact_rec_n 5, base, base)) ()
