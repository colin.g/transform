open PPrint
open Syntax
open Terms
open Types

let kw_add = string "ADD"
let kw_minus = string "MINUS"
let kw_mult = string "MULT"
let kw_div = string "DIV"
let kw_compare = string "COMPARE"
let kw_not = string "NOT"
let kw_and = string "AND"
let kw_or = string "OR"
let kw_compnz = string "COMPNZ"
let kw_push = string "PUSH"
let kw_lambda = string "LAMBDA"
let kw_lambda_rec = string "LAMBDA_REC"
let kw_pair = string "PAIR"
let kw_unpair = string "UNPAIR"
let kw_fst = string "FST"
let kw_snd = string "SND"
let kw_apply = string "APPLY"
let kw_exec = string "EXEC"
let kw_dup = string "DUP"
let kw_dig = string "DIG"
let kw_drop = string "DROP"
let kw_dip = string "DIP"
let kw_if = string "IF"
let kw_cons = string "CONS"
let kw_swap = string "SWAP"
let kw_nil = string "NIL"
let kw_star = star
let kw_pair_ty = string "pair"
let kw_list_ty = string "list"
let kw_lambda_ty = string "lambda"
let kw_unit_ty = string "unit"
let kw_int_ty = string "int"
let kw_bool_ty = string "bool"
let prefix = prefix 0 1
let separated = infix 0 0 space
let kw_stream = string "STREAM"
let kw_stream_ty = string "stream"
let kw_head = string "HEAD"
let kw_tail = string "TAIL"

let rec print_instr i =
  match i with
  | Prim p -> print_print p
  | Lit v -> print_value v
  | Seq (i1, i2) ->
      let i1 = print_instr i1 in
      let i2 = print_instr i2 in
      group (i1 ^^ semi) ^^ hardline ^^ i2

and print_print p =
  match p with
  | Add -> kw_add
  | Minus -> kw_minus
  | Mult -> kw_mult
  | Div -> kw_div
  | Not -> kw_not
  | And -> kw_and
  | Or -> kw_or
  | Compare -> kw_compare
  | Cons -> kw_cons
  | Nil ty -> prefix kw_nil (print_type ty)
  | CompNZ -> kw_compnz
  | Push (ty, v) ->
      let vd = print_value v in
      let tyd = print_type ty in
      group (infix 0 1 tyd kw_push vd)
  | Lam (var, covar, b) ->
      let vard = print_type var in
      let covard = print_type covar in
      let bd = print_instr b in

      separate space [ kw_lambda; vard; covard; group (braces (jump 1 0 bd)) ]
  | LamRec (var, covar, b) ->
      let vard = print_type var in
      let covard = print_type covar in
      let bd = print_instr b in

      separate space
        [ kw_lambda_rec; vard; covard; group (braces (jump 1 0 bd)) ]
  | Pair -> kw_pair
  | Unpair -> kw_unpair
  | Fst -> kw_fst
  | Snd -> kw_snd
  | Exec -> kw_exec
  | Apply -> kw_apply
  | Dup 0 -> kw_dup
  | Dup i ->
      let id = OCaml.int i in
      group (prefix kw_dup id)
  | Dig 1 -> kw_dig
  | Dig i ->
      let id = OCaml.int i in
      group (prefix kw_dig id)
  | Drop 1 -> kw_drop
  | Drop i ->
      let id = OCaml.int i in
      group (prefix kw_drop id)
  | Dip b ->
      let bd = print_instr b in
      group (prefix kw_dip (braces bd))
  | If (tt, ff) ->
      group
        (prefix kw_if
           (separate_map space
              (fun x -> group (braces (print_instr x)))
              [ tt; ff ]))
  | Swap -> kw_swap
  | Stream -> kw_stream
  | Head -> kw_head
  | Tail -> kw_tail

and print_value v =
  match v with
  | Int x -> OCaml.int x
  | Bool x -> OCaml.bool x
  | Unit -> parens empty
  | List xs -> OCaml.list print_value xs

and print_term x = print_instr x

and print_type_atom t =
  match t with
  | TyUnit -> kw_unit_ty
  | TyInt -> kw_int_ty
  | TyBool -> kw_bool_ty
  | TyArrow (t1, t2) ->
      parens
        (prefix kw_lambda_ty (separated (print_type t1) (print_type_atom t2)))
  | TyProd (t1, t2) ->
      parens
        (prefix kw_pair_ty (separated (print_type t1) (print_type_atom t2)))
  | TyList ty -> parens (prefix kw_list_ty (print_type_atom ty))
  | TyStream ty -> parens (prefix kw_stream_ty (print_type_atom ty))

and print_type ty = group (print_type_atom ty)

let string_of_mimi = IO.document_to_string print_term
