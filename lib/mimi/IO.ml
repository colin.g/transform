let document_to_string printer doc =
  let buf = Buffer.create 256 and doc = printer doc in
  PPrint.ToBuffer.pretty 0.9 80 buf doc;
  Buffer.contents buf

let document_to_format printer fmt doc =
  let doc = printer doc in
  PPrint.ToFormatter.pretty 0.9 80 fmt doc
