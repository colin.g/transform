open Syntax

module StackMachine = struct
  open Instructions

  let rec step : type i o. (i, o) instr -> i -> o =
   fun instr stack ->
    match (instr, stack) with
    | Add, (z2, (z1, s)) -> (z1 + z2, s)
    | Push i, s -> (i, s)
    | Cons (i1, i2), s -> step i2 (step i1 s)

  let value_of p = step p ()

  let%test _ = value_of push_1_push_2_add = (3, ())
end

module CStackMachine = struct
  open Cont

  let empty_stack = ((), ())

  let rec interpret_drop :
      type it i ot o. (it, i, it, i, ot, o, ot, o) proof -> it * i -> ot * o =
    function
    | KStack -> fun s -> s
    | KPrefix proof -> fun (_, s) -> interpret_drop proof s

  let rec interpret_dup :
      type before after. (before, after) dup_n_gadt_witness -> before -> after =
    function
    | Dup_n_zero -> fun (ans, _) -> ans
    | Dup_n_succ n -> fun (_, b) -> interpret_dup n b

  let rec interpret_dig :
      type x ts s tu u tv v.
      (ts, s, tu, u, x, tv * v, tv, v) proof -> ts * s -> x * (tu * u) =
    function
    | KStack -> fun s -> s
    | KPrefix proof ->
        fun s ->
          let x, u' = interpret_dig proof (snd s) in
          (x, (fst s, u'))

  let debug = ref false

  let print_arith_bin_instr op lhs rhs =
    if !debug then (
      Printf.printf "%s: lhs=%d rhs%d\n\n" op lhs rhs;
      flush_all ())

  let print_bool_bin_instr op lhs rhs =
    if !debug then (
      Printf.printf "%s: lhs=%b rhs%b\n\n" op lhs rhs;
      flush_all ())

  let print_unary_instr op arg =
    if !debug then (
      Printf.printf "%s: arg=%s\n\n" op arg;
      flush_all ())

  let print_if_cond test = print_unary_instr "IF_COND" (string_of_bool test)

  let step :
      type itop i otop o. (itop, i, otop, o) cinstr -> itop * i -> otop * o =
   fun i stack ->
    let rec exec :
        type itop i otop o ktop k.
        (itop, i, ktop, k) cinstr ->
        (ktop, k, otop, o) cont ->
        itop * i ->
        otop * o =
     fun k ks s ->
      if !debug then read_line () |> ignore;
      match (k, ks, s) with
      | IHalt, KNil, s -> s
      | IHalt, KReturn (s, ks), (ret, _) -> exec IHalt ks (ret, s)
      | IHalt, KCons (k, ks), s -> exec k ks s
      | IAdd k, ks, (i2, (i1, s)) ->
          print_arith_bin_instr "ADD" i1 i2;
          exec k ks (i1 + i2, s)
      | IMult k, ks, (i2, (i1, s)) ->
          print_arith_bin_instr "MULT" i1 i2;
          exec k ks (i1 * i2, s)
      | IMinus k, ks, (i2, (i1, s)) ->
          print_arith_bin_instr "MINUS" i1 i2;
          exec k ks (i1 - i2, s)
      | IDiv k, ks, (i2, (i1, s)) ->
          print_arith_bin_instr "DIV" i1 i2;
          exec k ks (i1 / i2, s)
      | IAnd k, ks, (i2, (i1, s)) ->
          print_bool_bin_instr "AND" i1 i2;
          exec k ks (i1 && i2, s)
      | IOr k, ks, (i2, (i1, s)) ->
          print_bool_bin_instr "OR" i1 i2;
          exec k ks (i1 || i2, s)
      | ICompare k, ks, (i2, (i1, s)) -> exec k ks (compare i1 i2, s)
      | INot k, ks, (i, s) ->
          print_unary_instr "NOT" (string_of_bool i);
          exec k ks (not i, s)
      | IPush (b, k), ks, s -> exec k ks (b, s)
      | ILambda (lam, k), ks, s -> exec k ks (lam, s)
      | IIf (tt, ff), ks, (test, s) ->
          print_if_cond test;
          if test then exec tt ks s else exec ff ks s
      | ICompNZ k, ks, (i, s) ->
          print_unary_instr "COMPNZ" (string_of_int i);
          exec k ks (i = 0, s)
      | IPair k, ks, (a, (b, s)) -> exec k ks ((a, b), s)
      | ISwap k, ks, (a, (b, s)) -> exec k ks (b, (a, s))
      | IUnpair k, ks, ((a, b), s) -> exec k ks (a, (b, s))
      | IFst k, ks, (p, s) -> exec k ks (fst p, s)
      | ISnd k, ks, (p, s) -> exec k ks (snd p, s)
      | IExec k, ks, (lam, (args, s)) ->
          let ret =
            match lam with
            | Lam (_, _, code) ->
                let ks = KReturn (s, KCons (k, ks)) in
                exec code ks (args, empty_stack)
            | LamRec (_, _, code) ->
                let ks = KReturn (s, KCons (k, ks)) in
                exec code ks (args, (lam, empty_stack))
          in
          ret
      | IApp k, ks, (lam, (arg, s)) ->
          let ret =
            match lam with
            | Lam (ty_in, ty_out, code) -> (
                match ty_in with
                | TyProd (_, ty2) ->
                    let lam' = Lam (ty2, ty_out, IPush (arg, IPair code)) in
                    exec k ks (lam', s))
            | LamRec (ty_in, ty_out, code) -> (
                match ty_in with
                | TyProd (_, ty2) ->
                    let lam' =
                      Lam
                        ( ty2,
                          ty_out,
                          IPush (arg, IPair (ILambda (lam, ISwap code))) )
                    in
                    exec k ks (lam', s))
          in
          ret
      | IDig (proof, k), ks, s ->
          let s = interpret_dig proof s in
          exec k ks s
      | IDup (proof, k), ks, s ->
          let x = interpret_dup proof s in
          exec k ks (x, s)
      | IDrop (proof, k), ks, s ->
          let s = interpret_drop proof s in
          exec k ks s
      | IDip (code, k), ks, (tip, s) ->
          let s' = exec code KNil s in
          exec k ks (tip, s')
      | ICons k, ks, (elt, (lst, s)) -> exec k ks (elt :: lst, s)
      | INil k, ks, s -> exec k ks ([], s)
      | IStream k, ks, (h, (t, s)) -> exec k ks (Stream (h, t), s)
      | IHead k, ks, (Stream (h, _), s) -> exec k ks (h, s)
      | ITail k, ks, (Stream (_, t), s) -> exec k ks (t, s)
    in

    exec i KNil stack

  let plus = Lam (TyProd (TyInt, TyInt), TyInt, IUnpair (IAdd IHalt))

  let%test _ =
    step (ILambda (plus, IApp (IExec IHalt))) (41, (1, empty_stack))
    = (42, empty_stack)

  let%test _ =
    step (IPush (2, IPush (1, IAdd IHalt))) empty_stack = (3, empty_stack)

  let%test _ =
    step (IPush (2, IPush (1, IPair (ISnd IHalt)))) empty_stack
    = (2, empty_stack)

  let%test _ =
    step (IPush (2, IPush (1, IPair (IFst IHalt)))) empty_stack
    = (1, empty_stack)

  let%test _ =
    step
      (IPush
         ( 4,
           IPush
             ( 3,
               IPush
                 (2, IPush (1, IDig (KPrefix (KPrefix (KPrefix KStack)), IHalt)))
             ) ))
      empty_stack
    = (4, (1, (2, (3, empty_stack))))

  let%test _ =
    step (IDrop (KPrefix KStack, IHalt)) (1, (42, empty_stack))
    = (42, empty_stack)

  let%test _ =
    step (IDip (IPush (42, IHalt), IHalt)) (1, empty_stack)
    = (1, (42, empty_stack))

  let%test _ =
    step (IDip (IDup (Dup_n_zero, IHalt), IHalt)) (1, (42, empty_stack))
    = (1, (42, (42, empty_stack)))

  (* Je ne suis pas sur d'avoir bien compris le type de drop  *)
  let%test _ =
    step (IDrop (KPrefix KStack, IHalt)) (42, empty_stack) = empty_stack

  let%test _ =
    step
      (IDip (IDrop (KPrefix KStack, IHalt), IHalt))
      (42, (1, ((), empty_stack)))
    = (42, ((), empty_stack))

  (* let%test _ = step ( succ *)

  let%test _ =
    step
      (IPush
         ( 4,
           IPush
             ( 3,
               IPush (2, IPush (1, IDig (KPrefix (KPrefix KStack), IAdd IHalt)))
             ) ))
      empty_stack
    = (4, (2, (4, empty_stack)))

  let%test _ = step (IUnpair IHalt) ((1, 2), empty_stack) = (1, (2, empty_stack))

  let%test _ =
    step (ICompNZ (IIf (IPush (1, IHalt), IPush (2, IHalt)))) (1, empty_stack)
    = (2, empty_stack)

  let%test _ =
    step (ICompNZ (IIf (IPush (1, IHalt), IPush (2, IHalt)))) (0, empty_stack)
    = (1, empty_stack)

  let dup0 = Dup_n_zero
  let dup1 = Dup_n_succ Dup_n_zero
  let dup2 = Dup_n_succ dup1
  let dup3 = Dup_n_succ dup2
  let dup4 = Dup_n_succ dup3
  let dup5 = Dup_n_succ dup4
  let prefix1 = KPrefix KStack
  let prefix2 = KPrefix prefix1
  let prefix3 = KPrefix prefix2
  let prefix4 = KPrefix prefix3

  let fact_rec =
    LamRec
      ( TyInt,
        TyInt,
        IDup
          ( dup0,
            ICompNZ
              (IIf
                 ( IPush (1, IDip (IDrop (prefix2, IHalt), IHalt)),
                   IDup
                     ( dup0,
                       IDup
                         ( dup0,
                           IPush
                             ( 1,
                               IMinus
                                 (IDup
                                    ( dup3,
                                      IExec
                                        (IDup
                                           ( dup1,
                                             IMult
                                               (IDip
                                                  (IDrop (prefix3, IHalt), IHalt))
                                           )) )) ) ) ) )) ) )

  let fact_rec_n n = IPush (n, ILambda (fact_rec, IExec IHalt))

  let%test _ = step (fact_rec_n 5) empty_stack = (120, empty_stack)
  let%test _ = step (fact_rec_n 6) empty_stack = (720, empty_stack)

  let fact_rec_pair =
    LamRec
      ( TyProd (TyInt, TyInt),
        TyInt,
        IUnpair
          (IDup
             ( dup1,
               ICompNZ
                 (IIf
                    ( IDip (IDrop (prefix2, IHalt), IHalt),
                      IDup
                        ( dup1,
                          IDup
                            ( dup0,
                              IPush
                                ( 1,
                                  IMinus
                                    (IDig
                                       ( prefix3,
                                         IDup
                                           ( dup4,
                                             IApp
                                               (IExec
                                                  (IDup
                                                     ( dup1,
                                                       IMult
                                                         (IDip
                                                            ( IDrop
                                                                (prefix3, IHalt),
                                                              IHalt )) ))) ) ))
                                ) ) ) )) )) )

  let fact_rec_n_pair n =
    IPush (n, IPush (1, ILambda (fact_rec_pair, IApp (IExec IHalt))))

  let%test _ = step (fact_rec_n_pair 5) empty_stack = (120, empty_stack)
  let%test _ = step (fact_rec_n_pair 6) empty_stack = (720, empty_stack)

  let one_two_three k = IPush ([ 1; 2; 3 ], k)

  let%test _ =
    step (one_two_three IHalt) empty_stack = ([ 1; 2; 3 ], empty_stack)

  let%test _ =
    step (one_two_three (IPush (0, ICons IHalt))) empty_stack
    = ([ 0; 1; 2; 3 ], empty_stack)

  let%test _ =
    step (INil (IPush (0, ICons IHalt))) empty_stack = ([ 0 ], empty_stack)
end
