module Types = struct
  type 'a ty_structure =
    | TyVar of 'a
    | TyInt
    | TyUnit
    | TyBool
    | TyArrow of ('a ty_structure list * 'a ty_structure)
    | TyProd of ('a ty_structure * 'a ty_structure)
    | TyList of 'a ty_structure
    | TyStream of 'a ty_structure

  and ty = int ty_structure
end

module Terms = struct
  open Types

  (* BNF
     v ::= lit | var | λ var . b
     c ::= (v v̄) | v
     b ::= let var = c in b | c

     env ::= empty | (var, atom)::env
  *)
  type var = string
  and atom = Var of var | Lit of lit

  and computation =
    | Val of atom
    | Lam of (var list * ty list * ty * body)
    | BinArith of (atom * arith_op * atom)
    | BinBool of (atom * bool_op * atom)
    | List of ty * atom list
    | Pair of atom * atom
    | Cons of atom * atom
    | Stream of atom * atom
    | Head of atom
    | Tail of atom
    | App of atom * atom
    | PartialApp of computation * atom list
    | If of atom * body * body
    | Fst of atom
    | Snd of atom
    | Not of atom
    | CompNZ of atom

  and body =
    | Computation of computation
    | Let of (var * ty * computation * body)
    | LetRec of (var * ty * computation * body)

  and arith_op = Add | Minus | Mult | Div
  and bool_op = Eq | NEq | And | Or
  and lit = Unit | Int of int | Bool of bool
  and term = body

  module Env = struct
    let empty = []

    let get env x =
      match List.assoc_opt x env with
      | Some v -> !v
      | None -> failwith (Printf.sprintf "Unbound variable : %s.\n" x)

    let bind env x v = (x, ref v) :: env

    let update env x v =
      match List.assoc_opt x env with
      | Some rv ->
          rv := v;
          env
      | None -> (x, ref v) :: env
  end

  module Values = struct
    type value =
      | VUnit
      | VInt of int
      | VBool of bool
      | VClo of (var list * body * (var * value ref) list)
      | VPair of value * value
      | VList of value list
      | VStream of value * value

    let vunit = VUnit
    let vint i = VInt i
  end

  let tunit = Lit Unit
  let tint i = Lit (Int i)
  let tvar x = Var x
  let tcomp c = Computation c
  let tapp t1 t2 = App (t1, t2)
  let tlam x var covar t = Lam ([ x ], [ var ], covar, t)
  let tval x = Val x
  let tx = tvar "x"
  let clet x ty c b = Let (x, ty, c, b)
  let clet_rec x ty c b = LetRec (x, ty, c, b)

  let tfun_unit_unit_id =
    tlam "x"
      (TyArrow ([ TyUnit ], TyUnit))
      (TyArrow ([ TyUnit ], TyUnit))
      (tcomp (tval tx))

  let cfun_unit_unit_id = tfun_unit_unit_id
  let tfun_unit_id = tlam "x" TyUnit TyUnit (tcomp (tval (tvar "x")))
  let cfun_unit_id = tfun_unit_id

  let capp_id_id =
    clet "id"
      (TyArrow ([ TyArrow ([ TyUnit ], TyUnit) ], TyArrow ([ TyUnit ], TyUnit)))
      tfun_unit_unit_id
      (tcomp (tapp (tvar "id") (tvar "id")))

  let cunit =
    clet "id"
      (TyArrow ([ TyUnit ], TyUnit))
      tfun_unit_id
      (tcomp (tapp (tvar "id") tunit))

  let capp_id_idy_unit =
    clet "u" TyUnit (tval tunit)
      (clet "id"
         (TyArrow
            ([ TyArrow ([ TyUnit ], TyUnit) ], TyArrow ([ TyUnit ], TyUnit)))
         tfun_unit_unit_id
         (clet "xy"
            (TyArrow ([ TyUnit ], TyUnit))
            tfun_unit_id
            (tcomp (tapp (tvar "xy") (tvar "u")))))

  (* let lam_minus =
   *   Lam ([ "x"; "y" ], Computation (BinArith (Var "x", Minus, Var "y"))) *)

  (* let one_minus_42 = tcomp (App (lam_minus, [ tint 1; tint 42 ])) *)
  let succ = tlam "x" TyInt TyInt (tcomp (BinArith (tx, Add, tint 1)))
  (* let succ_42 = Computation (App (succ, [ Lit (Int 42) ])) *)

  let capture_test =
    clet "x" TyInt
      (tval (tint 42))
      (clet "f"
         (TyArrow ([ TyUnit ], TyInt))
         (tlam "y" TyUnit TyInt (tcomp (tval (tvar "x"))))
         (clet "x" TyInt (tval (tint 0)) (tcomp (tapp (tvar "f") tunit))))

  let fact n =
    clet_rec "fact"
      (TyArrow ([ TyInt ], TyInt))
      (tlam "x" TyInt TyInt
         (Let
            ( "zx",
              TyBool,
              CompNZ (Var "x"),
              Computation
                (If
                   ( Var "zx",
                     Computation (Val (Lit (Int 1))),
                     Let
                       ( "predx",
                         TyInt,
                         BinArith (Var "x", Minus, Lit (Int 1)),
                         Let
                           ( "predf",
                             TyInt,
                             App (Var "fact", Var "predx"),
                             Computation (BinArith (Var "x", Mult, Var "predf"))
                           ) ) )) )))
      (Computation (App (Var "fact", Lit (Int n))))
end

module DeBruijnTerms = struct
  open Types

  module Env = struct
    let empty = []
    let bind env v = List.cons (ref v) env
    let get env i = !(List.nth env i)
    let update env i v = List.nth env i := v
  end

  type var = int
  and atom = Var of var | Lit of lit

  and computation =
    | Val of atom
    | Lam of int * ty list * ty * body
    | BinArith of (atom * arith_op * atom)
    | BinBool of (atom * bool_op * atom)
    | If of atom * body * body
    | App of (atom * atom)
    | PartialApp of computation * atom list
    | Pair of atom * atom
    | Cons of atom * atom
    | Stream of atom * atom
    | Head of atom
    | Tail of atom
    | List of ty * atom list
    | Fst of atom
    | Snd of atom
    | CompNZ of atom
    | Not of atom

  and body =
    | Let of (ty * computation * body)
    | LetRec of (ty * computation * body)
    | Computation of computation

  and lit = Unit | Int of int | Bool of bool
  and arith_op = Add | Minus | Mult | Div
  and bool_op = Eq | NEq | And | Or

  module Values = struct
    type value =
      | VInt of int
      | VBool of bool
      | VClo of (body * value ref list * value list)
      | VUnit
      | VPair of value * value
      | VList of value list
      | VStream of value * value
  end

  type atom_or_lam = Atom of atom | LamAtom of int * ty list * ty * body

  let rec fold_body bound_variables tys f acc b =
    match b with
    | Let (ty, c, b) ->
        let acc = fold_computation bound_variables tys f acc c in
        fold_body (succ bound_variables) (ty :: tys) f acc b
    | LetRec (ty, PartialApp (Lam (arity, tys', _, lb), params), b) ->
        let acc =
          fold_body
            (bound_variables + arity + 1)
            (ty :: (List.rev tys' @ tys))
            f acc lb
        in
        let acc = List.fold_left (fold_atom bound_variables tys f) acc params in
        let bv = succ bound_variables in
        let tys = ty :: tys in
        fold_body bv tys f acc b
    | LetRec (ty, Lam (arity, tys', _, lb), b) ->
        let acc =
          fold_body
            (bound_variables + arity + 1)
            (ty :: (List.rev tys' @ tys))
            f acc lb
        in
        let bv = succ bound_variables in
        let tys = ty :: tys in
        fold_body bv tys f acc b
    | Computation c -> fold_computation bound_variables tys f acc c
    | _ -> assert false

  and fold_computation bound_variables tys f acc c =
    match c with
    | Val a -> fold_atom bound_variables tys f acc a
    | Lam (arity, tys', _, b) ->
        fold_body (bound_variables + arity) (List.rev tys' @ tys) f acc b
    | BinArith (a1, _, a2) | BinBool (a1, _, a2) | App (a1, a2) ->
        let acc = fold_atom bound_variables tys f acc a1 in
        fold_atom bound_variables tys f acc a2
    | If (c, bt, bf) ->
        let acc = fold_atom bound_variables tys f acc c in
        let acc = fold_body bound_variables tys f acc bt in
        fold_body bound_variables tys f acc bf
    | PartialApp (a, xs) ->
        let acc = fold_computation bound_variables tys f acc a in
        List.fold_left
          (fun acc a -> fold_atom bound_variables tys f acc a)
          acc xs
    | Pair (a, b) | Cons (a, b) | Stream (a, b) ->
        let acc = f bound_variables tys acc a in
        f bound_variables tys acc b
    | List (_, xs) -> List.fold_left (f bound_variables tys) acc xs
    | Fst a | Snd a | CompNZ a | Not a | Head a | Tail a ->
        fold_atom bound_variables tys f acc a

  and fold_atom bound_variables tys f acc a =
    match a with _ -> f bound_variables tys acc a

  let rec map_body bound_variables f b =
    match b with
    | Let (ty, c, b) ->
        Let
          ( ty,
            map_computation bound_variables f c,
            map_body (bound_variables + 1) f b )
    | LetRec (ty, PartialApp (c, args), b) ->
        LetRec
          ( ty,
            PartialApp
              ( map_computation (bound_variables + 1) f c,
                List.map (map_atom bound_variables f) args ),
            map_body (bound_variables + 1) f b )
    | LetRec (ty, c, b) ->
        LetRec
          ( ty,
            map_computation (bound_variables + 1) f c,
            map_body (bound_variables + 1) f b )
    | Computation c -> Computation (map_computation bound_variables f c)

  and map_computation bound_variables f c =
    match c with
    | Val a -> Val (map_atom bound_variables f a)
    | Lam (ar, var, covar, b) ->
        let lam =
          match
            f bound_variables
              (LamAtom (ar, var, covar, map_body (bound_variables + ar) f b))
          with
          | LamAtom (ar, var, covar, b) -> Lam (ar, var, covar, b)
          | _ -> assert false
        in
        lam
    | BinArith (a1, op, a2) ->
        let a1 = map_atom bound_variables f a1 in
        let a2 = map_atom bound_variables f a2 in
        BinArith (a1, op, a2)
    | BinBool (a1, op, a2) ->
        let a1 = map_atom bound_variables f a1 in
        let a2 = map_atom bound_variables f a2 in
        BinBool (a1, op, a2)
    | If (c, bt, bf) ->
        let c = map_atom bound_variables f c in
        let bt = map_body bound_variables f bt in
        let bf = map_body bound_variables f bf in
        If (c, bt, bf)
    | App (a1, a2) ->
        let a1 = map_atom bound_variables f a1 in
        let a2 = map_atom bound_variables f a2 in
        App (a1, a2)
    | PartialApp (a, xs) ->
        let a = map_computation bound_variables f a in
        PartialApp (a, List.map (fun a -> map_atom bound_variables f a) xs)
    | Fst a -> Fst (map_atom bound_variables f a)
    | Snd a -> Snd (map_atom bound_variables f a)
    | Head a -> Head (map_atom bound_variables f a)
    | Tail a -> Tail (map_atom bound_variables f a)
    | CompNZ a -> CompNZ (map_atom bound_variables f a)
    | Not a -> Not (map_atom bound_variables f a)
    | Pair (a, b) ->
        Pair (map_atom bound_variables f a, map_atom bound_variables f b)
    | Cons (a, b) ->
        Cons (map_atom bound_variables f a, map_atom bound_variables f b)
    | Stream (a, b) ->
        Stream (map_atom bound_variables f a, map_atom bound_variables f b)
    | List (ty, xs) -> List (ty, List.map (map_atom bound_variables f) xs)

  and map_atom bound_variables f a =
    match f bound_variables (Atom a) with Atom a -> a | _ -> assert false

  let tvar x = Var x
  let tval x = Val x
  let tapp t1 t2 = App (t1, t2)
  let tlam a var covar t = Lam (a, [ var ], covar, t)
  let tcomp t = Computation t
  let tint i = Lit (Int i)
  let clet ty c b = Let (ty, c, b)
  let clet_rec ty c b = LetRec (ty, c, b)
  let int_ty = TyInt
  let arrow_ty t1 t2 = TyArrow ([ t1 ], t2)
  let prod_ty t1 t2 = TyProd (t1, t2)
  let tsnd x = Snd x
  let tfst x = Fst x

  let test_id =
    clet (arrow_ty int_ty int_ty)
      (tlam 1 int_ty int_ty (tcomp (tval (tvar 0))))
      (tcomp (tapp (tvar 0) (tint 1)))

  let tclo a ps = PartialApp (a, ps)

  let capp_id_one =
    clet int_ty
      (tval (tint 1))
      (clet (arrow_ty int_ty int_ty)
         (tlam 1 int_ty int_ty (tcomp (tval (tvar 0))))
         (tcomp (tapp (tvar 0) (tvar 1))))

  let t =
    clet int_ty
      (tval (tint 42))
      (clet (arrow_ty int_ty int_ty)
         (tlam 1 int_ty int_ty (tcomp (tval (tvar 1))))
         (tcomp (tapp (tvar 0) (tint 24))))

  let u =
    clet int_ty
      (tval (tint 41))
      (clet (arrow_ty int_ty int_ty)
         (BinArith (tvar 1, Add, tvar 0))
         (tcomp (tapp (tvar 1) (tint 1))))

  let v =
    clet int_ty
      (tval (tint 41))
      (clet int_ty (tval (tint 1)) (tcomp (BinArith (tvar 0, Add, tvar 1))))

  let v' = clet int_ty (tval (tint 1)) (tcomp (BinArith (tint 41, Add, tvar 0)))

  let w =
    clet (arrow_ty int_ty int_ty)
      (tlam 1 int_ty int_ty (tcomp (BinArith (tint 1, Add, tvar 0))))
      (tcomp (tapp (tvar 0) (tint 41)))

  let z =
    (*
       let x = 42 in
       let f : int -> int = fun (_ :int) -> x  in
       let g : int -> (int -> int) =
         fun (_ : int) -> f
       in
       let h : int -> int  = g 0 in
       h 4
    *)
    clet int_ty
      (tval (tint 42))
      (clet (arrow_ty int_ty int_ty)
         (tclo
            (tlam 1 (prod_ty int_ty int_ty) int_ty
               (clet int_ty
                  (tsnd (tvar 0))
                  (clet int_ty (tfst (tvar 1)) (tcomp (tval (tvar 0))))))
            [ tvar 0 ])
         (clet
            (arrow_ty int_ty (arrow_ty int_ty int_ty))
            (tclo
               (tlam 1
                  (prod_ty (arrow_ty int_ty int_ty) int_ty)
                  (arrow_ty int_ty int_ty)
                  (clet int_ty
                     (tsnd (tvar 0))
                     (clet int_ty (tfst (tvar 1)) (tcomp (tval (tvar 0))))))
               [ tvar 0 ])
            (clet (arrow_ty int_ty int_ty)
               (tapp (tvar 0) (tint 0))
               (tcomp (tapp (tvar 0) (tint 4))))))

  let z' =
    (* let x = 42 in
       let f _ = x in
       let g _ = g in
       let h = g 0 in
       h 4*)
    clet int_ty
      (tval (tint 42))
      (clet (arrow_ty int_ty int_ty)
         (tclo
            (tlam 2 (prod_ty int_ty int_ty) int_ty (tcomp (tval (tvar 1))))
            [ tvar 0 ])
         (clet
            (arrow_ty int_ty (arrow_ty int_ty int_ty))
            (tclo
               (tlam 2
                  (prod_ty (arrow_ty int_ty int_ty) int_ty)
                  (arrow_ty int_ty int_ty)
                  (tcomp (tval (tvar 1))))
               [ tvar 0 ])
            (clet (arrow_ty int_ty int_ty)
               (tapp (tvar 0) (tint 0))
               (tcomp (tapp (tvar 0) (tint 4))))))

  let fact n =
    clet_rec
      (TyArrow ([ TyInt ], TyInt))
      (tlam 1 TyInt TyInt
         (Let
            ( TyBool,
              CompNZ (Var 0),
              Computation
                (If
                   ( Var 0,
                     Computation (Val (Lit (Int 1))),
                     Let
                       ( TyInt,
                         BinArith (Var 1, Minus, Lit (Int 1)),
                         Let
                           ( TyInt,
                             App (Var 3, Var 0),
                             Computation (BinArith (Var 3, Mult, Var 0)) ) ) ))
            )))
      (Computation (App (Var 0, Lit (Int n))))
end
