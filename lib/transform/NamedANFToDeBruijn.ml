module S = Libanf.Syntax.Terms
module T = Libanf.Syntax.DeBruijnTerms
module VarMap = Map.Make (String)

let var_map = VarMap.empty

let rename bv_count var_map a =
  match a with
  | S.Var x -> T.Var (bv_count - VarMap.find x var_map - 1)
  | S.Lit (Int i) -> T.Lit (Int i)
  | S.Lit (Bool b) -> T.Lit (Bool b)
  | S.Lit Unit -> T.Lit Unit

let rec map_body bound_var_count var_map f b =
  match b with
  | S.Let (v, ty, c, b) ->
      T.Let
        ( ty,
          map_computation bound_var_count var_map f c,
          let var_map = VarMap.add v bound_var_count var_map in
          map_body (succ bound_var_count) var_map f b )
  | S.LetRec (v, ty, S.PartialApp (Lam (vs, var, covar, lb), params), b) ->
      T.LetRec
        ( ty,
          PartialApp
            ( (let bvc, var_map =
                 List.fold_left
                   (fun (bvc, vmap) v ->
                     let bvc' = bound_var_count + bvc in
                     (succ bvc, VarMap.add v bvc' vmap))
                   (0, var_map)
                   (vs @ [ v ])
               in
               T.Lam
                 ( bvc - 1,
                   var,
                   covar,
                   map_body (bound_var_count + bvc) var_map f lb )),
              List.map (map_atom bound_var_count var_map f) params ),
          let var_map = VarMap.add v bound_var_count var_map in
          map_body (succ bound_var_count) var_map f b )
  | S.LetRec (v, ty, S.Lam (vs, var, covar, lb), b) ->
      T.LetRec
        ( ty,
          (let bvc, var_map =
             List.fold_left
               (fun (bvc, vmap) v ->
                 let bvc' = bound_var_count + bvc in
                 (succ bvc, VarMap.add v bvc' vmap))
               (0, var_map)
               (vs @ [ v ])
           in
           T.Lam
             (bvc - 1, var, covar, map_body (bound_var_count + bvc) var_map f lb)),
          let var_map = VarMap.add v bound_var_count var_map in
          map_body (succ bound_var_count) var_map f b )
  | S.Computation c ->
      T.Computation (map_computation bound_var_count var_map f c)
  | S.LetRec _ -> failwith "Unsupported recursive values"

and map_computation bound_var_count var_map f c =
  match c with
  | S.(Lam (vs, var, covar, b)) ->
      let bvc, var_map =
        List.fold_left
          (fun (bvc, vmap) v ->
            let bvc' = bound_var_count + bvc in
            (succ bvc, VarMap.add v bvc' vmap))
          (0, var_map) vs
      in
      T.Lam (bvc, var, covar, map_body (bvc + bound_var_count) var_map f b)
  | S.Val a -> T.Val (map_atom bound_var_count var_map f a)
  | S.BinArith (a1, Add, a2) ->
      let a1 = map_atom bound_var_count var_map f a1 in
      let a2 = map_atom bound_var_count var_map f a2 in
      T.BinArith (a1, Add, a2)
  | S.BinArith (a1, Minus, a2) ->
      let a1 = map_atom bound_var_count var_map f a1 in
      let a2 = map_atom bound_var_count var_map f a2 in
      T.BinArith (a1, Minus, a2)
  | S.BinArith (a1, Mult, a2) ->
      let a1 = map_atom bound_var_count var_map f a1 in
      let a2 = map_atom bound_var_count var_map f a2 in
      T.BinArith (a1, Mult, a2)
  | S.BinArith (a1, Div, a2) ->
      let a1 = map_atom bound_var_count var_map f a1 in
      let a2 = map_atom bound_var_count var_map f a2 in
      T.BinArith (a1, Div, a2)
  | S.BinBool (a1, Eq, a2) ->
      let a1 = map_atom bound_var_count var_map f a1 in
      let a2 = map_atom bound_var_count var_map f a2 in
      T.BinBool (a1, Eq, a2)
  | S.BinBool (a1, NEq, a2) ->
      let a1 = map_atom bound_var_count var_map f a1 in
      let a2 = map_atom bound_var_count var_map f a2 in
      T.BinBool (a1, NEq, a2)
  | S.BinBool (a1, And, a2) ->
      let a1 = map_atom bound_var_count var_map f a1 in
      let a2 = map_atom bound_var_count var_map f a2 in
      T.BinBool (a1, And, a2)
  | S.BinBool (a1, Or, a2) ->
      let a1 = map_atom bound_var_count var_map f a1 in
      let a2 = map_atom bound_var_count var_map f a2 in
      T.BinBool (a1, Or, a2)
  | S.If (c, bt, bf) ->
      let c = map_atom bound_var_count var_map f c in
      let bt = map_body bound_var_count var_map f bt in
      let bf = map_body bound_var_count var_map f bf in
      T.If (c, bt, bf)
  | S.App (a1, a2) ->
      let a1 = map_atom bound_var_count var_map f a1 in
      let a2 = map_atom bound_var_count var_map f a2 in
      T.App (a1, a2)
  | S.PartialApp (c, args) ->
      let c = map_computation bound_var_count var_map f c in
      let args = List.map (map_atom bound_var_count var_map f) args in
      T.PartialApp (c, args)
  | S.Pair (x, y) ->
      T.Pair
        ( map_atom bound_var_count var_map f x,
          map_atom bound_var_count var_map f y )
  | S.Stream (x, y) ->
      T.Stream
        ( map_atom bound_var_count var_map f x,
          map_atom bound_var_count var_map f y )
  | S.Cons (x, y) ->
      T.Cons
        ( map_atom bound_var_count var_map f x,
          map_atom bound_var_count var_map f y )
  | S.List (ty, xs) ->
      T.List (ty, List.map (map_atom bound_var_count var_map f) xs)
  | S.Fst a -> T.Fst (map_atom bound_var_count var_map f a)
  | S.Snd a -> T.Snd (map_atom bound_var_count var_map f a)
  | S.Head a -> T.Head (map_atom bound_var_count var_map f a)
  | S.Tail a -> T.Tail (map_atom bound_var_count var_map f a)
  | S.CompNZ a -> T.CompNZ (map_atom bound_var_count var_map f a)
  | S.Not a -> T.Not (map_atom bound_var_count var_map f a)

and map_atom bound_var_count var_map f = f bound_var_count var_map

let encode_variables = map_body 0 var_map rename
