open CBPVToCBV
open CBVToMimi
module S = Libcbpv_syntax.Syntax.Terms
module STypes = Libcbpv_syntax.Syntax.Types

let compile (p : STypes.tycomp S.term) = cbpv_comp_to_cbv p |> translate

let return_one =
  let open S in
  tlet "one"
    (TypedTerm (ty_int, tint 1))
    (tlet "a"
       (TypedTerm
          ( ty_u (ty_arrow ty_int ty_int),
            tthunk
              (TypedTerm (ty_arrow ty_int ty_int, tfun "x" (treturn (t "one"))))
          ))
       (!!(t "a") @@! tint 42))

let fact n =
  let open S in
  tlet_rec "fact" (ty_arrow ty_int ty_int)
    (TypedTerm
       ( ty_arrow ty_int ty_int,
         tfun "x"
           (tif
              (Prim (CompNZ (t "x")))
              (treturn (tint 1))
              ((TypedTerm (ty_f ty_int, t "x" -! tint 1 |>! !!(t "fact"))
               =: "predf")
                 (treturn (t "x" *! t "predf")))) ))
    (tint n |>! !!(t "fact"))

let example_stream =
  let open S in
  App
    ( TypedTerm
        ( ty_arrow ty_int (ty_f (ty_stream ty_int)),
          Rec
            ( "ones",
              TypedTerm
                ( ty_arrow ty_int (ty_f (ty_stream ty_int)),
                  Fun
                    ( "_time",
                      Return
                        (Fby
                           ( Const (Int 1),
                             Thunk
                               (TypedTerm
                                  ( ty_arrow ty_int (ty_f (ty_stream ty_int)),
                                    Fun
                                      ( "_n",
                                        App
                                          ( Force (Var "ones"),
                                            Prim
                                              (Plus (Var "_n", Const (Int 1)))
                                          ) ) )) )) ) ) ) ),
      Const (Int 1) )

let%test _ = mimi_eval (cbpv_comp_to_cbv return_one) |> fst = 1
let%test _ = mimi_eval (cbpv_comp_to_cbv (fact 5)) |> fst = 120

let%test _ =
  let open Libmimi.Syntax.Cont in
  let x =
    match mimi_eval (cbpv_comp_to_cbv example_stream) with
    | Stream (x, _), _ -> x
  in
  x = 1
