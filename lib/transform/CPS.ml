(* open Libcbv
 * open Syntax
 * open Terms *)

let unique =
  let acc = ref 0 in
  fun () ->
    let x = !acc in
    incr acc;
    x

(* let fresh_var x = x ^ string_of_int (unique ())
 *
 * let k_of t =
 *   let k = fresh_var "k" in
 *   tlam k (tapp (tvar k) t)
 *
 * let rec cps_translate t =
 *   match t with
 *   | Lit _ | Var _ -> k_of t
 *   | Lam (x, ty, t) -> k_of (tlam x ty (cps_translate t))
 *   | Pair (x, y) -> k_of (Pair (k_of x, k_of y))
 *   | Fst x -> cps_translate_proj proj_fst x
 *   | Snd x -> cps_translate_proj proj_snd x
 *   | App (t1, t2) ->
 *       let ta = cps_translate t1 in
 *       let tb = cps_translate t2 in
 *       let va = fresh_var "a" in
 *       let vb = fresh_var "b" in
 *       let k = fresh_var "k" in
 *       tlam k
 *         (tapp ta
 *            (tlam va
 *               (tapp tb (tlam vb (tapp (tvar va) (tapp (tvar vb) (tvar k)))))))
 *
 * and proj_fst x = Fst x
 * and proj_snd x = Snd x
 *
 * and cps_translate_proj proj x =
 *   let tx = cps_translate x in
 *   let vx = fresh_var "x" in
 *   let k = fresh_var "k" in
 *   tlam k (tapp tx (tlam vx (tapp (tvar k) (proj (tvar vx)))))
 *
 * let cps_of = cps_translate *)
