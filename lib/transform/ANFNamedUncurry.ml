(* Source Language *)
module S = Libanf.Syntax.Terms

(* Target Language *)
module T = Libanf.Syntax.Terms
open Libanf.Syntax.Types

let rec _uncurry env t =
  match t with
  | S.Let (x, ty, c, b) ->
      let ty, c =
        match uncurry_comp env c with
        | T.Lam (_, var, covar, _) as c -> (TyArrow (var, covar), c)
        | T.Val (Var y) -> (T.Env.get env y, c)
        | T.Tail (Var x) as c ->
            (TyArrow ([ TyUnit; TyInt ], T.Env.get env x), c)
        | T.App (Var i, _) as c -> (type_of_partial_app env i, c)
        | T.PartialApp (Val (Var i), _) as c -> (type_of_partial_app env i, c)
        | c -> (ty, c)
      in
      let b = _uncurry (T.Env.bind env x ty) b in
      T.Let (x, ty, c, b)
  | S.LetRec (x, _, Lam (ids, var, covar, lb), b) ->
      let ty, c =
        match uncurry_lambda (Some x) env ids var covar lb with
        | T.Lam (_, var, covar, _) as c -> (TyArrow (var, covar), c)
        | _ -> assert false
      in
      let env = T.Env.bind env x ty in
      let b = _uncurry env b in
      T.LetRec (x, ty, c, b)
  | S.Computation c -> T.Computation (uncurry_comp env c)
  | S.LetRec _ -> failwith "Only lambdas can be recursive"

and type_of_partial_app env i =
  match T.Env.get env i with
  | TyArrow ([ _ ], u) -> u
  | TyArrow (_ :: ts, u) -> TyArrow (ts, u)
  | _ -> assert false

and uncurry_comp env c =
  match c with
  | S.Lam (base_arity, var, covar, b) ->
      uncurry_lambda None env base_arity var covar b
  | S.App ((Var i as v), b) ->
      if arity_by_type env i = 1 then T.App (v, b)
      else T.PartialApp (T.Val v, [ b ])
  | S.If (c, bt, bf) -> T.If (c, _uncurry env bt, _uncurry env bf)
  | S.Val _ | S.BinArith _ | S.BinBool _ | S.Snd _ | S.Fst _ | S.CompNZ _
  | S.Not _ | S.Pair _ | S.List _ | S.Cons _ | S.Stream _ | S.Head _ | S.Tail _
    ->
      c
  | S.App _ | S.PartialApp _ -> assert false
(* by typing *)

and arity_by_type env var =
  match T.Env.get env var with TyArrow (args, _) -> List.length args | _ -> 0

and uncurry_lambda rec_name env bvs var covar body =
  let env = List.fold_left2 T.Env.bind env bvs var in
  let bvs', var', covar, body =
    process_lambdas rec_name bvs var covar env body
  in
  let bvs = bvs' in
  let var = var' in
  T.(Lam (bvs, var, covar, body))

and process_lambdas rec_name bvs_acc var_acc covar env body =
  match (body, rec_name) with
  | S.(Computation (Lam (xs, tys, covar, b))), _ ->
      let env =
        List.fold_right2 (fun x ty env -> S.Env.bind env x ty) xs tys env
      in
      process_lambdas rec_name (bvs_acc @ xs) (var_acc @ tys) covar env b
  | _, Some name ->
      let env = T.Env.bind env name (TyArrow (var_acc, covar)) in
      let body = _uncurry env body in
      let covar = curry_type_of_body covar env body in
      (bvs_acc, var_acc, covar, body)
  | _, None ->
      let body = _uncurry env body in
      let covar = curry_type_of_body covar env body in
      (bvs_acc, var_acc, covar, body)

and curry_type_of_body old env body =
  match body with
  | T.Let (x, ty, _, b) | T.LetRec (x, ty, _, b) ->
      curry_type_of_body old (T.Env.bind env x ty) b
  | T.Computation (Val (Var x)) -> T.Env.get env x
  | T.Computation (Tail (Var x)) -> TyArrow ([ TyUnit; TyInt ], T.Env.get env x)
  | T.Computation (App (Var i, _)) -> type_of_partial_app env i
  | T.Computation (PartialApp (Val (Var i), _)) -> type_of_partial_app env i
  | _ -> old

let uncurry = _uncurry T.Env.empty
