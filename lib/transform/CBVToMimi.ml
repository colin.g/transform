open Libcbv.Typechecker
open ANFConversion
open ANFLambdaLifting
open NamedANFToDeBruijn
open ANFNamedUncurry
open ANFToMimi
module Mimi = Libmimi

let normal_form p =
  elaborate p |> to_anf |> uncurry |> encode_variables |> lambda_lift

let translate p = compile (normal_form p)
let mimi_typcheck p = translate p |> Mimi.Typechecker.(elaborate base)

let mimi_eval p =
  let open Mimi.Typechecker in
  mimi_typcheck p |> function
  | J { kinstr; _ } ->
      let code = kinstr IHalt in
      Mimi.Semantics.CStackMachine.(step code empty_stack) |> Obj.magic

let one_two_three =
  Libcbv.Syntax.Terms.(List [ Lit (Int 1); Lit (Int 2); Lit (Int 3) ])

let prog =
  "let (nil_int:int list) = [] in\n\
  \   let one_t (u:unit) :int = 1 in\n\
  \   let step\n\
  \     (s: (int list) * (unit -> int)) : ((int list) * (unit -> int)) =\n\
  \      let (h : int list) = fst s in\n\
  \      let (b : unit -> int) = snd s in\n\
  \       let (v:int) = b () in (v::h,b) in\n\
  \   let (p: (int list ) *(unit -> int)) = step (nil_int, one_t) in\n\
  \   let (p: (int list ) *(unit -> int)) = step p in\n\
  \   let (p: (int list ) *(unit -> int)) = step p in\n\
  \   fst p"

let fact_5 =
  "let  rec fact (x:int) : int = if is_zero x then 1 else x *  fact (x - 1 ) \
   in fact 5"

let fact_unit_5 =
  "let rec (fact: unit -> (int -> int)) =\n\
  \  fun (u:unit) ->\n\
  \  fun (x:int) -> if is_zero x then 1 else x *\n\
  \    fact () (x - 1 ) in fact () 5"

let stream_one =
  "let rec (ones: unit -> int -> (int stream)) = fun (u:unit) -> fun (i:int) \
   -> stream 1 ones in     (head ((tail (ones () 0)) () 1)) :: (head (ones () \
   0)) ::[] "

let a =
  "let (f : unit -> (int -> int)) =\n\
  \      let (a: unit -> (int -> int)) = fun (u:unit) -> fun (i:int) -> i in\n\
  \     fun (u:unit) -> a () \n\
  \    in f () 5"

let mfact =
  {|(let rec (fact : unit -> (int -> (int -> int)))=
    fun (u : unit) -> fun (x : int) ->
     if (x  =  0) then (fun (time : int) ->  1)
     else
       (fun (time2 : int) ->
       (let (y : int) = (fun (time3 : int) ->  x  -  1) time2 in
        fun (time4 : int) ->
          (let (z : int) =  fact () y time4 in
           fun (time5 : int) ->  x  *  z) time4) time2) in
   fact ()) 5 0|}

let nat_stream =
  {|
(let rec (nat : unit -> (int -> int stream)) =
  fun (u : unit) -> fun (time0 : int) ->
    stream 0
      (fun (u : unit) -> fun (n : int) -> (fun (u : unit) ->
        let rec (next : unit -> (int -> int stream)) =
         fun (u : unit) -> fun  (time2 : int) ->
           (let (n : int stream) =  nat () time2 in
             fun (time3 : int) ->
              (let (v : int) = (fun (time4 : int) ->
              let (otime : int) = (fun (i : int) ->  i  -  1) time4 in
               (let rec (nth : unit -> ((int stream) -> (int -> (int -> int)))) =
                    fun (u : unit) -> fun (xs : int stream) -> fun (i : int) -> fun (time14 : int) ->
                     if (i  =  0) then  head xs else
                       let (mtail : unit -> (int -> int stream)) = tail xs in
                       let (t: int stream) = mtail () time14 in
                       nth () t (i  -  1) (time14  +  1) in
                  nth ()) n otime 0) time3 in
                  fun (time5 : int) ->
                        stream (v  +  1)
                          (fun (u : unit) -> fun (n2 : int) ->  next () (n2  +  1))) time3)
                      time2 in
              next ()) () (n  +  1)) in nat ()) 0|}

let proj = {|
(fun (i:int) -> (fst(true,1),snd (true,1))) 0
|}

let if_cond =
  {|
(if true then (fun (i:int) -> false) else (fun (i:int)  -> true)) 0
|}

let%test _ = mimi_eval one_two_three |> fst = [ 1; 2; 3 ]
let%test _ = mimi_eval Libcbv.Syntax.Terms.s |> fst = 42
let%test _ = mimi_eval Libcbv.Syntax.Terms.p |> fst = 42
let%test _ = Libcbv.IO.parse_program fact_unit_5 |> mimi_eval |> fst = 120
let%test _ = Libcbv.IO.parse_program fact_5 |> mimi_eval |> fst = 120
let%test _ = Libcbv.IO.parse_program stream_one |> mimi_eval |> fst = [ 1; 1 ]
let%test _ = Libcbv.IO.parse_program prog |> mimi_eval |> fst = [ 1; 1; 1 ]
let%test _ = Libcbv.IO.parse_program mfact |> mimi_eval |> fst = 120
let%test _ = Libcbv.IO.parse_program proj |> mimi_eval |> fst = (true, 1)
let%test _ = Libcbv.IO.parse_program if_cond |> mimi_eval |> fst = false
