module S = Libcbpv_syntax.Syntax.Terms
module STypes = Libcbpv_syntax.Syntax.Types
module T = Libcbv.Syntax.Terms
module TTypes = Libcbv.Syntax.Types

let empty = []
let bind_val env x v = List.cons (x, v) env
let get env x = List.assoc x env

type env = (string * STypes.tyval S.term) list

let rec subst : type a. string -> STypes.tyval S.term -> a S.term -> a S.term =
 fun id v t ->
  match t with
  | S.Var x when id = x -> v
  | S.Fun (x, c) when x <> id -> Fun (x, subst id v c)
  | S.Rec (x, c) when x <> id -> Rec (x, subst id v c)
  | S.App (a, b) -> S.App (subst id v a, subst id v b)
  | S.Pair (a, b) -> S.Pair (subst id v a, subst id v b)
  | S.Let (x, b, c) when x <> id -> S.Let (x, subst id v b, subst id v c)
  | S.Let (x, b, c) -> S.Let (x, subst id v b, c)
  | S.To (b, x, c) when x <> id -> S.To (subst id v b, x, subst id v c)
  | S.To (b, x, c) -> S.To (subst id v b, x, c)
  | S.Thunk t -> S.Thunk (subst id v t)
  | S.Force t -> S.Force (subst id v t)
  | S.Return t -> S.Return (subst id v t)
  | S.IfThenElse (c, bt, bf) ->
      S.IfThenElse (subst id v c, subst id v bt, subst id v bf)
  | S.Prim (Plus (x, y)) -> S.Prim (Plus (subst id v x, subst id v y))
  | S.Prim (Minus (x, y)) -> S.Prim (Minus (subst id v x, subst id v y))
  | S.Prim (Mult (x, y)) -> S.Prim (Mult (subst id v x, subst id v y))
  | S.Prim (Div (x, y)) -> S.Prim (Div (subst id v x, subst id v y))
  | S.Prim (Eq (x, y)) -> S.Prim (Eq (subst id v x, subst id v y))
  | S.Prim (NEq (x, y)) -> S.Prim (NEq (subst id v x, subst id v y))
  | S.Prim (AndB (x, y)) -> S.Prim (AndB (subst id v x, subst id v y))
  | S.Prim (OrB (x, y)) -> S.Prim (OrB (subst id v x, subst id v y))
  | S.Prim (Fst x) -> S.Prim (Fst (subst id v x))
  | S.Prim (Snd x) -> S.Prim (Snd (subst id v x))
  | S.Prim (HdS x) -> S.Prim (HdS (subst id v x))
  | S.Prim (TlS x) -> S.Prim (TlS (subst id v x))
  | S.Prim (HdL x) -> S.Prim (HdL (subst id v x))
  | S.Prim (TlL x) -> S.Prim (TlL (subst id v x))
  | S.Prim (CompNZ x) -> S.Prim (CompNZ (subst id v x))
  | S.Prim (Not x) -> S.Prim (Not (subst id v x))
  | S.Prim (Cons (x, y)) -> S.Prim (Cons (subst id v x, subst id v y))
  | S.Fby (x, y) -> S.Fby (subst id v x, subst id v y)
  | S.Const _ | S.Var _ | S.Fun _ | S.Rec _ -> t
  | S.TypedTerm (ty, t) -> S.TypedTerm (ty, subst id v t)

let rec translate_ty : STypes.typ -> TTypes.ty =
 fun ty ->
  match ty with
  | STypes.TyApp (("unit", 0), []) -> TTypes.TyUnit
  | STypes.TyApp (("int", 0), []) -> TTypes.TyInt
  | STypes.TyApp (("bool", 0), []) -> TTypes.TyBool
  | STypes.TyApp (("->", 2), [ a; b ]) ->
      TTypes.TyArrow ([ translate_ty a ], translate_ty b)
  | STypes.TyApp (("*", 2), [ a; b ]) ->
      TTypes.TyProd (translate_ty a, translate_ty b)
  | STypes.TyApp (("F", 1), [ a ]) -> translate_ty a
  | STypes.TyApp (("U", 1), [ a ]) ->
      TTypes.(TyArrow ([ TyUnit ], translate_ty a))
  | STypes.TyApp (("stream", 1), [ ty ]) -> TTypes.TyStream (translate_ty ty)
  | _ -> failwith "CBPV type cannot be tranlsated. "

let rec cbpv_val_to_cbv (v : STypes.tyval S.term) : T.term =
  match v with
  | S.Var x -> T.Var x
  | S.Const (Int i) -> T.Lit (Int i)
  | S.Const (Bool b) -> T.Lit (Bool b)
  | S.Const Nil -> T.List []
  | S.Const Unit -> T.Lit Unit
  | S.Pair (a, b) -> T.Pair (cbpv_val_to_cbv a, cbpv_val_to_cbv b)
  | S.Thunk c -> T.(Lam ("()", TTypes.TyUnit, cbpv_comp_to_cbv c))
  | S.TypedTerm (ty, t) ->
      let ty = translate_ty ty in
      let t = cbpv_val_to_cbv t in
      T.TypedTerm (t, ty)
  | S.Fby (a, t) -> T.(Stream (cbpv_val_to_cbv a, cbpv_val_to_cbv t))
  | S.Prim (Fst t) -> T.Fst (cbpv_val_to_cbv t)
  | S.Prim (Snd t) -> T.Snd (cbpv_val_to_cbv t)
  | S.Prim (Plus (x, y)) ->
      T.BinArith (cbpv_val_to_cbv x, Add, cbpv_val_to_cbv y)
  | S.Prim (Minus (x, y)) ->
      T.BinArith (cbpv_val_to_cbv x, Minus, cbpv_val_to_cbv y)
  | S.Prim (Mult (x, y)) ->
      T.BinArith (cbpv_val_to_cbv x, Mult, cbpv_val_to_cbv y)
  | S.Prim (Div (x, y)) -> T.BinArith (cbpv_val_to_cbv x, Div, cbpv_val_to_cbv y)
  | S.Prim (Eq (x, y)) -> T.BinBool (cbpv_val_to_cbv x, Eq, cbpv_val_to_cbv y)
  | S.Prim (NEq (x, y)) -> T.BinBool (cbpv_val_to_cbv x, NEq, cbpv_val_to_cbv y)
  | S.Prim (AndB (x, y)) -> T.BinBool (cbpv_val_to_cbv x, And, cbpv_val_to_cbv y)
  | S.Prim (OrB (x, y)) -> T.BinBool (cbpv_val_to_cbv x, Or, cbpv_val_to_cbv y)
  | S.Prim (Cons (x, y)) -> T.Cons (cbpv_val_to_cbv x, cbpv_val_to_cbv y)
  | S.Prim (CompNZ x) -> T.CompNZ (cbpv_val_to_cbv x)
  | S.Prim (Not x) -> T.Not (cbpv_val_to_cbv x)
  | S.Prim (HdS x) -> T.Head (cbpv_val_to_cbv x)
  | S.Prim (TlS x) -> T.Tail (cbpv_val_to_cbv x)
  | S.Prim (HdL _) | S.Prim (TlL _) -> failwith "NYI"

and cbpv_comp_to_cbv (t : STypes.tycomp S.term) : T.term =
  match t with
  | S.TypedTerm (TyApp (("->", 2), [ ty; _ ]), Fun (x, b)) ->
      T.Lam (x, translate_ty ty, cbpv_comp_to_cbv b)
  | S.App (a, b) -> T.App (cbpv_comp_to_cbv a, cbpv_val_to_cbv b)
  | S.Let (id, ((Const _ | Var _) as b), c) ->
      let c = subst id b c in
      cbpv_comp_to_cbv c
  | S.Let (id, TypedTerm (ty, b), c) ->
      let b = cbpv_val_to_cbv b in
      let c = cbpv_comp_to_cbv c in
      let ty = translate_ty ty in
      T.(Let (id, ty, b, c))
  | S.TypedTerm (ty, Rec (id, b)) ->
      let b = cbpv_comp_to_cbv b in
      let ty = translate_ty ty in
      T.(
        LetRec
          ( id,
            TTypes.(TyArrow ([ TyUnit ], ty)),
            T.Lam ("()", TyUnit, b),
            App (Var id, Lit Unit) ))
  | S.To (TypedTerm (TyApp (("F", 1), [ ty ]), a), id, b) ->
      T.Let (id, translate_ty ty, cbpv_comp_to_cbv a, cbpv_comp_to_cbv b)
  | S.Force a -> T.App (cbpv_val_to_cbv a, Lit Unit)
  | S.Return a -> cbpv_val_to_cbv a
  | S.IfThenElse (c, bt, bf) ->
      T.If (cbpv_val_to_cbv c, cbpv_comp_to_cbv bt, cbpv_comp_to_cbv bf)
  | S.TypedTerm (ty, t) ->
      let ty = translate_ty ty in
      let t = cbpv_comp_to_cbv t in
      T.TypedTerm (t, ty)
  | S.Let _ | S.Fun _ | S.To _ | S.Rec _ ->
      failwith "Untyped term not translatable"
  | S.Prim _ -> .

let%test _ =
  subst "one" (Const (Int 1))
    S.(
      Let
        ( "x",
          TypedTerm
            ( ty_u (ty_product ty_int (ty_f ty_int)),
              Thunk (Return (Pair (Var "one", Const (Int 42)))) ),
          Return (Var "x") ))
  = S.(
      Let
        ( "x",
          TypedTerm
            ( ty_u (ty_product ty_int (ty_f ty_int)),
              Thunk (Return (Pair (Const (Int 1), Const (Int 42)))) ),
          Return (Var "x") ))

let%test _ =
  subst "one" (Const (Int 1))
    S.(
      Let
        ( "x",
          TypedTerm
            ( ty_u (ty_product ty_int (ty_f ty_int)),
              Thunk (Return (Pair (Const (Int 42), Var "one"))) ),
          Return (Var "x") ))
  = S.(
      Let
        ( "x",
          TypedTerm
            ( ty_u (ty_product ty_int (ty_f ty_int)),
              Thunk (Return (Pair (Const (Int 42), Const (Int 1)))) ),
          Return (Var "x") ))

let%test _ =
  subst "one" (Const (Int 1))
    S.(
      Let
        ( "x",
          TypedTerm
            ( ty_u (ty_arrow ty_int (ty_f ty_int)),
              Thunk
                (TypedTerm
                   (ty_arrow ty_int (ty_f ty_int), Fun ("y", Return (Var "one"))))
            ),
          Return (Var "x") ))
  = S.(
      Let
        ( "x",
          TypedTerm
            ( ty_u (ty_arrow ty_int (ty_f ty_int)),
              Thunk
                (TypedTerm
                   ( ty_arrow ty_int (ty_f ty_int),
                     Fun ("y", Return (Const (Int 1))) )) ),
          Return (Var "x") ))

let%test _ =
  subst "one" (Const (Int 1))
    S.(
      Let
        ( "x",
          TypedTerm
            ( ty_u (ty_arrow ty_int (ty_f ty_int)),
              Thunk
                (TypedTerm
                   ( ty_arrow ty_int (ty_f ty_int),
                     Fun ("one", Return (Var "one")) )) ),
          Return (Var "x") ))
  = S.(
      Let
        ( "x",
          TypedTerm
            ( ty_u (ty_arrow ty_int (ty_f ty_int)),
              Thunk
                (TypedTerm
                   ( ty_arrow ty_int (ty_f ty_int),
                     Fun ("one", Return (Var "one")) )) ),
          Return (Var "x") ))

let%test _ =
  subst "one" (Const (Int 1))
    S.(
      Let
        ( "one",
          TypedTerm
            ( ty_u (ty_arrow ty_int (ty_f ty_int)),
              Thunk
                (TypedTerm
                   (ty_arrow ty_int (ty_f ty_int), Fun ("y", Return (Var "one"))))
            ),
          Return (Var "one") ))
  = S.(
      Let
        ( "one",
          TypedTerm
            ( ty_u (ty_arrow ty_int (ty_f ty_int)),
              Thunk
                (TypedTerm
                   ( ty_arrow ty_int (ty_f ty_int),
                     Fun ("y", Return (Const (Int 1))) )) ),
          Return (Var "one") ))

let%test _ =
  subst "one" (Const (Int 1))
    S.(
      Let
        ( "x",
          TypedTerm
            ( ty_u (ty_arrow ty_int (ty_f ty_int)),
              Thunk
                (TypedTerm
                   (ty_arrow ty_int (ty_f ty_int), Fun ("y", Return (Var "one"))))
            ),
          Return (Var "one") ))
  = S.(
      Let
        ( "x",
          TypedTerm
            ( ty_u (ty_arrow ty_int (ty_f ty_int)),
              Thunk
                (TypedTerm
                   ( ty_arrow ty_int (ty_f ty_int),
                     Fun ("y", Return (Const (Int 1))) )) ),
          Return (Const (Int 1)) ))
