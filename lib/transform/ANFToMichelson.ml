(* Source Language *)
module S = Libanf.Syntax.DeBruijnTerms
module STy = Libanf.Syntax.Types

(* Target Language *)
module T = Libmichelson.Syntax

let rec translate_ty t =
  match t with
  | STy.TyUnit -> T.t_unit
  | STy.TyInt -> T.t_int
  | STy.TyBool -> T.t_bool
  | STy.TyArrow (a, b) -> T.t_lambda (translate_ty_lam_args a) (translate_ty b)
  | STy.TyProd (a, b) -> T.t_pair (translate_ty a) (translate_ty b)
  | STy.TyList t -> T.t_list (translate_ty t)
  | _ -> assert false

and translate_ty_lam_args ts =
  match ts with
  | [] -> assert false
  | [ t ] -> translate_ty t
  | [ t1; t2 ] -> T.t_pair (translate_ty t1) (translate_ty t2)
  | t :: ts -> T.t_pair (translate_ty t) (translate_ty_lam_args ts)

let node_to_list n =
  match n with Tezos_micheline.Micheline.Seq (_, ns) -> ns | _ -> [ n ]

let rec translate depth anf =
  match anf with
  | S.Let (_, a, b) ->
      let drops, n = translate depth b in
      let b = node_to_list n in
      let a = translate_comp depth a in
      (drops + 1, T.(seq (a @ b)))
  | S.Computation c -> (0, T.seq (translate_comp depth c))
  | S.LetRec (_, Lam (arity, var, covar, c), b) ->
      let args = process_args arity in
      let ty_args = translate_ty_lam_args var in
      let ty_ret = translate_ty covar in
      let drops, n = translate depth b in
      let b = node_to_list n in
      let c_drops, cn = translate 0 c in
      let c = node_to_list cn in
      ( drops + 1,
        T.(
          seq
            (i_lambda_rec ty_args ty_ret
               (seq (args @@ c @ [ i_dip [ i_drop (arity + 1 + c_drops) ] ]))
            :: b)) )
  | _ -> failwith "Recursive values are unsupported.\n"

and translate_comp depth c : (int, string) Tezos_micheline.Micheline.node list =
  match c with
  | S.Val v -> [ translate_val depth v ]
  | S.(Lam (arity, var, covar, b)) ->
      let args = process_args arity in
      let drops, n = translate 0 b in
      let b = node_to_list n in
      [
        T.(
          i_lambda
            (translate_ty_lam_args var)
            (translate_ty covar)
            (seq (args @@ b @ [ i_dip [ i_drop (arity + drops) ] ])));
      ]
  | S.PartialApp (f, ps) ->
      T.(
        translate_comp depth f
        @ List.fold_right
            (fun a f -> translate_val (succ depth) a :: i_apply :: f)
            ps [])
  | S.BinArith (lhs, Add, rhs) ->
      let rhs = translate_val depth rhs in
      let lhs = translate_val (succ depth) lhs in
      T.(lhs |+ rhs)
  | S.BinArith (lhs, Minus, rhs) ->
      let rhs = translate_val depth rhs in
      let lhs = translate_val (succ depth) lhs in
      T.(lhs |- rhs)
  | S.BinArith (lhs, Mult, rhs) ->
      let rhs = translate_val depth rhs in
      let lhs = translate_val (succ depth) lhs in
      T.(lhs |* rhs)
  | S.BinArith (lhs, Div, rhs) ->
      let rhs = translate_val depth rhs in
      let lhs = translate_val (succ depth) lhs in
      T.(lhs |/ rhs)
  | S.BinBool (lhs, And, rhs) ->
      let rhs = translate_val depth rhs in
      let lhs = translate_val (succ depth) lhs in
      T.(lhs |&& rhs)
  | S.BinBool (lhs, Or, rhs) ->
      let rhs = translate_val depth rhs in
      let lhs = translate_val (succ depth) lhs in
      T.(lhs ||| rhs)
  | S.BinBool (lhs, Eq, rhs) ->
      let rhs = translate_val depth rhs in
      let lhs = translate_val (succ depth) lhs in
      T.[ rhs; lhs; i_compare; i_eq ]
  | S.BinBool (lhs, NEq, rhs) ->
      let rhs = translate_val depth rhs in
      let lhs = translate_val (succ depth) lhs in
      T.[ rhs; lhs; i_compare; i_neq ]
  | S.If (c, bt, bf) ->
      let c = translate_val depth c in
      let bt =
        match translate depth bt with
        | 0, b -> b
        | drops, Tezos_micheline.Micheline.Seq (_, b) ->
            T.(seq (b @ [ i_dip [ i_drop drops ] ]))
        | drops, b -> T.(seq [ b; i_dip [ i_drop drops ] ])
      in
      let bf =
        match translate depth bf with
        | 0, b -> b
        | drops, Tezos_micheline.Micheline.Seq (_, b) ->
            T.(seq (b @ [ i_dip [ i_drop drops ] ]))
        | drops, b -> T.(seq [ b; i_dip [ i_drop drops ] ])
      in
      T.[ c; i_if bt bf ]
  | S.App (a, b) ->
      let b = translate_val (succ depth) b in
      let a = translate_val depth a in
      T.(a |@ b)
  | S.Fst a -> T.(fst (translate_val depth a))
  | S.Snd a -> T.(snd (translate_val depth a))
  | S.CompNZ a -> T.[ translate_val depth a; i_eq ]
  | S.Not a -> T.[ translate_val depth a; i_not ]
  | S.Pair (a, b) ->
      let b = translate_val depth b in
      let a = translate_val (succ depth) a in
      T.(pair a b)
  | S.Cons (a, b) ->
      let a = translate_val (succ depth) a in
      let b = translate_val depth b in
      T.(cons a b)
  | S.List (TyList ty, []) -> T.[ nil (translate_ty ty) ]
  | S.List (ty, xs) -> (
      let ty = translate_ty ty in
      let map_lit x acc =
        match (acc, translate_val depth x) with
        | Some xs, Tezos_micheline.Micheline.(Prim (_, "PUSH", [ _; x ], [])) ->
            Some (x :: xs)
        | _, _ -> None
      in
      match List.(fold_right map_lit xs (Some [])) with
      | Some xs -> T.[ i_push ty (c_list xs) ]
      | None -> translate_list depth ty xs)
  | S.Stream _ | S.Head _ | S.Tail _ ->
      failwith "Streams are no support in Michelson"

and translate_list depth ty xs =
  List.fold_right
    (fun x (depth, acc) ->
      (succ depth, T.((translate_val depth x :: acc) @ [ i_cons ])))
    xs
    (depth, [ T.nil ty ])
  |> snd

and translate_val depth v =
  match v with
  | S.(Var i) -> T.i_dup (depth + i)
  | S.(Lit (Int i)) -> T.(i_push t_int (c_int i))
  | S.(Lit Unit) -> T.(i_push t_unit c_unit)
  | S.(Lit (Bool b)) -> T.(i_push t_bool (c_bool b))

and process_args i =
  let rec aux i acc =
    if i = 1 then fun body -> body
    else if i = 2 then fun body -> acc @ body
    else aux (pred i) (T.i_unpair :: T.i_swap :: acc)
  in
  fun x -> aux i [ T.i_unpair; T.i_swap ] x

let string_of_michelson x =
  let open Libmichelson in
  Encode.pp Format.str_formatter x;
  Format.flush_str_formatter ()

let string_of_anf =
  let open Libanf in
  Libcbv.IO.document_to_string Printer.print_body

let compile x =
  let open Tezos_micheline.Micheline in
  match translate 0 x with
  | 0, xs -> xs
  | drops, Seq (loc, xs) -> T.(Seq (loc, xs @ [ i_dip [ i_drop drops ] ]))
  | drops, xs -> T.(seq (xs :: [ i_dip [ i_drop drops ] ]))

let compile_contract code ty =
  let open Tezos_micheline.Micheline in
  let code =
    match compile code with
    | Seq (loc, xs) ->
        T.(
          Seq
            ( loc,
              (i_drop 1 :: xs)
              @ [ i_drop 1; i_push t_unit c_unit; nil t_operation; i_pair ] ))
    | c ->
        T.(
          seq
            [
              i_drop 1;
              c;
              i_drop 1;
              i_push t_unit c_unit;
              nil t_operation;
              i_pair;
            ])
  in
  let ty = translate_ty ty in
  T.script ~parameter:ty ~storage:ty code

let%test _ =
  compile S.v |> string_of_michelson
  = {|{ PUSH int 41 ; PUSH int 1 ; DUP 2 ; DUP 2 ; ADD ; DIP { DROP 2 } }|}

let%test _ =
  compile S.v' |> string_of_michelson
  = "{ PUSH int 1 ; DUP ; PUSH int 41 ; ADD ; DIP { DROP } }"

let print_src_tgt p =
  let src = p |> string_of_anf in
  let ans = compile p |> string_of_michelson in
  Printf.printf "SRC:\n----\n%s\n--- \nTGT:\n----\n%s\n----\n" src ans

let%test _ =
  compile S.w |> string_of_michelson
  = {|{ LAMBDA int int { DUP ; PUSH int 1 ; ADD ; DIP { DROP } } ;
  DUP ;
  PUSH int 41 ;
  EXEC ;
  DIP { DROP } }|}

let%test _ =
  compile S.z |> string_of_michelson
  = {|{ PUSH int 42 ;
  LAMBDA
    (pair int int)
    int
    { DUP ; CDR ; DUP 2 ; CAR ; DUP ; DIP { DROP 3 } } ;
  DUP 2 ;
  APPLY ;
  LAMBDA
    (pair (lambda int int) int)
    (lambda int int)
    { DUP ; CDR ; DUP 2 ; CAR ; DUP ; DIP { DROP 3 } } ;
  DUP 2 ;
  APPLY ;
  DUP ;
  PUSH int 0 ;
  EXEC ;
  DUP ;
  PUSH int 4 ;
  EXEC ;
  DIP { DROP 4 } }|}

let%test _ =
  compile S.z' |> string_of_michelson
  = {|{ PUSH int 42 ;
  LAMBDA (pair int int) int { UNPAIR ; SWAP ; DUP 2 ; DIP { DROP 2 } } ;
  DUP 2 ;
  APPLY ;
  LAMBDA
    (pair (lambda int int) int)
    (lambda int int)
    { UNPAIR ; SWAP ; DUP 2 ; DIP { DROP 2 } } ;
  DUP 2 ;
  APPLY ;
  DUP ;
  PUSH int 0 ;
  EXEC ;
  DUP ;
  PUSH int 4 ;
  EXEC ;
  DIP { DROP 4 } }|}
