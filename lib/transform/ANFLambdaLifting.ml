(* Source Language *)
module S = Libanf.Syntax.DeBruijnTerms

(* Target Language *)
module T = Libanf.Syntax.DeBruijnTerms
open Libanf.Syntax.Types

let empty = []
let bind env x = List.cons x env
let get env x = List.nth env x

let _free_vars n a ty_env =
  S.fold_computation n ty_env
    (fun depth _tys acc a ->
      match a with
      | S.(Var i) when succ i > depth -> (a, depth, List.nth _tys i) :: acc
      | _ -> acc)
    [] a
  |> List.rev

let free_vars a ty_env = _free_vars 0 a ty_env
let free_vars_rec a ty_env = _free_vars 1 a ty_env

let lift_var k d a =
  match a with
  | S.(Atom (Var i)) when succ i > d -> S.(Atom (Var (i + k)))
  | _ -> a

let lift_body k = S.map_body 0 (lift_var k)
let lift_comp k = S.map_computation 0 (lift_var k)
let lift_atom k = S.map_atom 0 (lift_var k)

let rec _lambda_lift env t =
  match t with
  | S.(Let (ty, c, b)) ->
      let c = lambda_lift_computation env c in
      T.(Let (ty, c, _lambda_lift (bind env ty) b))
  | S.(LetRec (ty, Lam (ar, var, covar, lb), b)) ->
      let env' = ty :: List.fold_left (fun env arg -> arg :: env) env var in
      let lb = _lambda_lift env' lb in
      let lam = T.Lam (ar, var, covar, lb) in
      let fv = free_vars_rec lam (ty :: env) in
      if fv = [] then T.LetRec (ty, lam, _lambda_lift (bind env ty) b)
      else
        T.(
          LetRec
            ( ty,
              lift_lambda 1 env' ar var covar lb fv,
              _lambda_lift (bind env ty) b ))
  | S.(LetRec (ty, PartialApp (Lam (ar, var, covar, lb), params), b)) ->
      let env' = ty :: List.fold_left (fun env arg -> arg :: env) env var in
      let lb = _lambda_lift env' lb in
      let lam = T.Lam (ar, var, covar, lb) in
      let fv = free_vars_rec lam (ty :: env) in
      if fv = [] then
        T.LetRec (ty, PartialApp (lam, params), _lambda_lift (bind env ty) b)
      else
        let lam =
          match lift_lambda 1 env' ar var covar lb fv with
          | T.PartialApp (f, a) -> T.PartialApp (f, a @ params)
          | t -> T.PartialApp (t, params)
        in
        T.(LetRec (ty, lam, _lambda_lift (bind env ty) b))
  | S.LetRec _ -> failwith "Recursived values are un supported"
  | S.Computation c -> T.Computation (lambda_lift_computation env c)

and lambda_lift_computation env c =
  match c with
  | S.Lam (ar, var, covar, b) ->
      let env' = List.fold_left (fun env arg -> arg :: env) env var in
      let b = _lambda_lift env' b in
      let lam = T.Lam (ar, var, covar, b) in
      let fv = free_vars lam env in
      if fv = [] then lam else lift_lambda 0 env ar var covar b fv
  | S.If (a, bt, bf) -> T.If (a, _lambda_lift env bt, _lambda_lift env bf)
  | S.PartialApp (f, args) ->
      let c =
        match lambda_lift_computation env f with
        | T.PartialApp (f, a) -> T.PartialApp (f, a @ args)
        | t -> T.PartialApp (t, args)
      in
      c
  | c -> c

and get_var_index = function S.Var i -> i | _ -> assert false

and lift_lambda is_rec _env ar var covar b fvs : T.computation =
  let nb_fvs = List.length fvs in
  let ar' = ar + nb_fvs in
  let var_map =
    List.mapi
      (fun pos (fv, depth, _) -> (fv, (depth, nb_fvs + depth - 1 - pos)))
      fvs
  in
  let rename v d =
    match v with
    | S.Atom (Var _ as v) ->
        let v =
          match List.assoc_opt v var_map with
          | Some (d', v') when d = d' -> S.Var v'
          | Some (d', v') when d = d' - is_rec -> S.Var v'
          | _ -> v
        in
        S.Atom v
    | _ -> v
  in
  let ty' =
    List.map (function S.Var _, _, ty -> ty | _ -> assert false) fvs @ var
  in

  let b' = S.map_body ar (fun d v -> rename v d) b in
  PartialApp
    ( Lam (ar', ty', covar, b'),
      List.map (fun (v, d, _) -> T.Var (get_var_index v - d)) fvs )

let a =
  let open S in
  clet int_ty
    (tval (tint 42))
    (tcomp (Lam (3, [ int_ty; int_ty; int_ty ], int_ty, tcomp (tval (tvar 3)))))

let b =
  let open S in
  clet int_ty
    (tval (tint 28))
    (clet int_ty
       (tval (tint 42))
       (tcomp
          (Lam (3, [ int_ty; int_ty; int_ty ], int_ty, tcomp (tval (tvar 4))))))

let c =
  let open S in
  clet int_ty
    (tval (tint 42))
    (clet (arrow_ty int_ty int_ty)
       (tlam 1 int_ty int_ty (tcomp (tval (tvar 1))))
       (tcomp (Pair (tvar 0, tvar 0))))

let lambda_lift = _lambda_lift T.Env.empty

let%test _ =
  let open T in
  lambda_lift a
  = clet int_ty (tval (Lit (Int 42)))
      (tcomp
         (PartialApp
            ( Lam
                ( 4,
                  [ int_ty; int_ty; int_ty; int_ty ],
                  int_ty,
                  tcomp (tval (tvar 3)) ),
              [ tvar 0 ] )))

let%test _ =
  let open T in
  lambda_lift b
  = clet int_ty (tval (Lit (Int 28)))
      (clet int_ty (tval (Lit (Int 42)))
         (tcomp
            (PartialApp
               ( Lam
                   ( 4,
                     [ int_ty; int_ty; int_ty; int_ty ],
                     int_ty,
                     tcomp (tval (tvar 3)) ),
                 [ tvar 1 ] ))))

let lifted_c =
  let open T in
  clet int_ty
    (tval (tint 42))
    (clet (arrow_ty int_ty int_ty)
       (PartialApp
          ( Lam (2, [ int_ty; int_ty ], int_ty, tcomp (tval (tvar 1))),
            [ tvar 0 ] ))
       (tcomp (Pair (tvar 0, tvar 0))))

let%test _ = lambda_lift c = lifted_c

let d =
  (* let ((int * int)*(int -> int) -> int) = fun (2: (int * int)*(int ->int) ->
     let (int -> int) = (fun (1:int) ->
       let int = snd var2 in
       let int = var1 in
       let int = var3 var2 in
       var0 + var1)
     in var0 0) in var0
  *)
  let open T in
  clet
    (arrow_ty (TyProd (TyProd (int_ty, int_ty), arrow_ty int_ty int_ty)) int_ty)
    (Lam
       ( 2,
         [ TyProd (int_ty, int_ty); arrow_ty int_ty int_ty ],
         int_ty,
         clet (arrow_ty int_ty int_ty)
           (tlam 1 int_ty int_ty
              (clet int_ty
                 (Snd (tvar 2))
                 (clet int_ty
                    (tval (tvar 1))
                    (clet int_ty
                       (tapp (tvar 3) (tvar 2))
                       (tcomp (BinArith (tvar 0, Add, tvar 1)))))))
           (tcomp (tapp (tvar 0) (tint 0))) ))
    (tcomp (tval (tvar 0)))

let%test _ =
  let open T in
  lambda_lift d
  = Let
      ( TyArrow
          ([ TyProd (TyProd (TyInt, TyInt), TyArrow ([ TyInt ], TyInt)) ], TyInt),
        Lam
          ( 2,
            [ TyProd (TyInt, TyInt); TyArrow ([ TyInt ], TyInt) ],
            int_ty,
            Let
              ( TyArrow ([ TyInt ], TyInt),
                PartialApp
                  ( Lam
                      ( 3,
                        [
                          TyProd (TyInt, TyInt);
                          TyArrow ([ TyInt ], TyInt);
                          TyInt;
                        ],
                        int_ty,
                        Let
                          ( TyInt,
                            Snd (Var 2),
                            Let
                              ( TyInt,
                                Val (Var 1),
                                Let
                                  ( TyInt,
                                    App (Var 3, Var 2),
                                    Computation (BinArith (Var 0, Add, Var 1))
                                  ) ) ) ),
                    [ Var 1; Var 0 ] ),
                Computation (App (Var 0, Lit (Int 0))) ) ),
        Computation (Val (Var 0)) )

let%test _ =
  let p =
    {|
    let (x: unit) = () in
    let (y: int -> int -> unit) = fun (y:int) -> fun (y:int) ->
      let (x:unit) = x in x
    in
    y
   |}
  in
  Libcbv.IO.parse_program p |> Libcbv.Typechecker.elaborate
  |> ANFConversion.to_anf |> ANFNamedUncurry.uncurry
  |> NamedANFToDeBruijn.encode_variables |> lambda_lift
  |> Libcbv.IO.document_to_string Libanf.Printer.print_body
  = {|let unit = () in
let (int,int) -> unit = (fun (3:unit,int,int) -> let unit = var2 in var0) @
 var0 in
var0|}

let%test _ =
  let p =
    {|
    let (u:unit) = () in
    if true then fun (k:int) -> 1 else
    let (x:int) = 1 in
    let (y:int) = 2 in
    fun  (j:int) ->
     let (a:bool) = true in
     let (b:bool) = false in
     let (r:int -> int ) = (fun (z:int) -> x * y) in r j
   |}
  in
  Libcbv.IO.parse_program p |> Libcbv.Typechecker.elaborate
  |> ANFConversion.to_anf |> ANFNamedUncurry.uncurry
  |> NamedANFToDeBruijn.encode_variables |> lambda_lift
  |> Libcbv.IO.document_to_string Libanf.Printer.print_body
  = {|let unit = () in
if
(true)

 {(fun (1:int) -> 1)}
 {let int = 1 in
 let int = 2 in
 (fun (3:int,int,int) ->
  let bool = true in
  let bool = false in
  let int -> int = (fun (3:int,int,int) -> var2*var1) @ var4@var3 in
  let int -> int = var0 in let int = var4 in var1 var0) @
  var1@var0}|}

let%test _ =
  let p =
    {|
  (let rec (fact : unit -> (int -> (int -> int)))=
    fun (u : unit) -> fun (x : int) ->
     if (x  =  0) then (fun (time : int) ->  1)
     else
       (fun (time2 : int) ->
       (let (y : int) = (fun (time3 : int) ->  x  -  1) time2 in
        fun (time4 : int) ->
          (let (z : int) =  fact () y time4 in
           fun (time5 : int) ->  x  *  z) time4) time2) in
   fact ()) 5 0
    |}
  in
  Libcbv.IO.parse_program p |> Libcbv.Typechecker.elaborate
  |> ANFConversion.to_anf |> ANFNamedUncurry.uncurry
  |> NamedANFToDeBruijn.encode_variables |> lambda_lift
  |> Libcbv.IO.document_to_string Libanf.Printer.print_body
  = {|let rec (unit,int) -> (int -> int) = (fun (2:unit,int) ->
 let bool = var1=0 in
 if
 (var0)

  {(fun (1:int) -> 1)}
  {(fun (4:int,(unit,int) -> (int -> int),int,int) ->
   let int = var0 in
   let int = var4-1 in
   let int -> int = (fun (4:(unit,int) -> (int -> int),int,int,int) ->
    let (unit,int) -> (int -> int) = var3 in
    let unit = () in
    let int -> (int -> int) = var1 @ var0 in
    let int = var5 in
    let int -> int = var1 var0 in
    let int = var5 in
    let int = var1 var0 in
    let int -> int = (fun (3:int,int,int) -> var2*var1) @ var8@var0 in
    let int = var8 in var1 var0) @
    var4@var0@var3 in
   let int = var3 in var1 var0) @
   var2@var1@var2}) in
let (unit,int) -> (int -> int) = var0 in
let unit = () in
let int -> (int -> int) = var1 @ var0 in
let int = 5 in let int -> int = var1 var0 in let int = 0 in var1 var0|}
