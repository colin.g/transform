(* Source Language *)
module S = Libanf.Syntax.DeBruijnTerms
module STy = Libanf.Syntax.Types

(* Target Language *)
module T = Libmimi.Syntax.Terms
module TTy = Libmimi.Syntax.Types

let rec translate_ty t =
  match t with
  | STy.TyUnit -> TTy.TyUnit
  | STy.TyInt -> TTy.TyInt
  | STy.TyBool -> TTy.TyBool
  | STy.TyArrow (a, b) ->
      TTy.(TyArrow (translate_ty_lam_args a, translate_ty b))
  | STy.TyProd (a, b) -> TTy.TyProd (translate_ty a, translate_ty b)
  | STy.TyList t -> TTy.TyList (translate_ty t)
  | STy.TyStream t -> TTy.TyStream (translate_ty t)
  | _ -> assert false

and translate_ty_lam_args ts =
  match ts with
  | [] -> assert false
  | [ t ] -> translate_ty t
  | [ t1; t2 ] -> TTy.TyProd (translate_ty t1, translate_ty t2)
  | t :: ts -> TTy.TyProd (translate_ty t, translate_ty_lam_args ts)

let rec translate depth anf =
  match anf with
  | S.Let (_, a, b) ->
      T.(
        Seq
          ( translate_comp depth a,
            Seq (translate depth b, Prim (Dip (Prim (Drop 1)))) ))
  | S.Computation c -> translate_comp depth c
  | S.LetRec (_, Lam (arity, var, covar, c), b) ->
      let ty_args = translate_ty_lam_args var in
      let ty_ret = translate_ty covar in
      let code = T.Seq (translate 0 c, Prim (Dip (Prim (Drop (arity + 1))))) in
      let body =
        if arity > 1 then
          let args_rec = process_args_rec arity in
          T.Seq (Prim (Dip args_rec), code)
        else code
      in
      T.Seq
        ( Prim (LamRec (ty_args, ty_ret, T.Seq (Prim Swap, body))),
          Seq (translate depth b, Prim (Dip (Prim (Drop 1)))) )
  | S.LetRec (_, PartialApp (Lam (arity, var, covar, c), params), b) ->
      let params =
        List.mapi (fun i x -> translate_val (depth + i) x) (List.rev params)
      in
      let ty_args = translate_ty_lam_args var in
      let ty_ret = translate_ty covar in
      let repeat_apply n b =
        let rec aux i =
          if i = 1 then T.(Seq (Prim Apply, b))
          else T.Seq (Prim Apply, aux (i - 1))
        in
        aux n
      in
      let code =
        let params_len = List.length params in
        let dup_params =
          let rec aux i depth =
            if i = 1 then T.(Prim (Dup depth))
            else T.(Seq (Prim (Dup depth), aux (i - 1) (depth + 2)))
          in
          aux params_len (arity - params_len)
        in
        T.(
          Seq
            ( Prim (Dip dup_params),
              repeat_apply params_len
                (Seq (translate 0 c, Prim (Dip (Prim (Drop (arity + 1)))))) ))
      in
      let body =
        if arity > 1 then
          let args_rec = process_args_rec arity in
          T.Seq (Prim (Dip args_rec), code)
        else code
      in

      let lam_rec =
        T.(Prim (LamRec (ty_args, ty_ret, T.Seq (Prim Swap, body))))
      in
      let a =
        List.fold_right
          (fun arg f -> T.(Seq (arg, Seq (f, Prim Apply))))
          params lam_rec
      in
      T.Seq (a, Seq (translate depth b, Prim (Dip (Prim (Drop 1)))))
  | _ -> failwith "Recursive values are unsupported.\n"

and translate_comp depth c =
  match c with
  | S.Val v -> translate_val depth v
  | S.(Lam (arity, var, covar, b)) ->
      let args = process_args arity in
      T.Prim
        (Lam
           ( translate_ty_lam_args var,
             translate_ty covar,
             args @@ T.Seq (translate 0 b, Prim (Dip (Prim (Drop arity)))) ))
  | S.PartialApp (a, ps) ->
      let ps =
        List.mapi (fun i x -> translate_val (depth + i) x) (List.rev ps)
      in
      List.fold_right
        (fun arg f -> T.(Seq (arg, Seq (f, Prim Apply))))
        ps
        (translate_comp (List.length ps + depth) a)
  | S.BinArith (lhs, Add, rhs) ->
      let rhs = translate_val (succ depth) rhs in
      let lhs = translate_val depth lhs in
      T.(Seq (lhs, Seq (rhs, Prim Add)))
  | S.BinArith (lhs, Minus, rhs) ->
      let rhs = translate_val (succ depth) rhs in
      let lhs = translate_val depth lhs in
      T.(Seq (lhs, Seq (rhs, Prim Minus)))
  | S.BinArith (lhs, Mult, rhs) ->
      let rhs = translate_val (succ depth) rhs in
      let lhs = translate_val depth lhs in
      T.(Seq (lhs, Seq (rhs, Prim Mult)))
  | S.BinArith (lhs, Div, rhs) ->
      let rhs = translate_val (succ depth) rhs in
      let lhs = translate_val depth lhs in
      T.(Seq (lhs, Seq (rhs, Prim Div)))
  | S.BinBool (lhs, Eq, rhs) ->
      let rhs = translate_val (succ depth) rhs in
      let lhs = translate_val depth lhs in
      T.(Seq (lhs, Seq (rhs, Seq (Prim Compare, Prim CompNZ))))
  | S.BinBool (lhs, NEq, rhs) ->
      let rhs = translate_val (succ depth) rhs in
      let lhs = translate_val depth lhs in
      T.(Seq (lhs, Seq (rhs, Seq (Prim Compare, Seq (Prim CompNZ, Prim Not)))))
  | S.BinBool (lhs, And, rhs) ->
      let rhs = translate_val (succ depth) rhs in
      let lhs = translate_val depth lhs in
      T.(Seq (lhs, Seq (rhs, Prim And)))
  | S.BinBool (lhs, Or, rhs) ->
      let rhs = translate_val (succ depth) rhs in
      let lhs = translate_val depth lhs in
      T.(Seq (lhs, Seq (rhs, Prim Or)))
  | S.If (c, bt, bf) ->
      let c = translate_val depth c in
      let bt = translate depth bt in
      let bf = translate depth bf in
      T.(Seq (c, Prim (If (bt, bf))))
  | S.App (a, b) ->
      let b = translate_val depth b in
      let a = translate_val (succ depth) a in
      T.(Seq (b, Seq (a, Prim Exec)))
  | S.Snd a -> Seq (translate_val depth a, Prim Snd)
  | S.Fst a -> Seq (translate_val depth a, Prim Fst)
  | S.Head a -> Seq (translate_val depth a, Prim Head)
  | S.Tail a -> Seq (translate_val depth a, Prim Tail)
  | S.CompNZ a -> Seq (translate_val depth a, Prim CompNZ)
  | S.Not a -> Seq (translate_val depth a, Prim Not)
  | S.Pair (a, b) ->
      let a = translate_val (succ depth) a in
      let b = translate_val depth b in
      Seq (b, Seq (a, Prim Pair))
  | S.Stream (a, b) ->
      let a = translate_val (succ depth) a in
      let b = translate_val depth b in
      Seq (b, Seq (a, Prim Stream))
  | S.Cons (a, b) ->
      let a = translate_val (succ depth) a in
      let b = translate_val depth b in
      Seq (b, Seq (a, Prim Cons))
  | S.List (TyList ty, []) -> Prim (Nil (translate_ty ty))
  | S.List (ty, xs) -> (
      let ty = translate_ty ty in
      let map_lit x acc =
        match (acc, translate_val depth x) with
        | Some xs, T.Prim (Push (_, x)) -> Some (x :: xs)
        | _, _ -> None
      in
      match List.(fold_right map_lit xs (Some [])) with
      | Some xs -> T.Prim (Push (ty, List xs))
      | None -> translate_list depth ty xs)

and translate_list depth ty xs =
  List.fold_right
    (fun x (depth, acc) ->
      (succ depth, T.(Seq (acc, Seq (translate_val depth x, Prim Cons)))))
    xs (depth, T.Prim (Nil ty))
  |> snd

and translate_val depth v =
  match v with
  | S.(Var i) -> T.Prim (Dup (depth + i))
  | S.(Lit (Int i)) -> T.Prim (Push (TyInt, Int i))
  | S.(Lit (Bool b)) -> T.Prim (Push (TyBool, Bool b))
  | S.(Lit Unit) -> T.Prim (Push (TyUnit, Unit))

and process_args i =
  let rec aux i acc =
    if i = 1 then fun body -> body
    else if i = 2 then fun body -> T.(Seq (acc, body))
    else aux (pred i) T.(Seq (Prim Unpair, Seq (Prim Swap, acc)))
  in
  aux i T.(Seq (Prim Unpair, Prim Swap))

and process_args_rec i =
  let rec aux i acc =
    if i = 1 then acc
    else if i = 2 then acc
    else aux (pred i) T.(Seq (Prim Unpair, Seq (Prim Swap, acc)))
  in
  aux i T.(Seq (Prim Unpair, Prim Swap))

let string_of_mimi =
  let open Libmimi in
  IO.document_to_string Printer.print_term

let string_of_anf =
  let open Libanf in
  Libcbv.IO.document_to_string Printer.print_body

let compile = translate 0

let%test _ =
  compile S.v |> string_of_mimi
  = "PUSH int 41;\nPUSH int 1;\nDUP;\nDUP 2;\nADD;\nDIP {DROP};\nDIP {DROP}"

let%test _ =
  compile S.v' |> string_of_mimi
  = "PUSH int 1;\nPUSH int 41;\nDUP 1;\nADD;\nDIP {DROP}"

let print_src_tgt p =
  let src = p |> string_of_anf in
  let ans = compile p |> string_of_mimi in
  Printf.printf "SRC:\n----\n%s\n--- \nTGT:\n----\n%s\n----\n" src ans

let%test _ =
  compile S.w |> string_of_mimi
  = "LAMBDA int int {\n\
    \ PUSH int 1;\n\
    \ DUP 1;\n\
    \ ADD;\n\
    \ DIP {DROP}};\n\
     PUSH int 41;\n\
     DUP 1;\n\
     EXEC;\n\
     DIP {DROP}"

let%test _ =
  compile S.z |> string_of_mimi
  = "PUSH int 42;\n\
     DUP;\n\
     LAMBDA (pair int int) int {\n\
    \ DUP;\n\
    \ SND;\n\
    \ DUP 1;\n\
    \ FST;\n\
    \ DUP;\n\
    \ DIP {DROP};\n\
    \ DIP {DROP};\n\
    \ DIP {DROP}};\n\
     APPLY;\n\
     DUP;\n\
     LAMBDA (pair (lambda int int) int) (lambda int int) {\n\
    \ DUP;\n\
    \ SND;\n\
    \ DUP 1;\n\
    \ FST;\n\
    \ DUP;\n\
    \ DIP {DROP};\n\
    \ DIP {DROP};\n\
    \ DIP {DROP}};\n\
     APPLY;\n\
     PUSH int 0;\n\
     DUP 1;\n\
     EXEC;\n\
     PUSH int 4;\n\
     DUP 1;\n\
     EXEC;\n\
     DIP {DROP};\n\
     DIP {DROP};\n\
     DIP {DROP};\n\
     DIP {DROP}"

let%test _ =
  compile S.z' |> string_of_mimi
  = "PUSH int 42;\n\
     DUP;\n\
     LAMBDA (pair int int) int {\n\
    \ UNPAIR;\n\
    \ SWAP;\n\
    \ DUP 1;\n\
    \ DIP {DROP 2}};\n\
     APPLY;\n\
     DUP;\n\
     LAMBDA (pair (lambda int int) int) (lambda int int) {\n\
    \ UNPAIR;\n\
    \ SWAP;\n\
    \ DUP 1;\n\
    \ DIP {DROP 2}};\n\
     APPLY;\n\
     PUSH int 0;\n\
     DUP 1;\n\
     EXEC;\n\
     PUSH int 4;\n\
     DUP 1;\n\
     EXEC;\n\
     DIP {DROP};\n\
     DIP {DROP};\n\
     DIP {DROP};\n\
     DIP {DROP}"
