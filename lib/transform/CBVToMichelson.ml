open Libcbv.Typechecker
open ANFConversion
open ANFLambdaLifting
open NamedANFToDeBruijn
open ANFNamedUncurry
open ANFToMichelson
module Michelson = Libmichelson

let normal_form p = to_anf p |> uncurry |> encode_variables |> lambda_lift
let compile p = elaborate p |> fun p -> compile_contract (normal_form p) TyUnit
let compile_raw p = elaborate p |> normal_form |> ANFToMichelson.compile

let fact_5 =
  "let rec fact (x:int) : int = if is_zero x then 1 else x *  fact (x - 1 ) in \
   fact 5"

let alice_ml =
  {|
    let (value : (int * int) -> (int -> int)) =
      fun (param : int * int) ->
       fun (state: int) ->
        if is_zero (fst param) then 0
        else state + (snd param)
    in
    let (operations :(int * int) -> (int -> (int * int) list)) =
      fun (param : (int * int)) ->
        fun (state:int) ->
        if is_zero (fst param) then (0, state)::[]
        else []
    in
    let (run: (int * int) -> (int * int) -> (((int * int) list)  * (int * int) )) =
      fun (param : int * int) -> fun (laststate : int * int) ->
        let (init_state : int) = 0 in
        let (state : int) =
          if is_zero (fst laststate) then init_state else (snd laststate)
        in
        let (time : int) = fst laststate in
        let (vs : int) = value param state in
        let (ops : (int * int) list)  = operations param state in
        let (newstate : int * int) = (time + 1, vs) in
        (ops, newstate)
    in run
  |}

let compile_to_str c prog =
  Libcbv.IO.parse_program prog |> c |> ANFToMichelson.string_of_michelson

let%test _ =
  compile_to_str compile_raw alice_ml
  = {|{ LAMBDA
    (pair (pair int int) int)
    int
    { UNPAIR ;
      SWAP ;
      DUP 2 ;
      CAR ;
      DUP ;
      EQ ;
      DUP ;
      IF { PUSH int 0 } { DUP 4 ; CDR ; DUP ; DUP 5 ; ADD ; DIP { DROP } } ;
      DIP { DROP 4 } } ;
  LAMBDA
    (pair (pair int int) int)
    (list (pair int int))
    { UNPAIR ;
      SWAP ;
      DUP 2 ;
      CAR ;
      DUP ;
      EQ ;
      DUP ;
      IF { DUP 3 ;
           PUSH int 0 ;
           PAIR ;
           NIL (pair int int) ;
           DUP ;
           DUP 3 ;
           CONS ;
           DIP { DROP 2 } }
         { NIL (pair int int) } ;
      DIP { DROP 4 } } ;
  LAMBDA
    (pair (lambda (pair (pair int int) int) int)
          (pair (lambda (pair (pair int int) int) (list (pair int int)))
                (pair (pair int int) (pair int int))))
    (pair (list (pair int int)) (pair int int))
    { UNPAIR ;
      SWAP ;
      UNPAIR ;
      SWAP ;
      UNPAIR ;
      SWAP ;
      PUSH int 0 ;
      DUP 2 ;
      CAR ;
      DUP ;
      EQ ;
      DUP ;
      IF { DUP 3 } { DUP 4 ; CDR } ;
      DUP 5 ;
      CAR ;
      DUP 9 ;
      DUP 8 ;
      DUP 2 ;
      DUP 2 ;
      APPLY ;
      DUP 5 ;
      DUP 2 ;
      DUP 2 ;
      EXEC ;
      DUP 13 ;
      DUP 13 ;
      DUP 2 ;
      DUP 2 ;
      APPLY ;
      DUP 10 ;
      DUP 2 ;
      DUP 2 ;
      EXEC ;
      PUSH int 1 ;
      DUP 12 ;
      ADD ;
      DUP 7 ;
      DUP 2 ;
      PAIR ;
      DUP ;
      DUP 4 ;
      PAIR ;
      DIP { DROP 21 } } ;
  DUP 3 ;
  APPLY ;
  DUP 2 ;
  APPLY ;
  DUP ;
  DIP { DROP 3 } }|}

let%test _ =
  compile_to_str compile fact_5
  = {|{ parameter unit ;
  storage unit ;
  code { DROP ;
         LAMBDA_REC
           int
           int
           { DUP 2 ;
             EQ ;
             DUP ;
             IF { PUSH int 1 }
                { DUP 2 ;
                  PUSH int 1 ;
                  DUP 5 ;
                  SUB ;
                  DUP 2 ;
                  DUP 2 ;
                  EXEC ;
                  DUP ;
                  DUP 7 ;
                  MUL ;
                  DIP { DROP 3 } } ;
             DIP { DROP 3 } } ;
         DUP ;
         PUSH int 5 ;
         DUP 2 ;
         DUP 2 ;
         EXEC ;
         DIP { DROP 3 } ;
         DROP ;
         PUSH unit Unit ;
         NIL operation ;
         PAIR } }|}

let%test _ =
  compile_to_str compile
    {|
    (fun (time : int) ->
  (let (ones : int list) =
     (let rec (ones : unit -> (int -> int list)) =
       fun (u : unit) (time2 : int) -> 1::[]
     in ones ()) time in
   fun (time3 : int) ->
    (let (ux : unit -> (int -> int list)) =
      (let (m : unit -> (int -> int list)) =
      (fun (u:unit) (i:int) -> ones) in fun (time4 : int) ->  m) time3
   in
    fun (time5 : int) ->  ux) time3) time) 0 |}
  = {|{ parameter unit ;
  storage unit ;
  code { DROP ;
         PUSH int 0 ;
         LAMBDA_REC
           (pair unit int)
           (list int)
           { UNPAIR ; SWAP ; NIL int ; DUP ; PUSH int 1 ; CONS ; DIP { DROP 4 } } ;
         DUP ;
         PUSH unit Unit ;
         DUP 2 ;
         DUP 2 ;
         APPLY ;
         DUP 5 ;
         DUP 2 ;
         DUP 2 ;
         EXEC ;
         LAMBDA
           (pair (list int) int)
           (lambda (pair unit int) (list int))
           { UNPAIR ;
             SWAP ;
             LAMBDA
               (pair (list int) (pair unit int))
               (list int)
               { UNPAIR ; SWAP ; UNPAIR ; SWAP ; DUP 3 ; DIP { DROP 3 } } ;
             DUP 3 ;
             APPLY ;
             LAMBDA
               (pair (lambda (pair unit int) (list int)) int)
               (lambda (pair unit int) (list int))
               { UNPAIR ; SWAP ; DUP 2 ; DIP { DROP 2 } } ;
             DUP 2 ;
             APPLY ;
             DUP 3 ;
             DUP 2 ;
             DUP 2 ;
             EXEC ;
             LAMBDA
               (pair (lambda (pair unit int) (list int)) int)
               (lambda (pair unit int) (list int))
               { UNPAIR ; SWAP ; DUP 2 ; DIP { DROP 2 } } ;
             DUP 2 ;
             APPLY ;
             DUP 6 ;
             DUP 2 ;
             DUP 2 ;
             EXEC ;
             DIP { DROP 8 } } ;
         DUP 2 ;
         APPLY ;
         DUP 8 ;
         DUP 2 ;
         DUP 2 ;
         EXEC ;
         DIP { DROP 9 } ;
         DROP ;
         PUSH unit Unit ;
         NIL operation ;
         PAIR } }|}
