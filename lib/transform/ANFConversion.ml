module S = Libcbv.Syntax.Terms
module STypes = Libcbv.Syntax.Types
module T = Libanf.Syntax.Terms
module TTypes = Libanf.Syntax.Types

let unique =
  let acc = ref 0 in
  fun () ->
    let x = !acc in
    incr acc;
    x

let fresh_var x = x ^ string_of_int (unique ())

exception UntypedTerm of S.term

let rec anf_rewrite t =
  match t with
  | S.TypedTerm (Lit Unit, _) -> T.(Computation (Val (Lit Unit)))
  | S.TypedTerm (Lit (Int i), _) -> T.(Computation (Val (Lit (Int i))))
  | S.TypedTerm (Lit (Bool b), _) -> T.(Computation (Val (Lit (Bool b))))
  | S.TypedTerm (Pair (x, y), TyProd (tyx, tyy)) ->
      let tyx = translate_type tyx in
      let tyy = translate_type tyy in
      anf_rewrite_binary pair_cons (anf_rewrite x) tyx (anf_rewrite y) tyy
  | S.TypedTerm (Cons (x, y), (TyList t as tl)) ->
      let t = translate_type t in
      let tl = translate_type tl in
      anf_rewrite_binary list_cons (anf_rewrite x) t (anf_rewrite y) tl
  | S.TypedTerm (Stream (x, y), (TyStream t as ts)) ->
      let t = translate_type t in
      let tl = translate_type ts in
      anf_rewrite_binary stream_cons (anf_rewrite x) t (anf_rewrite y) tl
  | S.TypedTerm (Var label, _) -> T.(Computation (Val (Var label)))
  | S.TypedTerm (Lam (label, var, t), TyArrow (_, covar)) ->
      T.(
        Computation
          (Lam
             ( [ label ],
               [ translate_type var ],
               translate_type covar,
               anf_rewrite t )))
  | S.TypedTerm (App (TypedTerm (Lam (label, ty, b), _), a), _) ->
      flatten_body (anf_rewrite a) (fun c ->
          T.Let (label, translate_type ty, c, anf_rewrite b))
  | S.TypedTerm
      ( App
          ( (TypedTerm (_, (TyArrow (_, _) as tya)) as a),
            (TypedTerm (_, tyb) as b) ),
        _ ) ->
      let va = fresh_var "a" in
      let vb = fresh_var "b" in
      let tya = translate_type tya in
      let tyb = translate_type tyb in
      (fun ca ->
        T.Let
          ( va,
            tya,
            ca,
            flatten_body (anf_rewrite b) (fun cb ->
                T.Let (vb, tyb, cb, T.Computation (App (Var va, Var vb)))) ))
      |> flatten_body (anf_rewrite a)
  | S.TypedTerm (Fst (TypedTerm (_, typ) as p), _) ->
      flatten_unary proj_fst (translate_type typ) (anf_rewrite p)
  | S.TypedTerm (Snd (TypedTerm (_, typ) as p), _) ->
      flatten_unary proj_snd (translate_type typ) (anf_rewrite p)
  | S.TypedTerm (Head (TypedTerm (_, typ) as p), _) ->
      flatten_unary head (translate_type typ) (anf_rewrite p)
  | S.TypedTerm (Tail (TypedTerm (_, typ) as p), _) ->
      flatten_unary tail (translate_type typ) (anf_rewrite p)
  | S.TypedTerm (CompNZ (TypedTerm (_, typ) as p), _) ->
      flatten_unary compnz (translate_type typ) (anf_rewrite p)
  | S.TypedTerm (Not (TypedTerm (_, typ) as p), _) ->
      flatten_unary not (translate_type typ) (anf_rewrite p)
  | S.TypedTerm
      (BinArith ((TypedTerm (_, ty1) as t1), op, (TypedTerm (_, ty2) as t2)), _)
    ->
      let ty1 = translate_type ty1 in
      let ty2 = translate_type ty2 in
      anf_rewrite_binary (bin_arith_cons op) (anf_rewrite t1) ty1
        (anf_rewrite t2) ty2
  | S.TypedTerm
      (BinBool ((TypedTerm (_, ty1) as t1), op, (TypedTerm (_, ty2) as t2)), _)
    ->
      let ty1 = translate_type ty1 in
      let ty2 = translate_type ty2 in
      anf_rewrite_binary (bin_bool_cons op) (anf_rewrite t1) ty1
        (anf_rewrite t2) ty2
  | S.TypedTerm
      ( If
          ( (TypedTerm (_, _) as c),
            (TypedTerm (_, _) as bt),
            (TypedTerm (_, _) as bf) ),
        _ ) ->
      flatten_unary
        (fun c -> T.Computation (If (c, anf_rewrite bt, anf_rewrite bf)))
        TTypes.TyBool (anf_rewrite c)
  | S.TypedTerm (Let (label, tya, (TypedTerm (_, _) as a), t2), _) ->
      let tya = translate_type tya in
      flatten_body (anf_rewrite a) (fun c ->
          T.Let (label, tya, c, anf_rewrite t2))
  | S.TypedTerm
      (LetRec (label, tya, (TypedTerm (_, _) as a), (TypedTerm (_, _) as t2)), _)
    ->
      let tya = translate_type tya in
      flatten_body (anf_rewrite a) (fun c ->
          T.LetRec (label, tya, c, anf_rewrite t2))
  | S.TypedTerm (List xs, ty) -> (
      let rec map_lit x pred =
        match (pred, x) with
        | Some xs, S.Lit (Int i) -> Some (T.Lit (Int i) :: xs)
        | Some xs, S.Lit Unit -> Some (T.Lit Unit :: xs)
        | Some _, S.TypedTerm (x, _) -> map_lit x pred
        | _ -> None
      in
      match List.fold_right map_lit xs (Some []) with
      | Some xs -> T.(Computation (List (translate_type ty, xs)))
      | None -> anf_rewrite_list xs)
  | _ -> raise (UntypedTerm t)

and proj_fst x = T.Computation (Fst x)
and proj_snd x = T.Computation (Snd x)
and head x = T.Computation (Head x)
and tail x = T.Computation (Tail x)
and compnz x = T.Computation (CompNZ x)
and not x = T.Computation (Not x)

and anf_rewrite_list xs =
  let rec aux xs acc =
    match xs with
    | [ (S.TypedTerm (_, ty) as x) ] ->
        let velt = fresh_var "elt" in
        let ty = translate_type ty in
        flatten_body (anf_rewrite x) (fun c ->
            T.Let
              ( velt,
                ty,
                c,
                T.Computation
                  (List (TTypes.TyList ty, List.rev (T.Var velt :: acc))) ))
    | (S.TypedTerm (_, ty) as x) :: xs ->
        let velt = fresh_var "elt" in
        let ty = translate_type ty in
        flatten_body (anf_rewrite x) (fun c ->
            T.Let (velt, ty, c, aux xs (T.Var velt :: acc)))
    | _ -> raise (UntypedTerm (S.List xs))
  in
  aux xs []

and flatten_unary comp typ p =
  match p with
  | T.Computation (Val v) -> comp v
  | _ ->
      let vp = fresh_var "p" in
      flatten_body p (fun c -> T.Let (vp, typ, c, comp (Var vp)))

and flatten_body b (body : T.computation -> T.body) : T.body =
  match b with
  | T.Computation c -> body c
  | T.Let (label', ty', c', b) -> T.Let (label', ty', c', flatten_body b body)
  | T.LetRec (label', ty', c', b) ->
      T.LetRec (label', ty', c', flatten_body b body)

and pair_cons a b = T.Pair (a, b)
and list_cons a b = T.Cons (a, b)
and stream_cons a b = T.Stream (a, b)

and bin_arith_cons op a b =
  let op = translate_arith_op op in
  T.BinArith (a, op, b)

and bin_bool_cons op a b =
  let op = translate_bool_op op in
  T.BinBool (a, op, b)

and anf_rewrite_binary cons a tya b tyb =
  match (a, b) with
  | Computation (Val a), Computation (Val b) -> T.Computation (cons a b)
  | Computation (Val a), b ->
      let vb = fresh_var "b" in
      flatten_body b (fun c ->
          T.Let (vb, tyb, c, Computation (cons a (Var vb))))
  | a, Computation (Val b) ->
      let va = fresh_var "a" in
      flatten_body a (fun c ->
          T.Let (va, tya, c, Computation (cons (Var va) b)))
  | a, b ->
      let va = fresh_var "a" in
      let vb = fresh_var "b" in
      flatten_body a (fun ca ->
          T.Let
            ( va,
              tya,
              ca,
              flatten_body b (fun cb ->
                  T.Let (vb, tyb, cb, Computation (cons (Var va) (Var vb)))) ))

and translate_type t = Obj.magic t
(* match t with
 * | STypes.TyVar x -> TTypes.TyVar x
 * | STypes.TyUnit -> TTypes.TyUnit
 * | STypes.TyInt -> TTypes.TyInt
 * | STypes.TyProd (ty1, ty2) ->
 *     TTypes.TyProd (translate_type ty1, translate_type ty2)
 * | STypes.TyArrow (variance, covariance) ->
 *     TTypes.TyArrow
 *       (List.map translate_type variance, translate_type covariance) *)

and translate_arith_op op =
  match op with
  | S.Add -> T.Add
  | S.Minus -> T.Minus
  | S.Mult -> T.Mult
  | S.Div -> T.Div

and translate_bool_op op =
  match op with S.Eq -> T.Eq | S.NEq -> T.NEq | S.And -> T.And | S.Or -> T.Or

let to_anf = anf_rewrite
