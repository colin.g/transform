open Tezos_micheline

let c_int i = Micheline.(Int (0, Z.of_int i))
let c_string s = Micheline.(String (0, s))
let c_bytes s = Micheline.(Bytes (0, s))

let prim ?(annotations = []) s l =
  let ann = annotations in
  Micheline.(Prim (0, s, l, ann))

let prim_int ?(annotations = []) s n =
  let ann = annotations in
  Micheline.(Prim (0, s, [ c_int n ], ann))

let seq l = Micheline.(Seq (0, l))
let t_operation = prim "operation" []
let t_unit = prim "unit"
let t_pair a b = prim "pair" [ a; b ]
let c_pair a b = prim "Pair" [ a; b ]
let i_pair = prim "PAIR" []
let i_unpair = prim "UNPAIR" []
let i_swap = prim "SWAP" []
let t_unit = prim "unit" []
let t_int = prim "int" []
let t_bool = prim "bool" []
let c_true = prim "True" []
let c_false = prim "False" []
let c_bool b = if b then c_true else c_false
let t_lambda var covar = prim "lambda" [ var; covar ]
let t_list ty = prim "list" [ ty ]
let c_unit = prim "Unit" []
let i_dup n = if n = 0 then prim "DUP" [] else prim_int "DUP" (succ n)
let i_drop n = if n = 1 then prim "DROP" [] else prim_int "DROP" n
let i_dip l = prim "DIP" [ seq l ]
let i_lambda t_in t_out code = prim "LAMBDA" [ t_in; t_out; code ]
let i_lambda_rec t_in t_out code = prim "LAMBDA_REC" [ t_in; t_out; code ]
let c_lambda_rec code = prim "Lambda_rec" [ code ]
let i_add = prim "ADD" []
let i_sub = prim "SUB" []
let i_mul = prim "MUL" []
let i_ediv = prim "EDIV" []
let i_and = prim "AND" []
let i_or = prim "OR" []
let i_compare = prim "COMPARE" []
let i_car = prim "CAR" []
let i_cdr = prim "CDR" []
let i_if bt bf = prim "IF" [ bt; bf ]
let i_eq = prim "EQ" []
let i_neq = prim "NEQ" []
let i_not = prim "NOT" []
let i_nil ty = prim "NIL" [ ty ]
let i_apply = prim "APPLY" []
let i_exec = prim "EXEC" []
let c_list l = prim "" l
let i_cons = prim "CONS" []
let i_push t i = prim "PUSH" [ t; i ]
let bin_app lhs op rhs = [ rhs; lhs; op ]
let add lhs rhs = bin_app lhs i_add rhs
let sub lhs rhs = bin_app lhs i_sub rhs
let mul lhs rhs = bin_app lhs i_mul rhs
let ediv lhs rhs = bin_app lhs i_ediv rhs
let andb lhs rhs = bin_app lhs i_and rhs
let orb lhs rhs = bin_app lhs i_or rhs
let compare lhs rhs = bin_app lhs i_compare rhs
let eq lhs rhs = bin_app lhs i_eq rhs
let neq lhs rhs = bin_app lhs i_eq rhs
let apply f a = [ f; a; i_apply ]
let exec f a = [ f; a; i_exec ]
let ( |+ ) = add
let ( |- ) = sub
let ( |* ) = mul
let ( |/ ) = ediv
let ( |&& ) = andb
let ( ||| ) = orb
let ( |? ) = compare
let ( |= ) = eq
let ( |<> ) = neq
let cons h t = [ t; h; i_cons ]
let pair a b = [ b; a; i_pair ]
let nil t = i_nil t
let fst x = [ x; i_car ]
let snd x = [ x; i_cdr ]
let ( |@@ ) = apply
let ( |@ ) = exec

let script ~parameter ~storage code =
  seq
    [
      prim "parameter" [ parameter ];
      prim "storage" [ storage ];
      prim "code" [ code ];
    ]
