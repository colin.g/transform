(** See src/proto_alpha/lib_protocol/michelson_v1_primitives.ml
 *)
let primitives =
  [
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, "FOR" BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("parameter", "parameter");
    ("storage", "storage");
    ("code", "code");
    ("False", "False");
    ("Elt", "Elt");
    ("Left", "Left");
    ("None", "None");
    ("Pair", "Pair");
    ("Right", "Right");
    ("Some", "Some");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, "FOR" BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("True", "True");
    ("Unit", "Unit");
    ("PACK", "PACK");
    ("UNPACK", "UNPACK");
    ("BLAKE2B", "BLAKE2B");
    ("SHA256", "SHA256");
    ("SHA512", "SHA512");
    ("ABS", "ABS");
    ("ADD", "ADD");
    ("AMOUNT", "AMOUNT");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, "FOR" BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("AND", "AND");
    ("BALANCE", "BALANCE");
    ("CAR", "CAR");
    ("CDR", "CDR");
    ("CHECK_SIGNATURE", "CHECK_SIGNATURE");
    ("COMPARE", "COMPARE");
    ("CONCAT", "CONCAT");
    ("CONS", "CONS");
    ("CREATE_ACCOUNT", "CREATE_ACCOUNT");
    ("CREATE_CONTRACT", "CREATE_CONTRACT");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, "FOR" BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("IMPLICIT_ACCOUNT", "IMPLICIT_ACCOUNT");
    ("DIP", "DIP");
    ("DROP", "DROP");
    ("DUP", "DUP");
    ("EDIV", "EDIV");
    ("EMPTY_MAP", "EMPTY_MAP");
    ("EMPTY_SET", "EMPTY_SET");
    ("EQ", "EQ");
    ("EXEC", "EXEC");
    ("FAILWITH", "FAILWITH");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, "FOR" BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("GE", "GE");
    ("GET", "GET");
    ("GT", "GT");
    ("HASH_KEY", "HASH_KEY");
    ("IF", "IF");
    ("IF_CONS", "IF_CONS");
    ("IF_LEFT", "IF_LEFT");
    ("IF_NONE", "IF_NONE");
    ("INT", "INT");
    ("LAMBDA", "LAMBDA");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, "FOR" BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("LE", "LE");
    ("LEFT", "LEFT");
    ("LOOP", "LOOP");
    ("LSL", "LSL");
    ("LSR", "LSR");
    ("LT", "LT");
    ("MAP", "MAP");
    ("MEM", "MEM");
    ("MUL", "MUL");
    ("NEG", "NEG");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, "FOR" BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("NEQ", "NEQ");
    ("NIL", "NIL");
    ("NONE", "NONE");
    ("NOT", "NOT");
    ("NOW", "NOW");
    ("OR", "OR");
    ("PAIR", "PAIR");
    ("PUSH", "PUSH");
    ("RIGHT", "RIGHT");
    ("SIZE", "SIZE");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, "FOR" BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("SOME", "SOME");
    ("SOURCE", "SOURCE");
    ("SENDER", "SENDER");
    ("SELF", "SELF");
    ("STEPS_TO_QUOTA", "STEPS_TO_QUOTA");
    ("SUB", "SUB");
    ("SWAP", "SWAP");
    ("TRANSFER_TOKENS", "TRANSFER_TOKENS");
    ("SET_DELEGATE", "SET_DELEGATE");
    ("UNIT", "UNIT");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, "FOR" BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("UPDATE", "UPDATE");
    ("XOR", "XOR");
    ("ITER", "ITER");
    ("LOOP_LEFT", "LOOP_LEFT");
    ("ADDRESS", "ADDRESS");
    ("CONTRACT", "CONTRACT");
    ("ISNAT", "ISNAT");
    ("CAST", "CAST");
    ("RENAME", "RENAME");
    ("bool", "bool");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, "FOR" BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("contract", "contract");
    ("int", "int");
    ("key", "key");
    ("key_hash", "key_hash");
    ("lambda", "lambda");
    ("list", "list");
    ("map", "map");
    ("big_map", "big_map");
    ("nat", "nat");
    ("option", "option");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, "FOR" BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("or", "or");
    ("pair", "pair");
    ("set", "set");
    ("signature", "signature");
    ("string", "string");
    ("bytes", "bytes");
    ("mutez", "mutez");
    ("timestamp", "timestamp");
    ("unit", "unit");
    ("operation", "operation");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, "FOR" BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("address", "address");
    (* Alpha_002 addition *)
    ("SLICE", "SLICE");
    (* Alpha_005 addition *)
    ("DIG", "DIG");
    ("DUG", "DUG");
    ("EMPTY_BIG_MAP", "EMPTY_BIG_MAP");
    ("APPLY", "APPLY");
    ("chain_id", "chain_id");
    ("CHAIN_ID", "CHAIN_ID")
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, FOR BACKWARD COMPATIBILITY OF THE ENCODING. *)
    (* Alpha_008 addition *);
    ("LEVEL", "LEVEL");
    ("SELF_ADDRESS", "SELF_ADDRESS");
    ("never", "never");
    ("NEVER", "NEVER");
    ("UNPAIR", "UNPAIR");
    ("VOTING_POWER", "VOTING_POWER");
    ("TOTAL_VOTING_POWER", "TOTAL_VOTING_POWER");
    ("KECCAK", "KECCAK");
    ("SHA3", "SHA3");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, FOR BACKWARD COMPATIBILITY OF THE ENCODING. *)
    (* Alpha_008 addition *)
    ("PAIRING_CHECK", "PAIRING_CHECK");
    ("bls12_381_g1", "bls12_381_g1");
    ("bls12_381_g2", "bls12_381_g2");
    ("bls12_381_fr", "bls12_381_fr");
    ("sapling_state", "sapling_state");
    ("sapling_transaction_deprecated", "sapling_transaction_deprecated");
    ("SAPLING_EMPTY_STATE", "SAPLING_EMPTY_STATE");
    ("SAPLING_VERIFY_UPDATE", "SAPLING_VERIFY_UPDATE");
    ("ticket", "ticket");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, FOR BACKWARD COMPATIBILITY OF THE ENCODING. *)
    (* Alpha_008 addition *)
    ("TICKET", "TICKET");
    ("READ_TICKET", "READ_TICKET");
    ("SPLIT_TICKET", "SPLIT_TICKET");
    ("JOIN_TICKETS", "JOIN_TICKETS");
    ("GET_AND_UPDATE", "GET_AND_UPDATE");
    (* Alpha_011 addition *)
    ("chest", "chest");
    ("chest_key", "chest_key");
    ("OPEN_CHEST", "OPEN_CHEST");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, FOR BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("VIEW", "VIEW");
    ("view", "view");
    ("constant", "constant");
    (* Alpha_012 addition *)
    ("SUB_MUTEZ", "SUB_MUTEZ");
    (* Alpha_013 addition *)
    ("tx_rollup_l2_address", "tx_rollup_l2_address");
    ("MIN_BLOCK_TIME", "MIN_BLOCK_TIME");
    ("sapling_transaction", "sapling_transaction");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, FOR BACKWARD COMPATIBILITY OF THE ENCODING. *)
    (* Alpha_014 addition *)
    ("EMIT", "EMIT");
    (* Alpha_015 addition *)
    ("Lambda_rec", "Lambda_rec");
    ("LAMBDA_REC", "LAMBDA_REC")
    (* New instructions must be added here, for backward compatibility of the encoding. *)
    (* Keep the comment above at the end of the list *);
  ]
