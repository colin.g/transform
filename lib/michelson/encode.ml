open Tezos_micheline

type t = (int, string) Micheline.node

let of_canonical_micheline m = Micheline.(root m)
let to_canonical_micheline m = Micheline.strip_locations m

let of_micheline_node ?(make_location = fun _ -> 0) node =
  Micheline.map_node make_location (fun x -> x) node

let pp ppf m =
  Format.fprintf ppf "%a" Micheline_printer.print_expr
    (Micheline_printer.printable Base.Fn.id (to_canonical_micheline m))

let expr_encoding =
  Micheline_encoding.canonical_encoding_v2 ~variant:"michelson_v1"
    (* Data_encoding.Encoding.string *)
    (let open Data_encoding in
    def "michelson.v1.primitives" @@ string_enum Michelson_v1.primitives)

let encoding =
  let open Data_encoding in
  conv to_canonical_micheline of_canonical_micheline expr_encoding

let of_json (json : Ezjsonm.value) = Data_encoding.Json.destruct encoding json
let to_json mich : Ezjsonm.value = Data_encoding.Json.construct encoding mich
let loc = Micheline.dummy_location

let to_str_json m =
  let u = m in
  let u_json = to_json u in
  Ezjsonm.value_to_string u_json
