open Tezos_micheline
open Encode

let fmt_failwith fmt x =
  Printf.sprintf "Untyped.C.concrete: %s"
    (fmt Format.std_formatter x;
     Format.flush_str_formatter ())
  |> failwith

let concrete s =
  match Micheline_parser.tokenize s with
  | tokens, [] -> (
      match Micheline_parser.parse_expression ~check:false tokens with
      | node, [] ->
          of_micheline_node
            ~make_location:(fun loc -> loc.Micheline_parser.start.byte)
            node
      | _, errs ->
          fmt_failwith Tezos_error_monad.Error_monad.pp_print_top_error_of_trace
            errs)
  | _, errs ->
      fmt_failwith Tezos_error_monad.Error_monad.pp_print_top_error_of_trace
        errs
