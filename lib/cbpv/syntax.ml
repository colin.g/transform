module Types = struct
  type tykon = string * int
  and typ = TyVar of string | TyApp of tykon * typ list
  and tycomp = TyComp
  and tyval = TyVal
end

module Terms = struct
  open Types

  type _ term =
    | TypedTerm : typ * 'a term -> 'a term
    | Const : const -> tyval term
    | Prim : 'a prim -> 'a term
    | Var : string -> tyval term
    | Fun : string * tycomp term -> tycomp term
    | App : tycomp term * tyval term -> tycomp term
    | Rec : string * tycomp term -> tycomp term
    | Pair : tyval term * tyval term -> tyval term
    | Let : string * tyval term * tycomp term -> tycomp term
    | To : tycomp term * string * tycomp term -> tycomp term
    | Force : tyval term -> tycomp term
    | Thunk : tycomp term -> tyval term
    | Return : tyval term -> tycomp term
    | Fby : tyval term * tyval term -> tyval term
    | IfThenElse : tyval term * tycomp term * tycomp term -> tycomp term

  and const = Unit | Int of int | Bool of bool | Nil

  and _ prim =
    | Not : tyval term -> tyval prim
    | Fst : tyval term -> tyval prim
    | Snd : tyval term -> tyval prim
    | Plus : tyval term * tyval term -> tyval prim
    | Minus : tyval term * tyval term -> tyval prim
    | Mult : tyval term * tyval term -> tyval prim
    | Div : tyval term * tyval term -> tyval prim
    | Eq : tyval term * tyval term -> tyval prim
    | NEq : tyval term * tyval term -> tyval prim
    | AndB : tyval term * tyval term -> tyval prim
    | OrB : tyval term * tyval term -> tyval prim
    | Cons : tyval term * tyval term -> tyval prim
    | CompNZ : tyval term -> tyval prim
    | HdS : tyval term -> tyval prim
    | TlS : tyval term -> tyval prim
    | HdL : tyval term -> tyval prim
    | TlL : tyval term -> tyval prim

  let thunkt = ("U", 1)
  let arrowt = ("->", 2)
  let streamt = ("stream", 1)
  let intt = ("int", 0)
  let boolt = ("bool", 0)
  let ut = ("U", 1)
  let ft = ("F", 1)
  let prodt = ("*", 2)
  let ty_app cons params = TyApp (cons, params)
  let ty_arrow a b = ty_app arrowt [ a; b ]
  let ty_product a b = ty_app prodt [ a; b ]
  let ty_stream a = ty_app streamt [ a ]
  let ty_int = ty_app intt []
  let ty_bool = ty_app boolt []
  let ty_u t = ty_app ut [ t ]
  let ty_f t = ty_app ft [ t ]
  let tfst x = Prim (Fst x)
  let tsnd x = Prim (Snd x)
  let tlet x t u = Let (x, t, u)

  let tlet_rec x ty t u =
    Let (x, TypedTerm (ty_u ty, Thunk (TypedTerm (ty, Rec (x, t)))), u)

  let ( *: ) x y = Fby (x, y)
  let ( =: ) t x u = To (t, x, u)
  let ( |>! ) u t = App (t, u)
  let ( @@! ) t u = App (t, u)
  let tthunk t = Thunk t
  let tfun x t = Fun (x, t)
  let tforce t = Force t
  let ( !! ) = tforce
  let treturn t = Return t
  let trec x t = Rec (x, t)
  let t x = Var x
  let ( +! ) x t = Prim (Plus (x, t))
  let ( *! ) x t = Prim (Mult (x, t))
  let ( -! ) x t = Prim (Minus (x, t))
  let tunit = Const Unit
  let tint x = Const (Int x)
  let tprod x y = Pair (x, y)
  let tcons x y = Prim (Cons (x, y))
  let tif c x y = IfThenElse (c, x, y)
  let tfunn xs t = List.fold_right (fun x t -> tfun x t) xs t
end
