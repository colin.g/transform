let document_to_string printer doc =
  let buf = Buffer.create 256 and doc = printer doc in
  PPrint.ToBuffer.pretty 0.9 80 buf doc;
  Buffer.contents buf

let document_to_format printer fmt doc =
  let doc = printer doc in
  PPrint.ToFormatter.pretty 0.9 80 fmt doc

let process_program ~lexer_init ~input =
  try Parser.program Lexer.token (lexer_init input)
  with Sys_error msg -> failwith (Format.asprintf "%s during parsing." msg)

let parse_program, parse_file =
  let parse lexer_init input = process_program ~lexer_init ~input in
  let parse_string = parse Lexing.from_string in
  let parse_file f = parse Lexing.from_channel (open_in f) in
  (parse_string, parse_file)
