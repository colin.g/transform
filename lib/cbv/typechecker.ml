open Syntax
open Terms
module T = Types

module S = struct
  type 'a structure =
    | TyInt
    | TyUnit
    | TyBool
    | TyArrow of ('a list * 'a)
    | TyProd of ('a * 'a)
    | TyList of 'a
    | TyStream of 'a

  let map f s =
    match s with
    | TyArrow (t1, t2) -> TyArrow (List.map f t1, f t2)
    | TyProd (t1, t2) -> TyProd (f t1, f t2)
    | TyList ty -> TyList (f ty)
    | TyStream ty -> TyStream (f ty)
    | TyBool -> TyBool
    | TyUnit -> TyUnit
    | TyInt -> TyInt

  let iter f s =
    match s with
    | TyArrow (t1, t2) ->
        List.iter f t1;
        f t2
    | TyProd (t1, t2) ->
        f t1;
        f t2
    | TyList ty -> f ty
    | TyStream ty -> f ty
    | TyBool | TyUnit | TyInt -> ()

  let fold f t acc =
    match t with
    | TyArrow (t1, t2) ->
        let acc = List.fold_right f t1 acc in
        f t2 acc
    | TyList ty | TyStream ty -> f ty acc
    | TyProd (t1, t2) ->
        let acc = f t1 acc in
        f t2 acc
    | _ -> acc

  exception Iter2

  let iter2 f t u =
    match (t, u) with
    | TyArrow (t1, t2), TyArrow (u1, u2) ->
        (try List.iter2 f t1 u1 with Invalid_argument _ -> raise Iter2);
        f t2 u2
    | TyProd (t1, t2), TyProd (u1, u2) ->
        f t1 u1;
        f t2 u2
    | TyList t, TyList u -> f t u
    | TyStream t, TyStream u -> f t u
    | TyBool, TyBool | TyUnit, TyUnit | TyInt, TyInt -> ()
    | _ -> raise Iter2

  exception InconsistentConjunction = Iter2

  let conjunction f t u =
    iter2 f t u;
    t

  open PPrint

  let kw_arrow = string "->"
  let kw_unit_ty = string "unit"
  let kw_int_ty = string "int"
  let kw_bool_ty = string "bool"
  let kw_list_ty = string "list"
  let kw_star = star
  let kw_stream = string "stream"

  let rec print_type_arrow leaf t =
    match t with
    | TyArrow ([], _) -> assert false
    | TyArrow ([ t1 ], t2) -> infix 2 1 kw_arrow (leaf t1) (leaf t2)
    | TyArrow (args_tys, t2) ->
        infix 2 1 kw_arrow
          (surround 2 0 lparen (separate_map kw_star leaf args_tys) rparen)
          (leaf t2)
    | _ -> print_type_atom leaf t

  and print_type_atom leaf t =
    match t with
    | TyUnit -> kw_unit_ty
    | TyInt -> kw_int_ty
    | TyProd (ty1, ty2) -> braces (separate_map kw_star leaf [ ty1; ty2 ])
    | TyArrow _ -> parens (print_type leaf t)
    | TyList ty -> prefix 0 1 (leaf ty) kw_list_ty
    | TyStream ty -> prefix 0 1 (leaf ty) kw_stream
    | TyBool -> kw_bool_ty

  and print_type leaf ty = group (print_type_arrow leaf ty)
  and pprint leaf = print_type leaf
end

module O = struct
  type tyvar = int
  type 'a structure = 'a S.structure
  type ty = T.ty

  let inject n = 2 * n
  let variable x = T.TyVar x

  let structure t =
    match t with
    | S.TyUnit -> T.TyUnit
    | S.TyBool -> T.TyBool
    | S.TyInt -> T.TyInt
    | S.TyList t -> T.TyList t
    | S.TyStream t -> T.TyStream t
    | S.TyArrow (t1, t2) -> T.TyArrow (t1, t2)
    | S.TyProd (t1, t2) -> T.TyProd (t1, t2)

  let mu x t =
    ignore (x, t);
    assert false

  type scheme = tyvar list * ty
end

module X = struct
  type t = Var of string | Sym of int

  let fresh : unit -> t =
    let gensym = Inferno.Utils.gensym () in
    fun () -> Sym (gensym ())

  let hash = Hashtbl.hash
  let compare v1 v2 = Stdlib.compare v1 v2
  let equal v1 v2 = compare v1 v2 = 0
  let to_string v = match v with Var v -> v | Sym n -> string_of_int n
end

module Solver = Inferno.Solver.Make (X) (S) (O)
open Solver

let ty_unit = S.TyUnit
let ty_int = S.TyInt
let ty_bool = S.TyBool
let ty_arrow t1 t2 = S.TyArrow ([ t1 ], t2)
let ty_product t1 t2 = S.TyProd (t1, t2)
let ty_list t = S.TyList t
let ty_stream t = S.TyStream t

let rec convert_deep (ty : T.ty) =
  let conv = convert_deep in
  let deeps t = DeepStructure t in
  match ty with
  | TyVar _ -> failwith "types should be monomorph"
  | TyUnit -> deeps S.TyUnit
  | TyBool -> deeps S.TyBool
  | TyInt -> deeps S.TyInt
  | TyArrow (ty1, ty2) ->
      let ty1 = List.map conv ty1 in
      let ty2 = conv ty2 in
      deeps (S.TyArrow (ty1, ty2))
  | TyList ty -> deeps (S.TyList (conv ty))
  | TyStream ty -> deeps (S.TyStream (conv ty))
  | TyProd (ty1, ty2) -> deeps (S.TyProd (conv ty1, conv ty2))

let convert ty =
  let deep_ty = convert_deep ty in
  deep deep_ty

exception Poly of T.ty

let rec is_var (ty : T.ty) =
  match ty with
  | TyVar _ -> Some ty
  | TyInt | TyUnit | TyBool -> None
  | TyList t | TyStream t -> is_var t
  | TyArrow (x, y) ->
      List.fold_left
        (fun maybe_var ty -> match maybe_var with None -> is_var ty | t -> t)
        None (y :: x)
  | TyProd (ty1, ty2) -> ( match is_var ty1 with None -> is_var ty2 | t -> t)

let is_monomorphic ty = match is_var ty with Some _ -> false | _ -> true

let conj_of_term f cs t v =
  let+ a = f t v and+ b = cs in
  (fun (x, xs) -> x :: xs) (a, b)

let conj_of_list f ts vs = List.fold_left2 (conj_of_term f) (pure []) ts vs

let rec hastype ty_env t w =
  match t with
  | Var x ->
      let+ _ = instance (Var x) w and+ ty = decode w in
      TypedTerm (Var x, ty)
  | Lam (x, ty, t) ->
      let@ v1 = convert ty in
      let@ v2 = exist in
      let+ () = w --- ty_arrow v1 v2
      and+ u' = def (Var x) v1 (hastype ((x, ty) :: ty_env) t v2)
      and+ ty2 = decode v2 in
      let out_ty = T.TyArrow ([ ty ], ty2) in
      if is_monomorphic out_ty then TypedTerm (Lam (x, ty, u'), out_ty)
      else raise (Poly out_ty)
  | Lit Unit as k ->
      let+ () = w --- ty_unit in
      TypedTerm (k, TyUnit)
  | Lit (Int _) as k ->
      let+ () = w --- ty_int in
      TypedTerm (k, TyInt)
  | Lit (Bool _) as k ->
      let+ () = w --- ty_bool in
      TypedTerm (k, TyBool)
  | Pair (t1, t2) ->
      let@ v1 = exist in
      let@ v2 = exist in
      let+ () = w --- ty_product v1 v2
      and+ t1 = hastype ty_env t1 v1
      and+ t2 = hastype ty_env t2 v2
      and+ ty1 = decode v1
      and+ ty2 = decode v2 in
      TypedTerm (Pair (t1, t2), TyProd (ty1, ty2))
  | Stream (t1, t2) ->
      let@ a = exist in
      let@ b = exist in
      let@ vunit = exist in
      let@ vint = exist in
      let+ () = w --- ty_stream a
      and+ () = vint --- ty_int
      and+ () = vunit --- ty_unit
      and+ () = b --- ty_arrow vint w
      and+ t1 = hastype ty_env t1 a
      and+ t2 = lift (hastype ty_env) t2 (ty_arrow vunit b)
      and+ ty = decode w in
      TypedTerm (Stream (t1, t2), ty)
  | List xs ->
      let@ v = exist in
      let+ () = w --- ty_list v
      and+ xs =
        List.fold_right
          (fun term co -> conj_of_term (hastype ty_env) co term v)
          xs (pure [])
      and+ ty = decode w in
      TypedTerm (List xs, ty)
  | Not x ->
      let+ () = w --- S.TyBool and+ t = lift (hastype ty_env) x ty_bool in
      TypedTerm (Not t, TyBool)
  | CompNZ x ->
      let@ ty = exist in
      let+ () = w --- S.TyBool and+ t = hastype ty_env x ty in
      TypedTerm (CompNZ t, TyBool)
  | Fst x ->
      let@ v = exist in
      let+ t = lift (hastype ty_env) x (ty_product w v) and+ ty = decode w in
      TypedTerm (Fst t, ty)
  | Snd x ->
      let@ u = exist in
      let+ t = lift (hastype ty_env) x (ty_product u w) and+ ty = decode w in
      TypedTerm (Snd t, ty)
  | Head x ->
      let+ t = lift (hastype ty_env) x (ty_stream w) and+ ty = decode w in
      TypedTerm (Head t, ty)
  | Tail x ->
      let@ vint = exist in
      let@ vunit = exist in
      let@ a = exist in
      let@ u = exist in
      let@ v = exist in
      let+ () = vint --- ty_int
      and+ () = vunit --- ty_unit
      and+ () = u --- ty_stream v
      and+ () = a --- ty_arrow vint u
      and+ () = w --- ty_arrow vunit a
      and+ ty = decode w
      and+ t = lift (hastype ty_env) x (ty_stream v) in
      TypedTerm (Tail t, ty)
  | If (c, tt, ff) ->
      let@ t = exist in
      let+ () = w -- t
      and+ c = lift (hastype ty_env) c TyBool
      and+ bt = hastype ty_env tt w
      and+ bf = hastype ty_env ff w
      and+ ty = decode t in
      TypedTerm (If (c, bt, bf), ty)
  | BinArith (t1, op, t2) ->
      let+ () = w --- S.TyInt
      and+ t1 = hastype ty_env t1 w
      and+ t2 = hastype ty_env t2 w in
      TypedTerm (BinArith (t1, op, t2), TyInt)
  | BinBool (t1, ((And | Or) as op), t2) ->
      let+ () = w --- S.TyBool
      and+ t1 = hastype ty_env t1 w
      and+ t2 = hastype ty_env t2 w in
      TypedTerm (BinBool (t1, op, t2), TyBool)
  | BinBool (t1, op, t2) ->
      let@ a = exist in
      let+ () = w --- S.TyBool
      and+ t1 = hastype ty_env t1 a
      and+ t2 = hastype ty_env t2 a in
      TypedTerm (BinBool (t1, op, t2), TyBool)
  | Cons (t1, t2) ->
      let@ ty1 = exist in
      let+ () = w --- ty_list ty1
      and+ t1 = hastype ty_env t1 ty1
      and+ t2 = hastype ty_env t2 w
      and+ ty = decode w in
      TypedTerm (Cons (t1, t2), ty)
  | App (t1, t2) ->
      let@ v = exist in
      let+ t1 = lift (hastype ty_env) t1 (ty_arrow v w)
      and+ t2 = hastype ty_env t2 v
      and+ ty = decode w in
      TypedTerm (App (t1, t2), ty)
  | Let (x, ty, t1, t2) -> (
      let@ ty1 = convert ty in
      let+ t1 = hastype ty_env t1 ty1
      and+ t2 = def (Var x) ty1 (hastype ((x, ty) :: ty_env) t2 w) in
      match (t1, t2) with
      | TypedTerm (_, ty1), TypedTerm (_, ty2) ->
          TypedTerm (Let (x, ty1, t1, t2), ty2)
      | _ -> assert false)
  | LetRec (x, ty, t1, t2) -> (
      let env = (x, ty) :: ty_env in
      let@ ty1 = convert ty in
      let+ t1 = def (Var x) ty1 (hastype env t1 ty1)
      and+ t2 = def (Var x) ty1 (hastype env t2 w) in
      match (t1, t2) with
      | (TypedTerm (_, ty1) as t1), (TypedTerm (_, ty2) as t2) ->
          TypedTerm (LetRec (x, ty1, t1, t2), ty2)
      | _ -> assert false)
  | TypedTerm (t, ty) ->
      let@ v = convert ty in
      let+ () = w -- v and+ t = hastype ty_env t w in
      TypedTerm (t, ty)

exception Unbound = Solver.Unbound
exception Unify = Solver.Unify
exception Cycle = Solver.Cycle

let elaborate t =
  solve ~rectypes:false
    (let0
       (let@ w = exist in
        let+ t = (hastype [] t) w in
        t))
  |> snd
