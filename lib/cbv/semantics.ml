open Syntax
open Values

module Env = struct
  type t = Values.env

  let empty = []
  let bind env x v = (x, ref v) :: env

  let update env x v =
    match List.assoc_opt x env with
    | Some vref ->
        vref := v;
        env
    | None -> env

  let get env x =
    match List.assoc_opt x env with
    | Some vref -> !vref
    | None -> failwith "Unbound variable!\n"

  let pretty_env e =
    let open PPrint in
    OCaml.list
      (fun (x, y) -> string x ^^ string " := " ^^ Printer.print_value !y)
      e
end

module Cont = struct
  open Terms
  (* open Env *)

  type redex = Redex of prim * term list * value list * env
  and cont = redex list

  and prim =
    | PApp
    | PPair
    | PCar
    | PCdr
    | PAdd
    | PMinus
    | PMult
    | PDiv
    | PEq
    | PNEq
    | PAnd
    | POr
    | PCompNZ
    | PNot
    | PIf
    | PCons
    | PStream

  let pretty_prim p =
    let open PPrint in
    match p with
    | PApp -> string "PApp"
    | PPair -> string "PPair"
    | PCar -> string "PCar"
    | PCdr -> string "PCdr"
    | PAdd -> string "PAdd"
    | PMinus -> string "PMinus"
    | PMult -> string "PMult"
    | PDiv -> string "PDiv"
    | PEq -> string "PEq"
    | PNEq -> string "PNEq"
    | PAnd -> string "PAnd"
    | POr -> string "POr"
    | PNot -> string "PNot"
    | PCompNZ -> string "PCompNZ"
    | PCons -> string "PCons"
    | PIf -> string "PIf"
    | PStream -> string "PStream"

  let pretty_cont k =
    let open PPrint in
    let open Printer in
    OCaml.list
      (function
        | Redex (prim, args, vs, _e) ->
            group
              (string "Redex"
              ^^ OCaml.tuple
                   [
                     pretty_prim prim;
                     OCaml.list print_term args;
                     OCaml.list print_value vs (* pretty_env e; *);
                   ]))
      k

  let op_of_prim prim =
    match prim with PAdd -> Add | PMinus -> Minus | _ -> assert false

  let prim_of_arith_op op =
    match op with Add -> PAdd | Minus -> PMinus | Mult -> PMult | Div -> PDiv
end

module BigStep = struct
  open Terms
  open Env

  let rec eval env t =
    match t with
    | Lit l -> eval_litteral l
    | Pair (x, y) -> VPair (eval env x, eval env y)
    | Var x -> get env x
    | Lam (x, _, t) -> VClo (x, t, env)
    | App (f, x) -> eval_application (eval env f) (eval env x)
    | Let (x, _, t1, t2) -> eval (bind env x (eval env t1)) t2
    | LetRec (x, _, t1, t2) ->
        let env = bind env x VUnit in
        let vx = eval env t1 in
        let env = update env x vx in
        eval env t2
    | Fst x -> eval_fst env x
    | Snd x -> eval_snd env x
    | Head x -> eval_fst env x
    | Tail x -> eval_snd env x
    | Stream (x, y) -> VStream (eval env x, eval env y)
    | Cons (x, xs) ->
        let x = eval env x in
        let xs = match eval env xs with VList xs -> xs | _ -> assert false in
        VList (x :: xs)
    | BinArith (t1, op, t2) -> eval_bin_arith env op t1 t2
    | BinBool (t1, op, t2) -> eval_bin_bool env op t1 t2
    | Not t -> eval_not env t
    | CompNZ t -> eval_compnz env t
    | If (c, tt, ff) -> eval_if env c tt ff
    | TypedTerm (term, _) -> eval env term
    | List xs -> VList (List.map (eval env) xs)

  and eval_if env c tt ff =
    let branch = match eval env c with VBool true -> tt | _ -> ff in
    eval env branch

  and eval_not env t =
    match eval env t with VBool t -> VBool (not t) | _ -> assert false

  and eval_compnz env t =
    match eval env t with VInt 0 -> VBool true | _ -> VBool false

  and eval_bin_arith env op t1 t2 =
    match (op, eval env t1, eval env t2) with
    | Add, VInt t1, VInt t2 -> VInt (t1 + t2)
    | Minus, VInt t1, VInt t2 -> VInt (t1 - t2)
    | Mult, VInt t1, VInt t2 -> VInt (t1 * t2)
    | Div, VInt t1, VInt t2 -> VInt (t1 / t2)
    | _ -> assert false

  and eval_bin_bool env op t1 t2 =
    match (op, eval env t1, eval env t2) with
    | Eq, t1, t2 -> VBool (t1 = t2)
    | NEq, t1, t2 -> VBool (t1 <> t2)
    | And, VBool t1, VBool t2 -> VBool (t1 && t2)
    | Or, VBool t1, VBool t2 -> VBool (t1 || t2)
    | _ -> assert false

  and eval_projection proj env x =
    let p =
      match eval env x with
      | VPair (x, y) -> (x, y)
      | VStream (x, y) -> (x, y)
      | _ -> assert false
    in
    proj p

  and eval_fst env = eval_projection fst env
  and eval_snd env = eval_projection snd env

  and eval_litteral = function
    | Unit -> VUnit
    | Int i -> VInt i
    | Bool b -> VBool b

  and eval_application fv v =
    match fv with
    | VClo (x, t, env) ->
        let env = bind env x v in
        eval env t
    | _ -> failwith "Semantics big-step: cannot apply a non functionnal value\n"

  let value_of = eval empty

  let%test _ = value_of tunit = vunit
  let%test _ = value_of tapp_id_unit = vunit
  let%test _ = value_of tfun_id = VClo ("x", tx, empty)
  let%test _ = value_of tapp_id_idy_unit = vunit

  let%test _ =
    value_of
      (If
         ( CompNZ (BinArith (Lit (Int 1), Minus, Lit (Int 1))),
           Lit (Int 42),
           Lit (Int 41) ))
    = VInt 42

  let%test _ =
    value_of (List [ Lit (Int 1); BinArith (Lit (Int 1), Add, Lit (Int 41)) ])
    = VList [ VInt 1; VInt 42 ]
end

module CPSBigStep = struct
  open Terms
  open Env

  let id x = x

  let rec cps_eval env t k =
    match t with
    | Lit l -> cps_eval_litteral l k
    | Var x -> k @@ get env x
    | Lam (x, _, t) -> k @@ VClo (x, t, env)
    | Pair (x, y) -> cps_eval_pair env x y k
    | Stream (x, y) -> cps_eval_stream env x y k
    | Fst x -> cps_eval_proj fst env x k
    | Snd x -> cps_eval_proj snd env x k
    | Head x -> cps_eval_proj fst env x k
    | Tail x -> cps_eval_proj snd env x k
    | App (t1, t2) -> cps_eval_app env t1 t2 k
    | Let (x, ty, t1, t2) -> cps_eval_app env (Lam (x, ty, t2)) t1 k
    | LetRec (x, _, Lam (y, _, b), t2) ->
        let env = bind env x VUnit in
        let env = update env x (VClo (y, b, env)) in
        cps_eval env t2 k
    | BinArith (t1, op, t2) -> cps_eval_bin_arith env op t1 t2 k
    | BinBool (t1, Eq, t2) -> cps_eval_comp env ( = ) t1 t2 k
    | BinBool (t1, NEq, t2) -> cps_eval_comp env ( <> ) t1 t2 k
    | BinBool (t1, And, t2) -> cps_eval_bin_bool env ( && ) t1 t2 k
    | BinBool (t1, Or, t2) -> cps_eval_bin_bool env ( || ) t1 t2 k
    | Cons (x, xs) ->
        cps_eval env x (fun x ->
            cps_eval env xs (function
              | VList xs -> k @@ VList (x :: xs)
              | _ -> assert false))
    | Not x -> cps_eval_not env x k
    | CompNZ x -> cps_eval_compnz env x k
    | If (c, tt, ff) -> cps_eval_if env c tt ff k
    | TypedTerm (term, _) -> cps_eval env term k
    | List [] -> k @@ VList []
    | List xs -> cps_eval_list env k xs
    | LetRec _ -> assert false

  and cps_eval_list env k xs =
    let rec aux xs acc =
      match xs with
      | [] -> acc
      | x :: xs -> (
          match aux xs acc with
          | VList acc -> cps_eval env x (fun v -> VList (v :: acc))
          | _ -> assert false)
    in
    k @@ aux xs (VList [])

  and cps_eval_if env c tt ff k =
    cps_eval env c @@ function
    | VBool true -> cps_eval env tt k
    | _ -> cps_eval env ff k

  and cps_eval_compnz env x k =
    cps_eval env x @@ function
    | VInt 0 -> k @@ VBool true
    | _ -> k @@ VBool false

  and cps_eval_not env x k =
    cps_eval env x @@ function
    | VBool t -> k @@ VBool (not t)
    | _ -> assert false

  and cps_eval_litteral l k =
    match l with
    | Unit -> k @@ VUnit
    | Int i -> k @@ VInt i
    | Bool b -> k @@ VBool b

  and cps_eval_proj proj env x k =
    cps_eval env x @@ function
    | VPair x -> k @@ proj x
    | VStream (x, y) -> k @@ proj (x, y)
    | _ -> failwith "Semantics cps big-step: can not project a non pair term"

  and interpret_op op =
    match op with Add -> ( + ) | Minus -> ( - ) | Mult -> ( * ) | Div -> ( / )

  and cps_eval_bin_arith env op t1 t2 k =
    cps_eval env t1 @@ function
    | VInt t1 -> (
        cps_eval env t2 @@ function
        | VInt t2 -> k @@ VInt (interpret_op op t1 t2)
        | _ -> assert false)
    | _ -> assert false

  and cps_eval_bin_bool env op t1 t2 k =
    cps_eval env t1 @@ function
    | VBool t1 -> (
        cps_eval env t2 @@ function
        | VBool t2 -> k @@ VBool (op t1 t2)
        | _ -> assert false)
    | _ -> assert false

  and cps_eval_comp env op t1 t2 k =
    cps_eval env t1 @@ function
    | t1 -> ( cps_eval env t2 @@ function t2 -> k @@ VBool (op t1 t2))

  and cps_eval_pair env x y k =
    cps_eval env x (fun f -> cps_eval env y (fun s -> k @@ VPair (f, s)))

  and cps_eval_stream env x y k =
    cps_eval env x (fun f -> cps_eval env y (fun s -> k @@ VStream (f, s)))

  and cps_eval_app env t1 t2 k =
    cps_eval env t2 @@ fun (v : value) ->
    cps_eval env t1 @@ function
    | VClo (x, t, cenv) -> cps_eval (bind cenv x v) t k
    | _ ->
        failwith
          "Semantics cps big-step: cannot apply a non functionnal value\n"

  let value_of x = cps_eval empty x id

  let%test _ = value_of tunit = vunit
  let%test _ = value_of tapp_id_unit = vunit
  let%test _ = value_of tfun_id = VClo ("x", tx, empty)
  let%test _ = value_of tapp_id_idy_unit = vunit

  let%test _ =
    value_of
      (If
         ( CompNZ (BinArith (Lit (Int 1), Minus, Lit (Int 1))),
           Lit (Int 42),
           Lit (Int 41) ))
    = VInt 42

  let%test _ =
    value_of (List [ Lit (Int 1); BinArith (Lit (Int 1), Add, Lit (Int 41)) ])
    = VList [ VInt 1; VInt 42 ]
end

module DefunCPSBigStep = struct
  open Env
  open Terms
  open Cont

  let interpret_prim prim =
    match prim with
    | PAdd -> ( + )
    | PMinus -> ( - )
    | PMult -> ( * )
    | _ -> assert false

  let rec defun_cps_eval env t k =
    flush_all ();
    match t with
    | Lit l -> defun_cps_eval_lit l k
    | Var x -> apply k (get env x)
    | Lam (x, _, t) -> apply k (VClo (x, t, env))
    | Pair (x, y) ->
        apply k (defun_cps_eval env x [ Redex (PPair, [ y ], [], env) ])
    | Stream (x, y) ->
        apply k (defun_cps_eval env x [ Redex (PStream, [ y ], [], env) ])
    | Fst x -> apply k (defun_cps_eval_proj env x PCar)
    | Snd x -> apply k (defun_cps_eval_proj env x PCdr)
    | Head x -> apply k (defun_cps_eval_proj env x PCar)
    | Tail x -> apply k (defun_cps_eval_proj env x PCdr)
    | BinArith (t1, op, t2) ->
        apply k
          (defun_cps_eval env t1
             [ Redex (prim_of_arith_op op, [ t2 ], [], env) ])
    | BinBool (t1, Eq, t2) ->
        apply k (defun_cps_eval env t1 [ Redex (PEq, [ t2 ], [], env) ])
    | BinBool (t1, NEq, t2) ->
        apply k (defun_cps_eval env t1 [ Redex (PNEq, [ t2 ], [], env) ])
    | BinBool (t1, And, t2) ->
        apply k (defun_cps_eval env t1 [ Redex (PAnd, [ t2 ], [], env) ])
    | BinBool (t1, Or, t2) ->
        apply k (defun_cps_eval env t1 [ Redex (POr, [ t2 ], [], env) ])
    | Let (x, ty, t1, t2) ->
        defun_cps_eval env t1 (Redex (PApp, [ Lam (x, ty, t2) ], [], env) :: k)
    | LetRec (x, _, Lam (y, _, b), t2) ->
        let env = bind env x VUnit in
        let env = update env x (VClo (y, b, env)) in
        defun_cps_eval env t2 k
    | Cons _ -> assert false
    | App (t1, t2) -> defun_cps_eval_app env t1 t2 k
    | CompNZ x -> defun_cps_eval_compnz env x k
    | Not x -> defun_cps_eval_not env x k
    | If (c, tt, ff) -> defun_cps_eval_if env c tt ff k
    | TypedTerm (term, _) -> defun_cps_eval env term k
    | List [] -> apply k (VList [])
    | List (x :: xs) ->
        apply k (defun_cps_eval env x [ Redex (PCons, xs, [], env) ])
    | LetRec _ -> assert false

  and defun_cps_eval_if env c tt ff k =
    defun_cps_eval env c (Redex (PIf, [ tt; ff ], [], env) :: k)

  and defun_cps_eval_compnz env x k =
    apply k (defun_cps_eval env x [ Redex (PCompNZ, [], [], env) ])

  and defun_cps_eval_not env x k =
    apply k (defun_cps_eval env x [ Redex (PNot, [], [], env) ])

  and defun_cps_eval_lit l k =
    match l with
    | Unit -> apply k VUnit
    | Int i -> apply k (VInt i)
    | Bool b -> apply k (VBool b)

  and defun_cps_eval_proj env x p =
    defun_cps_eval env x [ Redex (p, [], [], env) ]

  and defun_cps_eval_app env t1 t2 k =
    defun_cps_eval env t2 (Redex (PApp, [ t1 ], [], env) :: k)

  and apply k v =
    match k with
    | [] -> v
    | [ Redex (PPair, [], [ f ], _) ] -> VPair (f, v)
    | [ Redex (PStream, [], [ f ], _) ] -> VStream (f, v)
    | [ Redex (PCar, [], [], _) ] -> (
        match v with
        | VPair (x, _) | VStream (x, _) -> x
        | _ ->
            failwith "Semantics cps big-step: can not project a non pair term")
    | [ Redex (PCdr, [], [], _) ] -> (
        match v with
        | VPair (_, x) | VStream (_, x) -> x
        | _ ->
            failwith "Semantics cps big-step: can not project a non pair term")
    | [ Redex (PCompNZ, [], [], _) ] -> (
        match v with VInt 0 -> VBool true | _ -> VBool false)
    | [ Redex (PNot, [], [], _) ] -> (
        match v with VBool t -> VBool (not t) | _ -> assert false)
    | [ Redex (PCons, [], vs, _) ] -> VList (List.rev (v :: vs))
    | [ Redex (((PAdd | PMinus | PMult | PDiv) as p), [], [ u ], _) ] -> (
        match (u, v) with
        | VInt v1, VInt v2 -> VInt (interpret_prim p v1 v2)
        | _ -> assert false)
    | [ Redex (PEq, [], [ u ], _) ] -> (
        match (u, v) with v1, v2 -> VBool (v1 == v2))
    | [ Redex (PNEq, [], [ u ], _) ] -> (
        match (u, v) with v1, v2 -> VBool (v1 <> v2))
    | [ Redex (PAnd, [], [ u ], _) ] -> (
        match (u, v) with
        | VBool v1, VBool v2 -> VBool (v1 && v2)
        | _ -> assert false)
    | [ Redex (POr, [], [ u ], _) ] -> (
        match (u, v) with
        | VBool v1, VBool v2 -> VBool (v1 || v2)
        | _ -> assert false)
    | Redex (PApp, [], [ a ], _) :: k -> (
        match v with
        | VClo (x, t, cenv) -> defun_cps_eval (bind cenv x a) t k
        | _ ->
            failwith
              "Semantics cps big-step defunctionnalized: cannot apply a non \
               functionnal value\n")
    | Redex (PIf, [ tt; ff ], [], env) :: k -> (
        match v with
        | VBool true -> defun_cps_eval env tt k
        | _ -> defun_cps_eval env ff k)
    | Redex (prim, t :: ts, args, env) :: k ->
        defun_cps_eval env t (Redex (prim, ts, v :: args, env) :: k)
    | _ -> assert false

  let value_of x = defun_cps_eval empty x []

  let%test _ = value_of tunit = vunit
  let%test _ = value_of tapp_id_unit = vunit
  let%test _ = value_of tfun_id = VClo ("x", tx, empty)
  let%test _ = value_of tapp_id_idy_unit = vunit
  let%test _ = value_of (Let ("x", TyInt, Lit (Int 1), Var "x")) = VInt 1

  let%test _ =
    value_of
      (App
         ( App
             ( Lam
                 ("x", TyInt, Lam ("y", TyInt, BinArith (Var "x", Add, Var "y"))),
               Lit (Int 1) ),
           Lit (Int 41) ))
    = VInt 42

  let%test _ = value_of (CompNZ (Lit (Int 0))) = VBool true

  let%test _ =
    value_of (If (CompNZ (Lit (Int 0)), Lit (Int 42), Lit (Int 41))) = VInt 42

  let%test _ =
    value_of
      (If
         ( CompNZ (BinArith (Lit (Int 1), Minus, Lit (Int 1))),
           Lit (Int 42),
           Lit (Int 41) ))
    = VInt 42

  let%test _ =
    value_of
      (If
         ( CompNZ (BinArith (Lit (Int 1), Minus, Lit (Int 1))),
           Fst (Pair (Lit (Int 1), Lit (Int 2))),
           Snd (Pair (Lit (Int 1), Lit (Int 2))) ))
    = VInt 1

  let%test _ =
    value_of (List [ Lit (Int 1); BinArith (Lit (Int 1), Add, Lit (Int 41)) ])
    = VList [ VInt 1; VInt 42 ]

  let%test _ =
    value_of (List [ Lit (Int 1); BinArith (Lit (Int 1), Add, Lit (Int 41)) ])
    = VList [ VInt 1; VInt 42 ]
end

module TailDefunCPSBigStep = struct
  open Env
  open Terms
  open Cont

  let interpret_prim prim =
    match prim with
    | PAdd -> ( + )
    | PMinus -> ( - )
    | PMult -> ( * )
    | _ -> assert false

  let closed_value env t =
    match t with
    | Lit Unit -> VUnit
    | Lit (Int i) -> VInt i
    | Var x -> get env x
    | Lam (x, _, t) -> VClo (x, t, env)
    | _ -> assert false
  (*by typing *)

  let stop = ref false

  let rec tail_defun_cps_eval env t k =
    if !stop then (
      Printf.printf "TERM: %s \nENV: %s\n"
        (IO.document_to_string Printer.print_term t)
        (IO.document_to_string pretty_env env);
      read_line () |> ignore);
    match (t, k) with
    | TypedTerm (term, _), k -> tail_defun_cps_eval env term k
    | (Lit _ | Var _ | Lam _), [] -> closed_value env t
    | Pair (x, y), [] ->
        tail_defun_cps_eval env x [ Redex (PPair, [ y ], [], env) ]
    | Stream (x, y), [] ->
        tail_defun_cps_eval env x [ Redex (PStream, [ y ], [], env) ]
    | Fst t, [] -> tail_defun_cps_eval env t [ Redex (PCar, [], [], empty) ]
    | Snd t, [] -> tail_defun_cps_eval env t [ Redex (PCdr, [], [], empty) ]
    | Head t, [] -> tail_defun_cps_eval env t [ Redex (PCar, [], [], empty) ]
    | Tail t, [] -> tail_defun_cps_eval env t [ Redex (PCdr, [], [], empty) ]
    | CompNZ t, [] ->
        tail_defun_cps_eval env t [ Redex (PCompNZ, [], [], empty) ]
    | Not t, [] -> tail_defun_cps_eval env t [ Redex (PNot, [], [], empty) ]
    | List [], [] -> VList []
    | List (x :: xs), [] ->
        tail_defun_cps_eval env x [ Redex (PCons, xs, [], empty) ]
    | BinArith (t1, op, t2), [] ->
        tail_defun_cps_eval env t1
          [ Redex (prim_of_arith_op op, [ t2 ], [], env) ]
    | App (t1, t2), k ->
        tail_defun_cps_eval env t2 (Redex (PApp, [ t1 ], [], env) :: k)
    | Let (x, ty, t1, t2), k ->
        tail_defun_cps_eval env t1
          (Redex (PApp, [ Lam (x, ty, t2) ], [], env) :: k)
    | LetRec (x, _, Lam (y, _, b), t2), k ->
        let env = bind env x VUnit in
        let env = update env x (VClo (y, b, env)) in
        tail_defun_cps_eval env t2 k
    | If (c, tt, ff), k ->
        tail_defun_cps_eval env c (Redex (PIf, [ tt; ff ], [], env) :: k)
    | t, [ Redex (PPair, [], [ v ], _) ] ->
        VPair (v, tail_defun_cps_eval env t [])
    | t, [ Redex (PStream, [], [ v ], _) ] ->
        VStream (v, tail_defun_cps_eval env t [])
    | t, [ Redex (PCar, [], [], _) ] -> (
        match tail_defun_cps_eval env t [] with
        | VPair (x, _) | VStream (x, _) -> x
        | _ ->
            failwith "Semantics cps big-step: can not project a non pair term")
    | t, [ Redex (PCdr, [], [], _) ] -> (
        match tail_defun_cps_eval env t [] with
        | VPair (_, x) | VStream (_, x) -> x
        | _ ->
            failwith "Semantics cps big-step: can not project a non pair term")
    | t, [ Redex (PCompNZ, [], [], _) ] -> (
        match tail_defun_cps_eval env t [] with
        | VInt 0 -> VBool true
        | VInt _ -> VBool false
        | _ -> assert false)
    | t, [ Redex (PNot, [], [], _) ] -> (
        match tail_defun_cps_eval env t [] with
        | VBool t -> VBool (not t)
        | _ -> assert false)
    | t, [ Redex (((PAdd | PMinus | PMult) as p), [], [ VInt v1 ], _) ] -> (
        match tail_defun_cps_eval env t [] with
        | VInt v2 -> VInt (interpret_prim p v1 v2)
        | _ -> assert false)
    | t, [ Redex (PCons, [], vs, _) ] ->
        VList (List.rev (tail_defun_cps_eval env t [] :: vs))
    | t, Redex (PApp, [], [ v ], _) :: k -> (
        match closed_value env t with
        | VClo (x, t, envc) -> tail_defun_cps_eval (bind envc x v) t k
        | _ ->
            failwith
              "Semantics cps big-step defunctionnalized: cannot apply a non \
               functionnal value\n")
    | t, Redex (PIf, [ tt; ff ], [], env) :: k -> (
        match tail_defun_cps_eval env t [] with
        | VBool true -> tail_defun_cps_eval env tt k
        | VBool false -> tail_defun_cps_eval env ff k
        | _ -> assert false)
    | t, Redex (p, a :: args, vs, env) :: k ->
        let v = tail_defun_cps_eval env t [] in
        tail_defun_cps_eval env a (Redex (p, args, v :: vs, env) :: k)
    | _ -> assert false

  let value_of x = tail_defun_cps_eval empty x []

  let%test _ = value_of tunit = vunit
  let%test _ = value_of tapp_id_unit = vunit
  let%test _ = value_of tfun_id = VClo ("x", tx, empty)
  let%test _ = value_of tapp_id_idy_unit = vunit
  let%test _ = value_of (Let ("x", TyInt, Lit (Int 1), Var "x")) = VInt 1

  let%test _ =
    value_of
      (App
         ( App
             ( Lam
                 ("x", TyInt, Lam ("y", TyInt, BinArith (Var "x", Add, Var "y"))),
               Lit (Int 1) ),
           Lit (Int 41) ))
    = VInt 42

  let%test _ =
    value_of (If (CompNZ (Lit (Int 0)), Lit (Int 42), Lit (Int 41))) = VInt 42

  let%test _ =
    value_of
      (If
         ( CompNZ (BinArith (Lit (Int 1), Minus, Lit (Int 1))),
           Lit (Int 42),
           Lit (Int 41) ))
    = VInt 42

  let%test _ =
    value_of
      (If
         ( CompNZ (BinArith (Lit (Int 1), Minus, Lit (Int 1))),
           Fst (Pair (Lit (Int 1), Lit (Int 2))),
           Snd (Pair (Lit (Int 1), Lit (Int 2))) ))
    = VInt 1
end

module AbstractMachine = struct
  open Env
  open Terms
  open Cont

  type step = Halt of value | Resume of env * term * cont

  let interpret_prim prim =
    match prim with
    | PAdd -> ( + )
    | PMinus -> ( - )
    | PMult -> ( * )
    | _ -> assert false

  let closed_value env t =
    match t with
    | Lit Unit -> VUnit
    | Lit (Int i) -> VInt i
    | Var x -> get env x
    | Lam (x, _, t) -> VClo (x, t, env)
    | _ -> (*by typing *) assert false

  let rec tail_rec_eval env t k =
    match (t, k) with
    | TypedTerm (term, _), k -> tail_rec_eval env term k
    | (Lit _ | Var _ | Lam _), [] -> closed_value env t
    | Pair (x, y), [] ->
        tail_rec_eval empty x (Redex (PPair, [ y ], [], env) :: k)
    | Stream (x, y), [] ->
        tail_rec_eval empty x (Redex (PStream, [ y ], [], env) :: k)
    | Fst t, [] -> tail_rec_eval env t (Redex (PCar, [], [], empty) :: k)
    | Snd t, [] -> tail_rec_eval env t (Redex (PCdr, [], [], empty) :: k)
    | Head t, [] -> tail_rec_eval env t (Redex (PCar, [], [], empty) :: k)
    | Tail t, [] -> tail_rec_eval env t (Redex (PCdr, [], [], empty) :: k)
    | BinArith (t1, op, t2), [] ->
        tail_rec_eval env t1 [ Redex (prim_of_arith_op op, [ t2 ], [], env) ]
    | CompNZ t, [] -> tail_rec_eval env t [ Redex (PCompNZ, [], [], empty) ]
    | Not t, [] -> tail_rec_eval env t [ Redex (PNot, [], [], empty) ]
    | App (t1, t2), k ->
        tail_rec_eval env t2 (Redex (PApp, [ t1 ], [], env) :: k)
    | Let (x, ty, t1, t2), k ->
        tail_rec_eval env t1 (Redex (PApp, [ Lam (x, ty, t2) ], [], env) :: k)
    | LetRec (x, _, Lam (y, _, b), t2), k ->
        let env = bind env x VUnit in
        let env = update env x (VClo (y, b, env)) in
        tail_rec_eval env t2 k
    | If (c, tt, ff), k ->
        tail_rec_eval env c (Redex (PIf, [ tt; ff ], [], env) :: k)
    | t, [ Redex (PPair, [], [ v ], _) ] -> VPair (v, tail_rec_eval env t k)
    | t, [ Redex (PStream, [], [ v ], _) ] -> VStream (v, tail_rec_eval env t k)
    | t, [ Redex (PCar, [], [], _) ] -> (
        match tail_rec_eval env t [] with
        | VPair (x, _) | VStream (x, _) -> x
        | _ ->
            failwith "Semantics cps big-step: can not project a non pair term")
    | t, [ Redex (PCdr, [], [], _) ] -> (
        match tail_rec_eval env t [] with
        | VPair (_, x) | VStream (_, x) -> x
        | _ ->
            failwith "Semantics cps big-step: can not project a non pair term")
    | t, [ Redex (((PAdd | PMinus | PMult) as p), [], [ VInt v1 ], _) ] -> (
        match tail_rec_eval env t [] with
        | VInt v2 -> VInt (interpret_prim p v1 v2)
        | _ -> assert false)
    | t, [ Redex (PCompNZ, [], [], _) ] -> (
        match tail_rec_eval env t [] with
        | VInt 0 -> VBool true
        | _ -> VBool false)
    | t, [ Redex (PNot, [], [], _) ] -> (
        match tail_rec_eval env t [] with
        | VBool t -> VBool (not t)
        | _ -> assert false)
    | t, Redex (PApp, [], [ v ], _) :: k -> (
        match closed_value env t with
        | VClo (x, t, env) -> tail_rec_eval (bind env x v) t k
        | _ -> (* by typing *) assert false)
    | t, Redex (PIf, [ tt; ff ], [], env) :: k -> (
        match tail_rec_eval env t [] with
        | VBool true -> tail_rec_eval env tt k
        | _ -> tail_rec_eval env ff k)
    | t, Redex (p, a :: args, vs, env) :: k ->
        let v = tail_rec_eval env t [] in
        tail_rec_eval env a (Redex (p, args, v :: vs, env) :: k)
    | _ -> assert false

  let rec step = function
    | env, ((Lit _ | Var _ | Lam _) as t), [] -> Halt (closed_value env t)
    | env, Pair (x, y), [] -> Resume (env, x, [ Redex (PPair, [ y ], [], env) ])
    | env, Stream (x, y), [] ->
        Resume (env, x, [ Redex (PStream, [ y ], [], env) ])
    | env, Fst t, [] -> Resume (env, t, [ Redex (PCar, [], [], empty) ])
    | env, Snd t, [] -> Resume (env, t, [ Redex (PCdr, [], [], empty) ])
    | env, Head t, [] -> Resume (env, t, [ Redex (PCar, [], [], empty) ])
    | env, Tail t, [] -> Resume (env, t, [ Redex (PCdr, [], [], empty) ])
    | env, CompNZ t, [] -> Resume (env, t, [ Redex (PCompNZ, [], [], empty) ])
    | env, Not t, [] -> Resume (env, t, [ Redex (PNot, [], [], empty) ])
    | env, BinArith (t1, op, t2), [] ->
        Resume (env, t1, [ Redex (prim_of_arith_op op, [ t2 ], [], env) ])
    | env, App (t1, t2), k ->
        Resume (env, t2, Redex (PApp, [ t1 ], [], env) :: k)
    | env, Let (x, ty, t1, t2), k ->
        Resume (env, t1, Redex (PApp, [ Lam (x, ty, t2) ], [], env) :: k)
    | env, LetRec (x, ty, (Lam (y, _, b) as t1), t2), k ->
        let env = bind env x VUnit in
        let env = update env x (VClo (y, b, env)) in
        Resume (env, t1, Redex (PApp, [ Lam (x, ty, t2) ], [], env) :: k)
    | env, If (c, tt, ff), k ->
        Resume (env, c, Redex (PIf, [ tt; ff ], [], env) :: k)
    | env, t, [ Redex (PPair, [], [ v ], _) ] ->
        Halt (VPair (v, tail_eval env t []))
    | env, t, [ Redex (PStream, [], [ v ], _) ] ->
        Halt (VStream (v, tail_eval env t []))
    | env, t, [ Redex (PCar, [], [], _) ] -> (
        match tail_eval env t [] with
        | VPair (x, _) | VStream (x, _) -> Halt x
        | _ ->
            failwith "Semantics cps big-step: can not project a non pair term")
    | env, t, [ Redex (PCdr, [], [], _) ] -> (
        match tail_eval env t [] with
        | VPair (_, x) | VStream (_, x) -> Halt x
        | _ ->
            failwith "Semantics cps big-step: can not project a non pair term")
    | env, t, [ Redex (((PAdd | PMinus | PMult) as p), [], [ VInt v1 ], _) ]
      -> (
        match tail_eval env t [] with
        | VInt v2 -> Halt (VInt (interpret_prim p v1 v2))
        | _ -> assert false)
    | env, t, [ Redex (PCompNZ, [], [], _) ] -> (
        match tail_eval env t [] with
        | VInt 0 -> Halt (VBool true)
        | _ -> Halt (VBool false))
    | env, t, [ Redex (PNot, [], [], _) ] -> (
        match tail_eval env t [] with
        | VBool t -> Halt (VBool (not t))
        | _ -> assert false)
    | env, t, Redex (PApp, [], [ v ], _) :: k -> (
        match closed_value env t with
        | VClo (x, t, env) -> Resume (bind env x v, t, k)
        | _ -> (*by typing*) assert false)
    | env, t, Redex (PIf, [ tt; ff ], [], envk) :: k -> (
        match tail_eval env t [] with
        | VBool true -> Resume (envk, tt, k)
        | _ -> Resume (envk, ff, k))
    | env, t, Redex (p, a :: args, vs, kenv) :: k ->
        let v = tail_eval env t [] in
        Resume (kenv, a, Redex (p, args, v :: vs, kenv) :: k)
    | _ -> assert false

  and tail_eval e t k =
    IO.document_to_string
      (fun () ->
        PPrint.(
          string "step on " ^^ Printer.print_term t ^^ string " with "
          ^^ pretty_cont k ^^ string " and env is " ^^ pretty_env e
          ^^ string "\n"))
      ()
    |> print_endline;

    match step (e, t, k) with
    | Halt v -> v
    | Resume (e', t', k') -> tail_eval e' t' k'

  let value_of x = tail_eval empty x []

  let%test _ = value_of tunit = vunit
  let%test _ = value_of tapp_id_unit = vunit
  let%test _ = value_of tfun_id = VClo ("x", tx, empty)
  let%test _ = value_of tapp_id_idy_unit = vunit
  let%test _ = value_of (Let ("x", TyInt, Lit (Int 1), Var "x")) = VInt 1

  let%test _ =
    value_of
      (App
         ( App
             ( Lam
                 ("x", TyInt, Lam ("y", TyInt, BinArith (Var "x", Add, Var "y"))),
               Lit (Int 1) ),
           Lit (Int 41) ))
    = VInt 42

  let%test _ =
    value_of (If (CompNZ (Lit (Int 0)), Lit (Int 42), Lit (Int 41))) = VInt 42

  let%test _ =
    value_of
      (If
         ( CompNZ (BinArith (Lit (Int 1), Minus, Lit (Int 1))),
           Lit (Int 42),
           Lit (Int 41) ))
    = VInt 42
end
