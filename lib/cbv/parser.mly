%{ (* Emacs, use -*- tuareg -*- to open this file. *)
   module Libcbv = struct end
   open Syntax.Terms
   type pbinop = ArithOp of arith_op | BoolOp of bool_op

   let make_bin op lhs rhs =
     match op with
    | ArithOp op -> BinArith (lhs, op, rhs)
    | BoolOp op -> BinBool (lhs, op, rhs)


%}

%token EOF
%token FUN LET REC IN INT UNIT FST SND COMPNZ IF THEN ELSE
%token ARROW LPAREN RPAREN COLON CONS EQUAL COMMA LIST STREAM HEAD TAIL
%token TIMES MINUS PLUS SLASH
%token AND OR NEQ NOT
%token<string> ID
%token<int> LINT
%token<bool> LBOOL
%token LUNIT
%token NIL
%token BOOL

%start<Syntax.Terms.program> program
%right ARROW IN CONS
%nonassoc ELSE
%left PLUS MINUS NEQ EQUAL
%left TIMES SLASH AND OR

%%
program: p=term EOF { p }

term:
a=abstraction
{
  a
}
| d=definition IN t=term
{
  Let(fst (fst d),snd (fst d),snd d, t)
}
| d=rec_definition IN t=term
{
  LetRec(fst (fst d),snd (fst d),snd d, t)
}
| lhs=term CONS rhs=term
{
  Cons (lhs,rhs)
}
| lhs=term op=binop rhs=term
{
  make_bin op lhs rhs
}
| IF c=term THEN bt=term ELSE bf=term
{
  If(c,bt,bf)
}
| t=simple_term
{
  t
}

simple_term:
  a=simple_term b=atomic_term
{
  App (a, b)
}
| FST a=atomic_term
{
  Fst a
}
| SND a=atomic_term
{
  Snd a
}
| HEAD a=atomic_term
{
  Head a
}
| TAIL a=atomic_term
{
  Tail a
}
| COMPNZ a=atomic_term
{
  CompNZ a
}
| NOT a=atomic_term
{
  Not a
}
| STREAM a=atomic_term b=atomic_term
{
  Stream (a,b)
}
| a=atomic_term
{
  a
}

atomic_term:
x=ID {
  Var x
    }
| NIL
{ List [] }
| l=literal {
  Lit l
}
| LPAREN lsh=term COMMA rhs=term RPAREN {
  Pair(lsh,rhs)
}
| LPAREN t = term RPAREN { t }

literal:
  i=LINT
{
  Int i
}
| LUNIT
{
  Unit
}
| b=LBOOL
{
 Bool b
}

%inline abstraction: FUN bs=binding+ ARROW t=term
{
  make_lambda_abstraction bs t
}

%inline definition: LET b=binding EQUAL t=term
{
  (b, t)
}
| LET x=ID arg=binding
  COLON oty=typ
  EQUAL t=term
{
  let tyf =  Syntax.Types.TyArrow ([snd arg], oty) in
  ((x, tyf), Syntax.Terms.make_lambda_abstraction [arg] t)
}

%inline rec_definition: LET REC b=binding EQUAL t=term
{
  (b, t)
}
| LET REC x=ID arg=binding
  COLON oty=typ
  EQUAL t=term
{
  let tyf =  Syntax.Types.TyArrow ([snd arg], oty) in
  ((x, tyf), Syntax.Terms.make_lambda_abstraction [arg] t)
}

%inline binop :
| PLUS  { ArithOp (Add) }
| TIMES { ArithOp (Mult) }
| MINUS { ArithOp (Minus) }
| SLASH { ArithOp (Div) }
| EQUAL { BoolOp (Eq) }
| NEQ { BoolOp (NEq) }
| AND  { BoolOp (And) }
| OR { BoolOp (Or) }


binding: LPAREN x=ID COLON ty=typ RPAREN
{
  (x, ty)
}

typ: x=type_constant
{
  x
}
| lhs=type_constant ARROW rhs=typ
{
  Syntax.Types.TyArrow ([lhs], rhs)
}
| lhs=type_constant TIMES rhs=typ
{
  Syntax.Types.TyProd (lhs, rhs)
}
| t=type_constant LIST
{ Syntax.Types.TyList t}
| t=type_constant STREAM
{ Syntax.Types.TyStream t}

type_constant:
INT
{
  Syntax.Types.TyInt
}
| BOOL
{
  Syntax.Types.TyBool
}
| UNIT
{
  Syntax.Types.TyUnit
}
| LPAREN t=typ RPAREN
{
  t
}
