module Types = struct
  type 'a ty_structure =
    | TyVar of 'a
    | TyInt
    | TyUnit
    | TyBool
    | TyArrow of ('a ty_structure list * 'a ty_structure)
    | TyProd of ('a ty_structure * 'a ty_structure)
    | TyList of 'a ty_structure
    | TyStream of 'a ty_structure

  and ty = int ty_structure

  let int_ty = TyInt
  let unit_ty = TyUnit
  let list_ty a = TyList a
  let int_list_ty = list_ty int_ty
  let arrow_ty variance covariance = TyArrow ([ variance ], covariance)
  let prod_ty ty1 ty2 = TyProd (ty1, ty2)
end

module Terms = struct
  open Types

  type var = string

  and term =
    | Var of var
    | Lit of lit
    | Pair of (term * term)
    | List of term list
    | Not of term
    | Fst of term
    | Snd of term
    | Cons of term * term
    | Head of term
    | Tail of term
    | Stream of term * term
    | BinArith of (term * arith_op * term)
    | BinBool of (term * bool_op * term)
    | CompNZ of term
    | If of term * term * term
    | Lam of (var * ty * term)
    | Let of (var * ty * term * term)
    | LetRec of (var * ty * term * term)
    | App of (term * term)
    | TypedTerm of (term * ty)

  and lit = Unit | Int of int | Bool of bool
  and arith_op = Add | Minus | Mult | Div
  and bool_op = Eq | NEq | And | Or
  and program = term

  let tunit = Lit Unit
  let tvar x = Var x
  let tlam x ty t = Lam (x, ty, t)
  let tapp t1 t2 = App (t1, t2)
  let tx = tvar "x"
  let tfun_id = tlam "x" (arrow_ty unit_ty unit_ty) tx
  let tfun_idy = tlam "y" unit_ty (tvar "y")
  let tapp_id_id = tapp tfun_id tfun_idy
  let tapp_id_unit = tapp tfun_idy tunit
  let tapp_id_idy_unit = tapp (tapp tfun_id tfun_idy) tunit

  let p =
    Let
      ( "plus",
        TyArrow ([ TyInt ], TyArrow ([ TyInt ], TyInt)),
        Lam ("x", TyInt, Lam ("y", TyInt, BinArith (Var "x", Add, Var "y"))),
        App (App (Var "plus", Lit (Int 1)), Lit (Int 41)) )

  let s =
    Let
      ( "succ",
        TyArrow ([ TyInt ], TyInt),
        Lam ("x", TyInt, BinArith (Var "x", Add, Lit (Int 1))),
        App (Var "succ", Lit (Int 41)) )

  let make_lambda_abstraction bs t =
    let rec aux = function [] -> t | (x, t) :: bs -> Lam (x, t, aux bs) in
    aux bs
end

module Values = struct
  open Terms

  type value =
    | VUnit
    | VInt of int
    | VBool of bool
    | VList of value list
    | VPair of (value * value)
    | VClo of (var * term * env)
    | VStream of value * value

  and env = (var * value ref) list

  let vunit = VUnit
end
