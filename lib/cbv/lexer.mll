{ (* Emacs, use -*- tuareg -*- to open this file. *)
  open Parser

  let enter_newline lexbuf =
    Lexing.new_line lexbuf;
    lexbuf
}

let newline = ('\010' | '\013' | "\013\010")

let blank   = [' ' '\009' '\012']

let id = ['a'-'z''0'-'9''_']+

let decimal = ['0'-'9']+

let int = decimal+

rule token = parse
| newline { enter_newline lexbuf |> token }
| blank   { token lexbuf }
| "(*"    { comment 0 lexbuf }
| "->"    { ARROW }
| ":"     { COLON }
| ","     { COMMA }
| "="     { EQUAL }
| "fun"   { FUN }
| "let"   { LET }
| "rec"   { REC }
| "in"    { IN }
| "fst"   { FST }
| "snd"   { SND }
| "int"   { INT }
| "unit"   { UNIT }
| "list"  { LIST }
| "("     { LPAREN }
| ")"     { RPAREN }
| "*"     { TIMES }
| "+"     { PLUS  }
| "-"     { MINUS }
| "/"     { SLASH }
| "&&"     { AND }
| "||"     { OR }
| "<>"     { NEQ }
| "not"   { NOT   }
| "[]"    { NIL   }
| "::"    { CONS  }
| "()"    { LUNIT }
| "if"    { IF }
| "is_zero"   { COMPNZ }
| "then"  { THEN }
| "else"  { ELSE }
| "stream" { STREAM }
| "head" { HEAD }
| "tail" { TAIL }
| "bool" { BOOL }
| "true" { LBOOL (true) }
| "false" { LBOOL (false) }
| int as i { LINT (int_of_string i) }
| id as s { ID s }
| eof     { EOF }
| _ as c  {
  Printf.eprintf "Lexing error at %d: Unexpected `%c'."
    lexbuf.Lexing.lex_curr_pos c;
  exit 1
}

and comment depth = parse
| "*)" {
  if depth = 0 then token lexbuf else comment (depth - 1) lexbuf
}
| "(*" {
  comment (depth + 1) lexbuf
}
| eof {
  Printf.eprintf "Unexpected EOF inside comments.";
  exit 1
}
| _ {
  comment depth lexbuf
}
